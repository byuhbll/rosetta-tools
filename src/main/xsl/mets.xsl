<?xml version="1.0" encoding="UTF-8"?>

<!-- Make it's xlin not xlink or the submission will fail -->
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:mets="http://www.loc.gov/METS/"
	xmlns:xlin="http://www.w3.org/1999/xlink">
	
<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />


<!--

	Rosetta uses a subset of the METS schema. It expects four sections:
	
	* dmdSec - houses the descriptive metadata in formats such as dublin core
	* amdSec - don't know what this does, but it needs to be there
	* fileSec - list and describe each file
	* structMap - how the files are related to one another

	General METS documentation:
	
	* METS schema and documentation: http://www.loc.gov/standards/mets/mets-schemadocs.html 
	* METS current schema: http://www.loc.gov/standards/mets/mets.xsd
	* METS examples: http://www.loc.gov/standards/mets/mets-examples.html
	
-->

<!-- A prefix for all file IDs -->
<xsl:param name="fileIdPrefix" select="'file-'" />


<xsl:template match="/">
	<!--<xsl:message><copy-of select="*" /></xsl:message>-->
	<xsl:call-template name="mets" />
</xsl:template>


<xsl:template name="mets">
	<mets:mets>
		<xsl:call-template name="dmdSec" />
		<xsl:call-template name="amdSecRep" />
		<xsl:call-template name="amdSecFile" />
		<xsl:call-template name="amdSecIe" />
		<xsl:call-template name="fileSec" />
		<xsl:call-template name="structMap" />
	</mets:mets>
</xsl:template>


<xsl:template name="dmdSec">
	<mets:dmdSec ID="ie-dmd">
		<mets:mdWrap MDTYPE="DC">
			<mets:xmlData>
				<xsl:copy-of select="/premets/dc/*" />
			</mets:xmlData>
		</mets:mdWrap>
	</mets:dmdSec>
</xsl:template>


<xsl:template name="amdSecRep">
	<mets:amdSec ID="rep1-amd">
		<xsl:call-template name="amdSec">
			<xsl:with-param name="id" select="'rep1'" />
			<xsl:with-param name="sections"><tech/></xsl:with-param>
			<xsl:with-param name="xmlData">
				<dnx xmlns="http://www.exlibrisgroup.com/dps/dnx">
					<section id="generalRepCharacteristics">
						<record>
							<key id="preservationType">PRESERVATION_MASTER</key>
							<key id="usageType">VIEW</key>
							<key id="RevisionNumber">1</key>
						</record>
					</section>
				</dnx>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="amdSec">
			<xsl:with-param name="id" select="'rep1'" />
			<xsl:with-param name="sections"><rights/><source/><digiprov/></xsl:with-param>
		</xsl:call-template>
	</mets:amdSec>
</xsl:template>


<xsl:template name="amdSecFile">
	<xsl:for-each select="/premets/files/descendant::file">
		<xsl:variable name="id" select="concat('file-', .)" />
		
		<mets:amdSec>
			<xsl:attribute name="ID" select="concat($id, '-amd')" />
			<xsl:call-template name="amdSec">
				<xsl:with-param name="id" select="$id" />
			</xsl:call-template>
		</mets:amdSec>
	</xsl:for-each>
</xsl:template>


<xsl:template name="amdSecIe">
	<mets:amdSec ID="ie-amd">
		<xsl:call-template name="amdSec">
			<xsl:with-param name="id" select="'ie'" />
			<xsl:with-param name="sections"><tech/></xsl:with-param>
			<xsl:with-param name="xmlData">
				<xsl:choose>
					<xsl:when test="/premets/retentionPolicy != ''">
						<dnx xmlns="http://www.exlibrisgroup.com/dps/dnx">
							<section id="retentionPolicy">
								<record>
									<key id="policyId">
										<xsl:value-of select="/premets/retentionPolicy" />
									</key>
								</record>
							</section>
						</dnx>
					</xsl:when>
					<xsl:otherwise>
						<dnx xmlns="http://www.exlibrisgroup.com/dps/dnx"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="amdSec">
			<xsl:with-param name="id" select="'ie'" />
			<xsl:with-param name="sections"><rights/></xsl:with-param>
			<xsl:with-param name="xmlData">
				<xsl:choose>
					<xsl:when test="/premets/rightsPolicy != ''">
						<dnx xmlns="http://www.exlibrisgroup.com/dps/dnx">
							<section id="accessRightsPolicy">
								<record>
									<key id="policyId">
										<xsl:value-of select="/premets/rightsPolicy" />
									</key>
								</record>
							</section>
						</dnx>
					</xsl:when>
					<xsl:otherwise>
						<dnx xmlns="http://www.exlibrisgroup.com/dps/dnx"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:choose>
			<xsl:when test="/premets/srcmd"> 
				<xsl:call-template name="srcSec">
					<xsl:with-param name="id" select="'ie'" />
					<xsl:with-param name="sections"><source/></xsl:with-param>
					<xsl:with-param name="mdType" select="/premets/srcmd/@type" />
					<xsl:with-param name="otherMdType" select="/premets/srcmd/@otherType" />
					<xsl:with-param name="xmlData" select="/premets/srcmd/*" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="amdSec">
					<xsl:with-param name="id" select="'ie'" />
					<xsl:with-param name="sections"><source/></xsl:with-param>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:call-template name="amdSec">
			<xsl:with-param name="id" select="'ie'" />
			<xsl:with-param name="sections"><digiprov/></xsl:with-param>
		</xsl:call-template>
	</mets:amdSec>
</xsl:template>


<xsl:template name="amdSec">
	<xsl:param name="id" />
	<xsl:param name="sections"><tech/><rights/><source/><digiprov/></xsl:param>
	<xsl:param name="mdType" select="'OTHER'" />
	<xsl:param name="otherMdType" select="'dnx'" />
	<xsl:param name="xmlData"><dnx xmlns="http://www.exlibrisgroup.com/dps/dnx"/></xsl:param>

	<xsl:for-each select="$sections/*">
		<xsl:element name="mets:{name()}MD">
			<xsl:attribute name="ID" select="concat($id, '-amd-', name())" />
			<mets:mdWrap>
				<xsl:attribute name="MDTYPE" select="$mdType" />
				<xsl:if test="$otherMdType"><xsl:attribute name="OTHERMDTYPE" select="$otherMdType"/></xsl:if>
				<mets:xmlData>
					<xsl:copy-of select="$xmlData" />
				</mets:xmlData>
			</mets:mdWrap>
		</xsl:element>
	</xsl:for-each>
</xsl:template>

<xsl:template name="srcSec">
	<xsl:param name="id" />
	<xsl:param name="sections"><source/></xsl:param>
	<xsl:param name="mdType" select="'OTHER'" />
	<xsl:param name="otherMdType" select="'dnx'" />
	<xsl:param name="xmlData"><dnx xmlns="http://www.exlibrisgroup.com/dps/dnx"/></xsl:param>

	<xsl:for-each select="$sections/*">	
		<xsl:element name="mets:{name()}MD">
			<xsl:attribute name="ID" select="concat($id, '-amd-', name())" />
			<mets:mdWrap MDTYPE="OTHER" OTHERMDTYPE="dnx">
				<mets:xmlData>
					<dnx xmlns="http://www.exlibrisgroup.com/dps/dnx" />
				</mets:xmlData>
			</mets:mdWrap>
		</xsl:element>
		<xsl:element name="mets:{name()}MD">
			<xsl:attribute name="ID" select="concat($id, '-amd-', name(), '-', $otherMdType)" />
			<mets:mdWrap>
				<xsl:attribute name="MDTYPE" select="$mdType" />
				<xsl:if test="$otherMdType"><xsl:attribute name="OTHERMDTYPE" select="$otherMdType"/></xsl:if>
				<mets:xmlData>
					<xsl:copy-of select="$xmlData" />
				</mets:xmlData>
			</mets:mdWrap>
		</xsl:element>
	</xsl:for-each>
</xsl:template>


<xsl:template name="fileSec">
	<mets:fileSec>
		<mets:fileGrp USE="VIEW" ID="rep1" ADMID="rep1-amd">
			<xsl:for-each select="/premets/files/descendant::file">
				<mets:file>
					<xsl:attribute name="ID" select="concat($fileIdPrefix, .)" />
					<xsl:attribute name="MIMETYPE">
						<xsl:choose>
							<xsl:when test="ends-with(., '.pdf')">
								<xsl:text>application/pdf</xsl:text>
							</xsl:when>
							<xsl:when test="ends-with(., '.xml')">
								<xsl:text>text/xml</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>application/octet-stream</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:attribute name="ADMID" select="concat($fileIdPrefix, ., '-amd')" />
					<mets:FLocat LOCTYPE="URL">
						<xsl:attribute name="xlin:href" select="."/>
					</mets:FLocat>
				</mets:file>			
			</xsl:for-each>
		</mets:fileGrp>
	</mets:fileSec>
</xsl:template>


<xsl:template name="structMap">
	<mets:structMap ID="rep1-1" TYPE="PHYSICAL">
		<mets:div LABEL="PRESERVATION_MASTER;VIEW">
			<xsl:apply-templates select="/premets/files/*" />
		</mets:div>
	</mets:structMap>
</xsl:template>


<xsl:template match="div">
	<mets:div>
		<xsl:if test="@label">
			<xsl:attribute name="LABEL" select="@label" />
		</xsl:if>
		
		<xsl:if test="@type">
			<xsl:attribute name="TYPE" select="@type" />
		</xsl:if>
		
		<xsl:apply-templates select="div|file"/>
	</mets:div>
</xsl:template>


<xsl:template match="file">
	<mets:div TYPE="FILE">
		<xsl:if test="@label">
			<xsl:attribute name="LABEL" select="@label" />
		</xsl:if>
		
		<mets:fptr>
			<xsl:attribute name="FILEID" select="concat($fileIdPrefix, .)" />
		</mets:fptr>
	</mets:div>
</xsl:template>


</xsl:stylesheet>
