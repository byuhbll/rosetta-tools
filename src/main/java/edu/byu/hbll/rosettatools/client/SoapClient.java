package edu.byu.hbll.rosettatools.client;

import edu.byu.hbll.xml.XmlUtils;
import javax.xml.bind.JAXBException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.w3c.dom.Document;

/**
 * Soap Client.
 *
 * @author cfd2
 */
public class SoapClient {

  /**
   * Send SOAPPart.
   *
   * @param uri the uri
   * @param action the action
   * @param data the data
   * @return SOAPPart
   * @throws SOAPException if a SOAP exception occurs
   * @throws JAXBException if a JAXB exception occurs
   */
  public static SOAPPart send(String uri, String action, Object data)
      throws SOAPException, JAXBException {
    MessageFactory messageFactory = MessageFactory.newInstance();
    SOAPMessage soapMessage = messageFactory.createMessage();
    SOAPPart soapPart = soapMessage.getSOAPPart();

    // SOAP Body
    SOAPBody soapBody = soapPart.getEnvelope().getBody();
    Document document = XmlUtils.marshal(data);
    soapBody.addDocument(document);

    MimeHeaders headers = soapMessage.getMimeHeaders();
    headers.addHeader("SOAPAction", action);

    soapMessage.saveChanges();

    SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
    SOAPConnection soapConnection = soapConnectionFactory.createConnection();

    SOAPMessage soapResponse = soapConnection.call(soapMessage, uri);

    soapConnection.close();

    return soapResponse.getSOAPPart();
  }
}
