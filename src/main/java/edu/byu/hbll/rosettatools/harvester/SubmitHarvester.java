package edu.byu.hbll.rosettatools.harvester;

import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.sip.Sip;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * The Submit Harvester.
 *
 * @author cfd2
 */
public class SubmitHarvester extends AbstractHarvester {

  static final Logger logger = LoggerFactory.getLogger(SubmitHarvester.class);

  private String subDir = "manual";

  private DocumentBuilder docBuilder;

  /**
   * Constructor.
   *
   * @throws ParserConfigurationException if an error occurs
   */
  public SubmitHarvester() throws ParserConfigurationException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    docBuilder = factory.newDocumentBuilder();
  }

  @Override
  public void harvest(Date from, Date until, int startIndex, int throughIndex) throws Exception {
    for (File sip : getSips()) {
      harvest(sip);
    }
  }

  @Override
  public void harvest(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {
    for (File sip : getSips()) {
      if (sip.getName().startsWith(collection + "-")) {
        harvest(sip);
      }
    }
  }

  @Override
  public void harvest(String collection, String item) throws Exception {
    File sip = new File(sipFactory.getDepositDir(), collection + "-" + item);
    harvest(sip);
  }

  /**
   * Harvests the sip.
   *
   * @param sipDir the location of the sip
   * @throws SAXException if an error occurs
   * @throws TransformerException if an error occurs
   */
  public void harvest(File sipDir) throws SAXException, TransformerException {
    String collection = getCollection(sipDir);
    String item = getItem(sipDir);
    File dc = null;

    //Allows program to continue even if the dc.xml is not found
    //Provides consistent processing for large collections without interruption
    //error files are caught in Rosetta processing.
    try {
      Sip sip = sipFactory.newInstance(collection, item);

      logger.info("Processing " + collection + "/" + item);

      if (!sip.getDcFile().exists()) {

        dc = new File(sip.getDir(), subDir + "/dc.xml");
        FileUtils.copyFile(dc, sip.getDcFile());
      }

      Document dcDoc = docBuilder.parse(sip.getDcFile());

      if (!sip.getMetsFile().exists()) {
        File files = new File(sip.getDir(), subDir + "/files.xml");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("dc", dcDoc);

        String metsXml = toMetsXml(new StreamSource(files), params);
        sip.saveMets(metsXml);
      }

      // send it to Rosetta
      if (noSubmit) {
        logger.warn("Skipping submission to Rosetta");
      } else {
        sip.submit();
      }
    } catch (IOException e) {
      logger.error("File not found " + dc + " " + e);
    }
  }

  @Override
  public Set<Item> list() throws Exception {
    Set<Item> collections = new HashSet<Item>();

    for (File sip : getSips()) {
      Item collection = new Item(getCollection(sip));
      collections.add(collection);
    }

    return collections;
  }

  @Override
  public Set<Item> list(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {
    Set<Item> items = new HashSet<Item>();

    for (File sip : getSips()) {
      if (getCollection(sip).equals(collection)) {
        continue;
      }

      Item item = new Item(getItem(sip));
      items.add(item);
    }

    return items;
  }

  public List<File> getSips() {
    List<File> sips = Arrays.asList((new File(sipFactory.getDepositDir())).listFiles());
    return sips;
  }

  /**
   * Gets the collection for the sip.
   *
   * @param sip the sip
   * @return the collection
   */
  public String getCollection(File sip) {
    return sip.getName().replaceAll("-.*", "");
  }

  /**
   * Gets the item for the sip.
   *
   * @param sip the sip
   * @return the item
   */
  public String getItem(File sip) {
    return sip.getName().replaceAll(".*-", "");
  }

  // getters
  public String getSubDir() {
    return subDir;
  }

  // setters
  public void setSubDir(String subDir) {
    this.subDir = subDir;
  }
}
