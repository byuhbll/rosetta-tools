package edu.byu.hbll.rosettatools.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The SpreadSheet model.
 *
 * @author Joe Larson
 * @author Ben Welker
 */
@XmlRootElement(name = Spreadsheet.RECORD)
@XmlAccessorType(XmlAccessType.NONE)
public class Spreadsheet implements Serializable {

  private static final long serialVersionUID = 1L;

  public static final String RECORD = "record";

  public static final String FIELD = "field";

  @XmlElement(name = FIELD)
  private List<Field> fields = new ArrayList<Field>();

  public void add(Field field) {
    fields.add(field);
  }

  // getters
  public List<Field> getField() {
    return fields;
  }

  // setters
  public void setField(List<Field> fields) {
    this.fields = fields;
  }
}
