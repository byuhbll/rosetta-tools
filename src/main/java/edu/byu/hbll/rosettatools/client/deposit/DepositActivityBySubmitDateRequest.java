package edu.byu.hbll.rosettatools.client.deposit;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * POJO to model request.
 *
 * @author cfd2
 */
@XmlRootElement(name = "getDepositActivityBySubmitDate", namespace = "http://dps.exlibris.com/")
@XmlAccessorType(XmlAccessType.NONE)
public class DepositActivityBySubmitDateRequest {

  public static final String DATE_FORMAT = "dd/MM/yyyy";

  @XmlElement(name = "arg0")
  private String pdsHandle;

  @XmlElement(name = "arg1")
  private String depositActivityStatus;

  @XmlElement(name = "arg2")
  private String producerID;

  @XmlElement(name = "arg3")
  private String producerAgentID;

  @XmlElement(name = "arg4")
  private String submitDateFrom;

  @XmlElement(name = "arg5")
  private String submitDateTo;

  @XmlElement(name = "arg6")
  private String startRecord;

  @XmlElement(name = "arg7")
  private String endRecord;

  // getters
  public String getPdsHandle() {
    return pdsHandle;
  }

  public String getDepositActivityStatus() {
    return depositActivityStatus;
  }

  public String getProducerID() {
    return producerID;
  }

  public String getProducerAgentID() {
    return producerAgentID;
  }

  public String getSubmitDateFrom() {
    return submitDateFrom;
  }

  public String getSubmitDateTo() {
    return submitDateTo;
  }

  public String getStartRecord() {
    return startRecord;
  }

  public String getEndRecord() {
    return endRecord;
  }

  // setters
  public void setPdsHandle(String pdsHandle) {
    this.pdsHandle = pdsHandle;
  }

  public void setDepositActivityStatus(String depositActivityStatus) {
    this.depositActivityStatus = depositActivityStatus;
  }

  public void setProducerID(String producerID) {
    this.producerID = producerID;
  }

  public void setProducerAgentID(String producerAgentID) {
    this.producerAgentID = producerAgentID;
  }

  public void setSubmitDateFrom(String submitDateFrom) {
    this.submitDateFrom = submitDateFrom;
  }

  public void setSubmitDateFrom(Date submitDateFrom) {
    this.submitDateFrom = new SimpleDateFormat(DATE_FORMAT).format(submitDateFrom);
  }

  public void setSubmitDateTo(String submitDateTo) {
    this.submitDateTo = submitDateTo;
  }

  public void setSubmitDateTo(Date submitDateTo) {
    this.submitDateTo = new SimpleDateFormat(DATE_FORMAT).format(submitDateTo);
  }

  public void setStartRecord(String startRecord) {
    this.startRecord = startRecord;
  }

  public void setStartRecord(int startRecord) {
    this.startRecord = String.valueOf(startRecord);
  }

  public void setEndRecord(String endRecord) {
    this.endRecord = endRecord;
  }

  public void setEndRecord(int endRecord) {
    this.endRecord = String.valueOf(endRecord);
  }
}
