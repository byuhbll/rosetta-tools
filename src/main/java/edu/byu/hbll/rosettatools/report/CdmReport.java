package edu.byu.hbll.rosettatools.report;

import edu.byu.hbll.rosettatools.client.ie.IeClient;
import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.harvester.CdmHarvester;
import edu.byu.hbll.rosettatools.util.PdsUtil;
import edu.byu.hbll.xml.XmlUtils;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.w3c.dom.Document;

/** @author Joe Larson */
public class CdmReport extends AbstractReport {

  private String rosettaCollection;

  @Override
  public void generateReport(String reportName) throws Exception {
    try {
      connection =
          DriverManager.getConnection(
              "jdbc:oracle:thin:@" + reportDBUri, reportDBUser, reportDBPassword);
    } catch (Exception e) {
      logger.error("Report failed! " + e);
    }

    switch (reportName) {
      case "status":
        generateStatusReport();
        break;
      default:
        logger.error("No report of name \"" + reportName + "\"");
        return;
    }

    connection.close();
  }

  private void generateStatusReport() throws Exception {
    String reportName = "status";
    CdmHarvester harvester = new CdmHarvester();

    Map<String, String> rosettaIes = getRosettaIeMapByCdmId();

    if (rosettaIes.size() == 0) {
      logger.error("No CDM ID's found! Make sure you have chosen a CDM collection.");
      return;
    }

    List<String> allCdmIds = getCdmIds(harvester);

    HSSFSheet sheet = wb.createSheet();
    wb.setSheetName(0, "Report");

    HSSFRow headerRow = sheet.createRow(0);

    headerRow.createCell(0).setCellValue("CDM_COLLECTION");
    headerRow.createCell(1).setCellValue("CDM_ID");
    headerRow.createCell(2).setCellValue("TITLE");
    headerRow.createCell(3).setCellValue("CDM_FULLRS");
    headerRow.createCell(4).setCellValue("CDM_IDENTI");
    headerRow.createCell(5).setCellValue("ROSETTA_IE");
    headerRow.createCell(6).setCellValue("INGEST_DATE");
    headerRow.createCell(7).setCellValue("ROSETTA_FILENAME");
    headerRow.createCell(8).setCellValue("ROSETTA_COLLECTION");
    headerRow.createCell(9).setCellValue("IN_PERMANENT");

    for (int i = 0; i < allCdmIds.size(); i++) {
      String cdmId = allCdmIds.get(i);

      String itemInfo = harvester.stripNonValidXMLCharacters(harvester.getItem(collection, cdmId));
      Document doc = XmlUtils.parse(itemInfo);
      logger.info("Retrieved item info for CDM ID: " + cdmId);
      String fullrs = XmlUtils.xpath("/xml/fullrs", doc).get(0).getTextContent();
      String identi = XmlUtils.xpath("/xml/identi", doc).get(0).getTextContent();

      HSSFRow row = sheet.createRow(i + 1);
      row.createCell(0).setCellValue(collection);
      row.createCell(1).setCellValue(Integer.parseInt(cdmId));
      row.createCell(3).setCellValue(fullrs);
      row.createCell(4).setCellValue(identi);

      if (rosettaIes.keySet().contains(cdmId)) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        String sql =
            "SELECT *" + "FROM ie_in_permanent " + "WHERE pid = '" + rosettaIes.get(cdmId) + "' ";
        PreparedStatement prepared = connection.prepareStatement(sql);
        ResultSet result = prepared.executeQuery();
        result.next();

        row.createCell(2).setCellValue(result.getString("LABEL"));
        row.createCell(5).setCellValue(rosettaIes.get(cdmId));
        row.createCell(6).setCellValue(dateFormat.parse(result.getString("CREATION_DATE")));

        result.close();
        prepared.close();

        String files = "";
        sql =
            "SELECT file_filename "
                + "FROM files_information_view "
                + "WHERE ie_pid = '"
                + rosettaIes.get(cdmId)
                + "'";
        prepared = connection.prepareStatement(sql);

        result = prepared.executeQuery();
        while (result.next()) {
          files +=
              files.equals("")
                  ? result.getString("FILE_FILENAME")
                  : ", " + result.getString("FILE_FILENAME");
        }

        result.close();
        prepared.close();

        if (files.length() > 32767) {
          files = files.substring(0, 32750);
          files += "... (truncated)";
        }

        row.createCell(7).setCellValue(files);
        row.createCell(8).setCellValue(rosettaCollection);
        row.createCell(9).setCellValue("YES");
      } else {
        String title = XmlUtils.xpath("/xml/title", doc).get(0).getTextContent();
        row.createCell(2).setCellValue(title);
        row.createCell(5).setCellValue("--");
        row.createCell(6).setCellValue("--");
        row.createCell(7).setCellValue("--");
        row.createCell(8).setCellValue("--");
        row.createCell(9).setCellValue("NO");
      }
    }

    setStatusReportStyles(sheet, headerRow);

    saveReportToFile(reportName);
  }

  private Map<String, String> getRosettaIeMapByCdmId() throws Exception {
    Map<String, String> rosettaItems = new HashMap<>();
    String pdsHandle = PdsUtil.getPdsHandle(loginUri, user, password, institution);

    String sql =
        "SELECT cm.pid as ie_pid, trim(cv.name) as c_name "
            + "FROM collections_view cv, collection_members_view cm "
            + "WHERE cv.id = cm.collectionid AND trim(cv.name) = '"
            + collection
            + "' AND cv.owner = 'CRS00."
            + institution
            + "' "
            +
            // "AND ROWNUM <= 5 " +
            "ORDER BY ie_pid";
    PreparedStatement prepared = connection.prepareStatement(sql);
    ResultSet result = prepared.executeQuery();

    while (result.next()) {
      String ieId = result.getString("ie_pid");
      rosettaCollection = result.getString("c_name");
      logger.info("Retrieving METS for: " + ieId);
      String rawXml = IeClient.getIEMD(pdsHandle, ieId);
      Document doc = XmlUtils.parse(rawXml);
      String cdmId = XmlUtils.xpath("string(//cdm/@item", doc).get(0).getTextContent();
      if (!cdmId.equals("") && cdmId != null) {
        logger.info("CDM ID for " + ieId + ": " + cdmId);
        rosettaItems.put(cdmId, ieId);
      } else {
        logger.warn("CDM ID for " + ieId + ": " + "Not Found!");
      }
    }
    result.close();
    prepared.close();
    return rosettaItems;
  }

  private List<String> getCdmIds(CdmHarvester harvester) throws Exception {
    List<Item> cdmItemList =
        new ArrayList<>(harvester.list(collection)); // collection, null, null, 1, 10
    Collections.sort(cdmItemList, new Item().new ItemComp());
    List<String> allCdmIds = new ArrayList<>();

    for (Item item : cdmItemList) {
      allCdmIds.add(item.getId());
    }

    return allCdmIds;
  }

  private void setStatusReportStyles(HSSFSheet sheet, HSSFRow headerRow) {

    // 1. set style for date cells
    sheet.setDefaultColumnStyle(6, dateCellStyle);

    // 2. set style for header row cells, overriding previous styles
    Iterator<Cell> headerCellIterator = headerRow.cellIterator();
    while (headerCellIterator.hasNext()) {
      HSSFCell cell = (HSSFCell) headerCellIterator.next();
      cell.setCellStyle(headerCellStyle);
    }

    // 3. highlight rows for items not found in Rosetta
    Iterator<Row> rowIterator = sheet.rowIterator();
    while (rowIterator.hasNext()) {
      HSSFRow row = (HSSFRow) rowIterator.next();
      if (row.getCell(9).getStringCellValue().equals("NO")) {
        Iterator<Cell> cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
          cellIterator.next().setCellStyle(warnCellStyle);
        }
      }
    }

    // 4. auto-resize columns
    for (int i = 0; i < headerRow.getPhysicalNumberOfCells(); i++) {
      sheet.autoSizeColumn(i);
    }
  }

  /**
   * @param reportName
   * @throws IOException
   */
  private void saveReportToFile(String reportName) throws IOException {
    String date = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
    String file =
        "/operational_shared/reports/"
            + collection.toLowerCase()
            + "."
            + reportName
            + "."
            + date
            + ".xls";
    FileOutputStream out = new FileOutputStream(file);
    wb.write(out);
    out.close();
  }
}
