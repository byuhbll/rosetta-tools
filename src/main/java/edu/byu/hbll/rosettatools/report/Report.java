package edu.byu.hbll.rosettatools.report;

/** @author Joe Larson */
public interface Report {

  public void generateReport(String reportName) throws Exception;
}
