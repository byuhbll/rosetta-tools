package edu.byu.hbll.rosettatools.data;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * Field model.
 *
 * @author joelarson
 * @author Ben Welker
 */
@XmlAccessorType(XmlAccessType.NONE)
public class Field implements Serializable {

  private static final long serialVersionUID = 1L;
  public static final String FIELD = "field";
  public static final String NAME = "name";

  @XmlValue() private String field;

  @XmlAttribute(name = NAME)
  private String name;

  public Field(String name, String value) {
    this.name = name;
    this.field = value;
  }

  // getters
  public String getField() {
    return field;
  }

  public String getName() {
    return name;
  }

  // setters
  public void setField(String value) {
    this.field = value;
  }

  public void setName(String name) {
    this.name = name;
  }
}
