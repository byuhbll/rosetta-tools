<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />


<!-- The dublin core metadata -->
<xsl:param name="dc" />

<!-- The metadata from the source -->
<xsl:param name="srcmd" />

<!-- The retention policy as set by user or default from configuration -->
<xsl:param name="retentionPolicy" />

<!-- The rights policy as set by user or default from configuration -->
<xsl:param name="rightsPolicy" />

<xsl:template match="xml">
	<premets>
		<dc>
			<xsl:copy-of select="$dc" />
		</dc>
		<srcmd type="OTHER" otherType="ojs">
			<xsl:copy-of select="$srcmd" />
		</srcmd>
		<files>
			<xsl:apply-templates select="articles/article" />
		</files>
		<retentionPolicy>
			<xsl:copy-of select="$retentionPolicy" />
		</retentionPolicy>
		<rightsPolicy>
			<xsl:copy-of select="$rightsPolicy" />
		</rightsPolicy>
	</premets>
</xsl:template>


<xsl:template match="article">
	<!-- <xsl:message terminate="no"><xsl:copy-of select="pages/text()"/></xsl:message> -->
	<div>
		<xsl:attribute name="label">
			<xsl:value-of select="title/text()"/>
			<xsl:if test="pages/text()">
				<xsl:text> [Pages: </xsl:text>
				<xsl:value-of select="pages/text()"/>
				<xsl:text>]</xsl:text>
			</xsl:if>
		</xsl:attribute>
		<!-- <xsl:attribute name="type" select="section/text()"/> -->
		
		<xsl:apply-templates select="files/file|supp_files/file"/>
	</div>
</xsl:template>


<xsl:template match="file">
	<file>
		<xsl:attribute name="label" select="filename/text()" />
		<xsl:value-of select="filename" />
	</file>
</xsl:template>

</xsl:stylesheet>
