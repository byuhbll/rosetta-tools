/** */
package edu.byu.hbll.rosettatools.harvester;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.sip.SipFactory;
import edu.byu.hbll.xslt.XslTransformer;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Abstract harvester.
 *
 * @author cfd2
 */
public abstract class AbstractHarvester implements Harvester {

  /** The constant logger. */
  static final Logger logger = LoggerFactory.getLogger(AbstractHarvester.class);

  static final ObjectMapper mapper = new ObjectMapper();

  /** The Mets xsl. */
  protected String metsXsl = "xsl/mets.xsl";
  /** The Mets xsl util. */
  protected XslTransformer metsXslUtil;

  /** The No download. */
  protected boolean noDownload = false;
  /** The No submit. */
  protected boolean noSubmit = false;
  /** The Retention. */
  protected String retention = null;
  /** The Rights. */
  protected String rights = null;
  /** The Rosetta collection. */
  protected String rosettaCollection = null;

  /** The Sip factory. */
  protected SipFactory sipFactory;

  public void init(JsonNode node) throws Exception {
    this.setMetsXsl(node.path("metsXsl").asText());
  }

  @Override
  public Set<Item> list(String collection) throws Exception {
    return list(collection, (Date) null, (Date) null, 0, 0);
  }

  @Override
  public void harvest() throws Exception {
    harvest((Date) null, (Date) null, 0, 0);
  }

  @Override
  public void harvest(String collection) throws Exception {
    harvest(collection, (Date) null, (Date) null, 0, 0);
  }

  /**
   * To mets xml.
   *
   * @param premetsXslUtil the premets xsl util
   * @param source the source
   * @param params the params
   * @return string
   * @throws TransformerException the transformer exception
   */
  public String toMetsXml(
      XslTransformer premetsXslUtil, Source source, Map<String, ? extends Object> params)
      throws TransformerException {
    StringWriter premetsWriter = new StringWriter();
    premetsXslUtil.transform(source, new StreamResult(premetsWriter), params);
    String premetsXml = premetsWriter.toString();
    return toMetsXml(new StreamSource(new StringReader(premetsXml)), params);
  }

  /**
   * To mets xml.
   *
   * @param premetsSource the premets source
   * @param params the params
   * @return string
   * @throws TransformerException the transformer exception
   */
  public String toMetsXml(Source premetsSource, Map<String, ? extends Object> params)
      throws TransformerException {
    StringWriter metsWriter = new StringWriter();
    metsXslUtil.transform(premetsSource, new StreamResult(metsWriter), params);
    String metsXml = metsWriter.toString();
    return metsXml;
  }

  // getters
  public String getMetsXsl() {
    return metsXsl;
  }

  public SipFactory getSipFactory() {
    return sipFactory;
  }

  public boolean isNoDownload() {
    return noDownload;
  }

  public boolean isNoSubmit() {
    return noSubmit;
  }

  public String getRetention() {
    return retention;
  }

  public String getRights() {
    return rights;
  }

  public String getRosettaCollection() {
    return rosettaCollection;
  }

  // setters
  public void setSipFactory(SipFactory sipFactory) {
    this.sipFactory = sipFactory;
  }

  public void setNoDownload(boolean noDownload) {
    this.noDownload = noDownload;
  }

  public void setNoSubmit(boolean noSubmit) {
    this.noSubmit = noSubmit;
  }

  public void setRetention(String retention) {
    this.retention = retention;
  }

  public void setRights(String rights) {
    this.rights = rights;
  }

  public void setRosettaCollection(String rosettaCollection) {
    this.rosettaCollection = rosettaCollection;
  }

  public void setMetsXsl(String metsXsl) throws TransformerConfigurationException {
    this.metsXsl = metsXsl;
    this.metsXslUtil = new XslTransformer(new StreamSource(new File(metsXsl)));
  }

  /**
   * Include boolean.
   *
   * @param modified the modified
   * @param from the from
   * @param until the until
   * @return boolean
   */
  public static boolean include(Date modified, Date from, Date until) {
    if (modified == null) {
      return true;
    }

    boolean fromTrue = from == null || modified.compareTo(from) >= 0;
    boolean untilTrue = until == null || modified.compareTo(until) <= 0;
    boolean include = fromTrue && untilTrue;

    return include;
  }
}
