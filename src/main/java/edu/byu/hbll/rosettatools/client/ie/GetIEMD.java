
package edu.byu.hbll.rosettatools.client.ie;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Get IEMD class.
 *
 * @author joelarson
 * @author Ben Welker
 */
@XmlRootElement(name = "getIEMD", namespace = "http://dps.exlibris.com/")
@XmlAccessorType(XmlAccessType.NONE)
public class GetIEMD {

  @XmlElement(name = "pdsHandle")
  private String pdsHandle;

  @XmlElement(name = "iePID")
  private String ieId;

  // getters
  public String getPdsHandle() {
    return pdsHandle;
  }

  public String getIeId() {
    return ieId;
  }

  // setters
  public void setPdsHandle(String pdsHandle) {
    this.pdsHandle = pdsHandle;
  }

  public void setIeId(String ieId) {
    this.ieId = ieId;
  }
}
