package edu.byu.hbll.rosettatools.util;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;

public class PdsUtil {

  public static final Client client = ClientBuilder.newClient();

  public static String getPdsHandle(String uri, String user, String password, String institution)
      throws Exception {
    WebTarget target = client.target(uri);

    Form form = new Form();
    form.param("bor_id", user);
    form.param("bor_verification", password);
    form.param("institute", institution);
    form.param("func", "login");

    String response = target.request().post(Entity.form(form), String.class);
    String pdsHandle = response.replaceAll("(?s).*pds_handle=([^&]+).*", "$1");

    if (pdsHandle.contains("\n")) {
      throw new Exception("Invalid user or password");
    }

    return pdsHandle;
  }
}
