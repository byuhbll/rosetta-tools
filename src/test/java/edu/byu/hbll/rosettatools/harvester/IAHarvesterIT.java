package edu.byu.hbll.rosettatools.harvester;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.sip.Sip;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.junit.Test;

public class IAHarvesterIT {
  CdmHarvester harvester;
  Sip sip;
  String collection;
  String item;
  Date from;
  Date until;
  int startIndex;
  int endIndex;

  public IAHarvesterIT() throws Exception {
    harvester = new CdmHarvester();
    harvester.setHost(System.getProperty("cdmhost"));
    sip = new Sip(Files.createTempDirectory("sipTest").toFile(), collection, item);
    from = new SimpleDateFormat("yyyy-MM-dd").parse("2010-01-01");
    until = new SimpleDateFormat("yyyy-MM-dd").parse("2015-01-01");

    collection = "p15999coll20";
    item = "21961";
    startIndex = 1;
    endIndex = 304; //This is significant as it tests collection total size + 1
  }

  @Test
  public void testCdmHarvester() {
    assertTrue(harvester != null);
  }

  @Test
  public void testList() throws IOException {
    Set<Item> items = harvester.list();
    assertTrue(items != null && !items.isEmpty());
  }

  //	@Test
  //	public void testListWithDefaultParams() throws Exception {
  //		Set<Item> items = harvester.list(collection,from, until, 0, 0);
  //		System.out.println(items.toString());
  //		assertTrue(items != null && !items.isEmpty());
  //	}
  //
  @Test
  public void testListWithPopulatedParamsPass() throws Exception {
    Set<Item> items = harvester.list(collection, from, until, 1, 82);
    System.out.println(items.toString());
    assertTrue(items != null && !items.isEmpty());
  }

  @Test
  public void testListWithPopulatedParams2() throws Exception {
    Set<Item> items = harvester.list("p15999coll24");
    System.out.println(items.toString());
    assertTrue(items != null && !items.isEmpty());
  }

  @Test
  public void testGetItem() {
    String item = harvester.getItem("p15999coll20", "21961");
    assertTrue(
        item.contains(
            "L. Tom Perry Special Collections, Harold B. Lee Library, Brigham Young University"));
  }

  @Test
  public void testGetItems() throws IOException {
    List<String> control = new LinkedList<String>();
    control.add("2361");
    control.add("2490");
    control.add("2604");
    control.add("2838");
    control.add("2897");
    control.add("4888");
    control.add("5185");
    control.add("5203");
    control.add("5227");
    control.add("5613");
    control.add("5846");
    control.add("6426");
    control.add("7141");
    control.add("7148");
    control.add("7399");
    control.add("7628");
    control.add("7668");
    control.add("8337");
    control.add("8343");
    control.add("8376");

    List<String> items = harvester.getItems("p15999coll20", from, until, 1, 100, 20, 303);
    Collections.sort(control);
    Collections.sort(items);
    assertEquals(control.size(), items.size());
    for (int i = 0; i < control.size(); i++) {
      assertTrue(control.contains(items.get(i)));
    }
  }
}
