@XmlSchema(
  xmlns = {
    @XmlNs(namespaceURI = "http://purl.org/dc/elements/1.1/", prefix = "dc"),
    @XmlNs(namespaceURI = "http://purl.org/dc/terms/", prefix = "dcterms"),
  }
)
package edu.byu.hbll.rosettatools.data;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;
