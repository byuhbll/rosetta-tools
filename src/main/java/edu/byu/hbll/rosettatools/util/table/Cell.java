package edu.byu.hbll.rosettatools.util.table;

public interface Cell {

  public String getColumnName();

  public int getIndex();

  public Row getRow();

  public int getRowIndex();

  public Table getTable();

  public Object getValue();

  public void setValue(Object value);
}
