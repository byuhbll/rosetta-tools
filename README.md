# README #

Installation:

1. mkdir -p /srv/java
1. cd /srv/java
1. git clone https://bitbucket.org/byuhbll/rosetta-tools.git && cd rosetta-tools
1. bin/system_setup.sh
1. rosetta-tools-user-setup
1. mvn clean package

Run `rosetta-tools-user-setup` for each user that must run them.
After running `rosetta-tools-user-setup`, run `harvest` to see options.

Modify bin/run.sh as needed to specify the global config location and other options.