package edu.byu.hbll.rosettatools.util.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TableImpl implements Table {

  private Row columnNames = new RowImpl(this, -1);
  private Map<String, Integer> columnIndexes = new HashMap<String, Integer>();
  private ArrayList<Row> rows = new ArrayList<Row>();
  private Map<Integer, Map<Object, Cell>> columnLookUp = new HashMap<Integer, Map<Object, Cell>>();

  @Override
  public Row add() {
    Row row = new RowImpl(this, rows.size());
    rows.add(row);
    return row;
  }

  @Override
  public Cell add(Object value) {
    if (rows.isEmpty()) {
      add();
    }

    return rows.get(rows.size() - 1).add(value);
  }

  @Override
  public Row add(Row row) {
    Row newRow = add();

    for (Cell cell : row) {
      newRow.add(cell);
    }

    return newRow;
  }

  @Override
  public void addColumnName(String columnName) {
    columnIndexes.put(columnName, columnNames.size());
    columnNames.add(columnName);
  }

  @Override
  public void addColumnNames(Collection<String> columnNames) {
    for (String columnName : columnNames) {
      addColumnName(columnName);
    }
  }

  @Override
  public Row get(int index) {
    return rows.get(index);
  }

  @Override
  public Cell get(int rowIndex, int columnIndex) {
    return rows.get(rowIndex).get(columnIndex);
  }

  @Override
  public Cell get(int columnIndex, Object value) {
    Map<Object, Cell> columnMap = columnLookUp.get(columnIndex);

    if (columnMap == null) {
      synchronized (columnLookUp) {
        columnMap = new HashMap<Object, Cell>();
        columnLookUp.put(columnIndex, columnMap);

        for (Row row : this) {
          Cell cell = row.get(columnIndex);

          if (cell != null && cell.getValue() != null) {
            columnMap.put(cell.getValue(), cell);
          }
        }
      }
    }

    return columnMap.get(value);
  }

  @Override
  public String getColumnName(int index) {
    return (String) columnNames.get(index).getValue();
  }

  @Override
  public int getIndex(String columnName) {
    return columnIndexes.get(columnName);
  }

  @Override
  public Iterator<Row> iterator() {
    return rows.iterator();
  }

  @Override
  public Cell set(int rowIndex, int columnIndex, Cell cell) {
    rows.get(rowIndex).set(columnIndex, cell);
    return cell;
  }

  @Override
  public Cell set(int rowIndex, int columnIndex, Object value) {
    Cell cell = rows.get(rowIndex).set(columnIndex, value);
    return cell;
  }

  @Override
  public Row set(int index, Row row) {
    rows.ensureCapacity(index + 1);
    rows.set(index, row);
    return row;
  }

  @Override
  public Cell set(int rowIndex, String columnName, Cell cell) {
    rows.ensureCapacity(rowIndex + 1);
    return null;
  }

  @Override
  public Cell set(int rowIndex, String columnName, Object value) {
    rows.ensureCapacity(rowIndex + 1);
    return null;
  }

  @Override
  public String setColumnName(int index, String columnName) {
    columnNames.set(index, columnName);
    columnIndexes.put(columnName, index);
    return columnName;
  }

  @Override
  public int size() {
    return rows.size();
  }

  @Override
  public String toString() {
    return rows.toString();
  }

  @Override
  public List<String> getColumnNames() {
    List<String> columnNames = new ArrayList<String>();

    for (Cell cell : this.columnNames) {
      columnNames.add(cell.getValue().toString());
    }

    return columnNames;
  }
}
