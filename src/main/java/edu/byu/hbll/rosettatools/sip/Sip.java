package edu.byu.hbll.rosettatools.sip;

import edu.byu.hbll.rosettatools.client.deposit.DepositClient;
import edu.byu.hbll.rosettatools.client.sip.SipClient;
import edu.byu.hbll.rosettatools.util.RosettaXmlUtils;
import edu.byu.hbll.xml.XmlUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPPart;
import org.apache.commons.io.FileUtils;
import org.glassfish.jersey.client.ClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

/**
 * @author cfd2
 * @author ericwnord.com
 */
public class Sip {

  static final Logger logger = LoggerFactory.getLogger(Sip.class);

  /**
   * 2 minute timeout for ContentDM downloads that aren't responsive Without this ContentDM can hang
   * an entire harvest process indefinitely.
   */
  private static int connectTimeout = 120000;

  /** Client staged with connect and read timeouts to negate poor behavior from vendor servers. */
  private static final Client CLIENT =
      ClientBuilder.newBuilder()
          .property(ClientProperties.CONNECT_TIMEOUT, connectTimeout)
          .property(ClientProperties.READ_TIMEOUT, connectTimeout)
          .build();

  private static ExecutorService executor;

  private File dir;
  private File dcFile;
  private File metsFile;
  private File objectDir;

  private String pdsHandle = "";
  private String materialFlowId = "77922";
  private String producerId = "90187";
  private String depositSetId = "1";
  private int timeoutTries = 0;

  private String sipMove;
  private File finalDir;

  /** Collection to harvest. */
  private String collection;

  private String item;

  /** Current download object index. */
  private int downloadIndex = 0;

  /** sip Id. */
  private String sipId;

  /**
   * Constructor.
   *
   * @param dir Sip dir
   * @param collection Collection
   * @param item Item
   */
  public Sip(File dir, String collection, String item) {
    this.dir = dir;
    this.collection = collection;
    this.item = item;
  }

  /**
   * Converts a file to a File with an absolute path.
   *
   * @param file File to convert
   * @return the full file
   */
  public static File getFullFile(File file) {
    File fullFile = file;

    try {
      fullFile = file.getCanonicalFile();
    } catch (IOException e) {
      fullFile = file.getAbsoluteFile();
    }

    return fullFile;
  }

  /**
   * Saves the DC and returns the saved file.
   *
   * @param data Data to save
   * @return the saved file
   * @throws IOException if an error occurs
   */
  public File saveDc(String data) throws IOException {
    File sipMetadata = save(data, dcFile.getPath());
    return sipMetadata;
  }

  /**
   * Saves the Mets and returns the saved file.
   *
   * @param data Data to save
   * @return the saved file
   * @throws IOException if an error occurs
   */
  public File saveMets(String data) throws IOException {
    File sipMetadata = save(data, metsFile.getPath());
    return sipMetadata;
  }

  /**
   * Saves an object and returns the saved file.
   *
   * @param download The File to save
   * @return the saved file
   * @throws IOException if an error occurs
   */
  public File saveObject(File download) throws IOException {
    File object = save(download, new File(objectDir, download.getName()).getPath());
    return object;
  }

  /**
   * Saves an object and returns the saved file.
   *
   * @param object object to save
   * @param name name of the object
   * @return the saved file
   * @throws IOException if an error occurs
   */
  public File saveObject(String object, String name) throws IOException {
    File sipMetadata = save(object, new File(objectDir, name).getPath());
    return sipMetadata;
  }

  /**
   * Saves a copy of an object.
   *
   * @param download File to save
   * @return the saved file
   * @throws IOException if an error occurs
   */
  public File saveObjectCopy(File download) throws IOException {
    File object = saveCopy(download, new File(objectDir, download.getName()).getPath());
    return object;
  }

  /**
   * Saves a file.
   *
   * @param tmpFile the temp file to save
   * @param path the new save location
   * @return the new saved file
   * @throws IOException if an error occurs
   */
  public static File save(File tmpFile, String path) throws IOException {
    File destFile = new File(path);

    if (destFile.exists()) {
      destFile.delete();
    }

    FileUtils.moveFile(tmpFile, destFile);

    return destFile;
  }

  /**
   * Saves data to a file.
   *
   * @param data the data to save
   * @param path the path to save it to
   * @return the new saved file
   * @throws IOException if an error occurs
   */
  public static File save(String data, String path) throws IOException {
    File file = new File(path);
    FileUtils.writeStringToFile(file, data, "UTF-8");
    return file;
  }

  /**
   * Saves a copy of a file.
   *
   * @param tmpFile the file to copy
   * @param path the location to copy the file to
   * @return the new saved file
   * @throws IOException if an error occurs
   */
  public static File saveCopy(File tmpFile, String path) throws IOException {
    File destFile = new File(path);

    if (destFile.exists()) {
      destFile.delete();
    }

    FileUtils.copyFile(tmpFile, destFile);

    return destFile;
  }

  public File downloadObject(String uri, String metaFilename) throws IOException {
    return downloadObject(uri, metaFilename, null);
  }

  public File downloadObject(String uri) throws IOException {
    return downloadObject(uri, null, null);
  }

  /**
   * Download method for all harvesters TODO check for existing file before trying to download
   *
   * @param uri - uri to file location
   * @param metaFilename filename provided to overwrite download filename when masked with
   *     showfile.exe
   * @param md5 the md5 hash to validate against
   * @return File - downloaded file
   * @throws IOException if an error occurs
   */
  public File downloadObject(String uri, String metaFilename, String md5) throws IOException {
    File file = null;
    File tmpFile = null;
    String filename = null;
    String filenameFromHeader = null;

    try {
      WebTarget target = CLIENT.target(uri);
      logger.info("Downloading object: " + target);
      Response response = target.request(uri).get();
      tmpFile = response.readEntity(File.class);

      //Checks response for expected values and warns if variant encountered.
      String filenameHeader = response.getStringHeaders().getFirst("Content-disposition");

      // Resolve appropriate filename
      filenameFromHeader = response.getStringHeaders().getFirst("Content-disposition");

      if (filenameFromHeader != null) {
        if (response.getStringHeaders().getFirst("Content-disposition").equals("")) {
          logger.warn(
              "Unexpected response from ContentDM webservices: "
                  + response.readEntity(String.class));
        }
        filename = filenameFromHeader.replaceAll(".*filename=\"(.*?)\".*", "$1");
      } else {
        filename = target.getUri().getPath().replaceAll(".*/", "");
        filename = filename.equals("showfile.exe") ? metaFilename : filename;
      }

      if (filename == null || filename.isEmpty()) {
        String extension = getFileExtension(tmpFile);
        filename = dir.getName() + "-file." + (downloadIndex) + "." + extension;
      }

      //Check for md5 match
      if (md5 != null) {
        String downloadMd5 = getChecksum(tmpFile);

        if (downloadMd5.equals(md5)) {
          logger.info("MD5 hashes matched!");
          logger.debug("SRC-MD5: " + md5);
          logger.debug("DST-MD5: " + downloadMd5);
        } else {
          logger.info("MD5 hashes did not match.");
          logger.debug("SRC-MD5: " + md5);
          logger.debug("DST-MD5: " + downloadMd5);
          logger.warn("Unable to verify object! Check item: " + filename);
        }
      }

      //Save and return file
      file = save(tmpFile, new File(objectDir, filename).getPath());
      logger.info("Saved object to: " + getFullFile(file).getPath());

      downloadIndex++;
      timeoutTries = 0;
      return file;

    } catch (SocketTimeoutException | ProcessingException s) {

      //Retries the download 3 times then skips it some files in ContentDM are corrupt and
      //will not download
      if (timeoutTries++ < 3) {
        logger.warn("Timeout encountered: retrying download " + uri);
        return downloadObject(uri, metaFilename, md5);
      } else {
        logger.error("Too many timeouts: download skipped " + uri);
        file = save(tmpFile, new File(objectDir, filename).getPath());
        return file;
      }
    } catch (Exception e) {
      logger.error("Downloading object " + uri + " resulted in an error " + e);
      file = save(tmpFile, new File(objectDir, filename).getPath());
      return file;
    }
  }

  /**
   * Calculates the MD5 checksum of a file.
   *
   * @param file the file to check
   * @return the MD5 checksum
   */
  public String getChecksum(File file) {
    try {
      InputStream fin = new FileInputStream(file);
      java.security.MessageDigest md5er = MessageDigest.getInstance("MD5");
      byte[] buffer = new byte[1024];
      int read;
      do {
        read = fin.read(buffer);
        if (read > 0) {
          md5er.update(buffer, 0, read);
        }
      } while (read != -1);
      fin.close();
      byte[] digest = md5er.digest();
      if (digest == null) {
        return null;
      }
      String strDigest = "";
      for (int i = 0; i < digest.length; i++) {
        strDigest += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1).toLowerCase();
      }
      return strDigest;
    } catch (Exception e) {
      logger.error("Checksum could not be established" + e);
      return null;
    }
  }

  /** Submits this sip to Rosetta. */
  public void submit() {

    logger.info("Submitting " + dir.getPath() + " to Rosetta");

    try {
      SOAPPart response =
          DepositClient.submitDepositActivity(
              pdsHandle, materialFlowId, dir.getName(), producerId, depositSetId);
      Document xml = XmlUtils.marshal(response);
      String depositXml = RosettaXmlUtils.evaluate("//DepositResult", xml);
      Document depositResult = XmlUtils.parse(depositXml);
      String isError = RosettaXmlUtils.evaluate("/deposit_result/is_error", depositResult);

      if (sipMove != null && !sipMove.isEmpty()) {
        finalDir = new File(dir.getParentFile(), sipMove);
      }

      if (isError.equalsIgnoreCase("true")) {
        String messageCode =
            RosettaXmlUtils.evaluate("/deposit_result/message_code", depositResult);
        String messageDesc =
            RosettaXmlUtils.evaluate("/deposit_result/message_desc", depositResult);
        logger.error(
            "Error depositing "
                + dir.getName()
                + " into Rosetta - Code: "
                + messageCode
                + ", Message: "
                + messageDesc);
      } else {
        sipId = RosettaXmlUtils.evaluate("/deposit_result/sip_id", depositResult);
        String activityId =
            RosettaXmlUtils.evaluate("/deposit_result/deposit_activity_id", depositResult);

        if (finalDir != null) {

          // Process sip to rosetta take a while
          // this waits until the sip processing is done then moves dir
          if (executor != null) {
            executor.submit(new SipMoveJob(this));
          }
        }

        logger.info(
            dir.getName()
                + " deposited into Rosetta - SIP ID: "
                + sipId
                + ", Activity ID: "
                + activityId);
      }
    } catch (Exception e) {
      logger.error("Error depositing " + dir.getName() + " into Rosetta - " + e);
    }
  }

  /**
   * Gets the status of this sip.
   *
   * @return the status
   */
  public Map<String, String> getStatus() {
    try {
      return SipClient.getStatus(sipId);
    } catch (SOAPException | JAXBException | InterruptedException e) {
      logger.error(e.toString());
    }
    return null;
  }

  /**
   * Gets the object files.
   *
   * @param metadataFile Metadata files to check
   * @return the object files
   */
  public Set<File> getObjectFiles(String... metadataFile) {

    Set<File> metadataFiles = new HashSet<File>();

    for (String filename : metadataFile) {
      metadataFiles.add(new File(getObjectDir(), filename));
    }

    Set<File> objectFiles = new TreeSet<File>();

    for (File file : getObjectDir().listFiles()) {
      if (!metadataFiles.contains(file)) {
        objectFiles.add(file);
      }
    }

    return objectFiles;
  }

  @Override
  public String toString() {
    return dir.getPath();
  }

  /**
   * Gets the file extension of the provided file.
   *
   * @param file File to check
   * @return the file extension
   * @throws IOException if an error occurs
   */
  public static String getFileExtension(File file) throws IOException {
    return getFileExtension(Files.probeContentType(Paths.get(file.getPath())));
  }

  /**
   * Gets the file extension for the provided content type.
   *
   * @param contentType the content type
   * @return the file extension
   */
  public static String getFileExtension(String contentType) {
    switch (contentType) {
      case "application/pdf":
        return "pdf";
      case "application/xml":
        return "xml";
      case "text/html":
        return "html";
      case "image/jp2":
        return "jp2";
      case "image/jpeg":
        return "jpg";
      case "application/x-shockwave-flash":
        return "swf";
      case "image/tiff":
        return "tif";
      case "text/plain":
        return "txt";
      case "audio/x-wav":
        return "wav";
      default:
        return "bin";
    }
  }

  /** Stars the movie executor. */
  public static synchronized void startMoveExecutor() {
    shutdownMoveExecutor();
    executor = Executors.newFixedThreadPool(4);
  }

  /** Stops the move executor. */
  public static synchronized void shutdownMoveExecutor() {
    if (executor != null) {
      executor.shutdown();

      try {
        while (!executor.awaitTermination(1, TimeUnit.DAYS)) {
          //Do Nothing
        }
      } catch (InterruptedException e) {
        //Do Nothing
      }
    }
  }

  // getters
  public File getDir() {
    return dir;
  }

  public String getPdsHandle() {
    return pdsHandle;
  }

  public String getMaterialFlowId() {
    return materialFlowId;
  }

  public String getProducerId() {
    return producerId;
  }

  public String getDepositSetId() {
    return depositSetId;
  }

  public int getDownloadIndex() {
    return downloadIndex;
  }

  public File getDcFile() {
    return dcFile;
  }

  public File getMetsFile() {
    return metsFile;
  }

  public File getObjectDir() {
    return objectDir;
  }

  public String getCollection() {
    return collection;
  }

  public String getItem() {
    return item;
  }

  public String getSipMove() {
    return sipMove;
  }

  public File getFinalDir() {
    return finalDir;
  }

  // setters
  public void setDir(File dir) {
    this.dir = dir;
  }

  public void setPdsHandle(String pdsHandle) {
    this.pdsHandle = pdsHandle;
  }

  public void setMaterialFlowId(String materialFlowId) {
    this.materialFlowId = materialFlowId;
  }

  public void setProducerId(String producerId) {
    this.producerId = producerId;
  }

  public void setDepositSetId(String depositSetId) {
    this.depositSetId = depositSetId;
  }

  public void setDownloadIndex(int downloadIndex) {
    this.downloadIndex = downloadIndex;
  }

  public void setDcFile(File dcFile) {
    this.dcFile = dcFile;
  }

  public void setMetsFile(File metsFile) {
    this.metsFile = metsFile;
  }

  public void setObjectDir(File objectDir) {
    this.objectDir = objectDir;
  }

  public void setCollection(String collection) {
    this.collection = collection;
  }

  public void setItem(String item) {
    this.item = item;
  }

  public void setSipMove(String sipMove) {
    this.sipMove = sipMove;
  }

  public void setFinalDir(File finalDir) {
    this.finalDir = finalDir;
  }
}
