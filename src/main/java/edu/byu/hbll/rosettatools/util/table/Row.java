package edu.byu.hbll.rosettatools.util.table;

public interface Row extends Iterable<Cell> {

  public Cell add(Object value);

  public Cell get(int index);

  public Cell get(String columnName);

  public int getIndex();

  public Table getTable();

  public Cell set(int index, Cell cell);

  public Cell set(int index, Object value);

  public Cell set(String columnName, Cell cell);

  public Cell set(String columnName, Object value);

  public int size();
}
