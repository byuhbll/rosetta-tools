package edu.byu.hbll.rosettatools.util.table;

public class CellImpl implements Cell {

  private Row row;
  private int index;
  private Object value;

  /**
   * Constructor.
   *
   * @param row the row
   * @param index the index
   * @param value the value
   */
  public CellImpl(Row row, int index, Object value) {
    this.row = row;
    this.index = index;
    this.value = value;
  }

  @Override
  public String getColumnName() {
    return row.getTable().getColumnName(index);
  }

  @Override
  public int getIndex() {
    return index;
  }

  @Override
  public Row getRow() {
    return row;
  }

  @Override
  public int getRowIndex() {
    return row.getIndex();
  }

  @Override
  public Table getTable() {
    return row.getTable();
  }

  @Override
  public Object getValue() {
    return value;
  }

  @Override
  public void setValue(Object value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return value.toString();
  }
}
