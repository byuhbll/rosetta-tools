package edu.byu.hbll.rosettatools.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DmQuery {
  private Pager pager;
  private Records records;

  public Pager getPager() {
    return pager;
  }

  public void setPager(Pager pager) {
    this.pager = pager;
  }

  public Records getRecords() {
    return records;
  }

  public void setRecords(Records records) {
    this.records = records;
  }

  public int getTotal() {
    return this.pager.getTotal();
  }
}
