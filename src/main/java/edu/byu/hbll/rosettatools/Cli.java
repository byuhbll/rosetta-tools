package edu.byu.hbll.rosettatools;

import edu.byu.hbll.json.JsonField;
import edu.byu.hbll.rosettatools.filelist.CdmIdScraper;
import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entry point for rosetta tools. Parses the command line options and loads the configuration.
 *
 * @author Ben Welker
 */
public class Cli {

  private static Logger logger = LoggerFactory.getLogger(Cli.class);
  private static Options options;

  static {
    options = new Options();
    options.addOption("a", "auto-login", false, "do not prompt for login, use auto user");
    options.addOption(
        "c", "collection", true, "rosetta collection (defaults to source collection code)");
    options.addOption(
        "d",
        "difference",
        false,
        "Checks difference between ContentDM list of ids and mets file ids");
    options.addOption("f", "from", true, "harvest from this date (yyyy-MM-dd)");
    options.addOption(
        "g",
        "generate-report",
        false,
        "generate reports with: harvest -g [src] [collection] [report-name]");
    options.addOption("h", "help", false, "print this help");
    options.addOption("k", "keep", true, "retention policy code (default in ./conf/rosetta.yml)");
    options.addOption("l", "list", false, "list configured sources");
    options.addOption(
        "m", "material-flow", true, "material flow id (default in ./conf/rosetta.yml)");
    options.addOption(
        "o", "origin-field", true, "CDM ONLY! Select associated master filename field");
    options.addOption("p", "producer", true, "producer id (default in ./conf/rosetta.yml)");
    options.addOption(
        "r", "rights", true, "rosetta policy rights code (default in ./conf/rosetta.yml)");
    options.addOption("s", "start-index", true, "harvest from this item index (not item ID)");
    options.addOption("t", "through-index", true, "harvest through this item index (not item ID)");
    options.addOption("u", "until", true, "harvest until this date (yyyy-MM-dd)");
    options.addOption("w", "no-download", false, "do not download the object(s)");
    options.addOption("x", "no-submit", false, "do not submit sip to Rosetta");
    options.addOption("y", "yml", true, "YML configuration file (default ./conf/rosetta.yml)");
  }

  /**
   * Main entrypoint for the java application.
   *
   * @param args command line arguments
   */
  public static void main(String[] args) {
    CommandLine cli = null;

    try {
      CommandLineParser parser = new DefaultParser();
      cli = parser.parse(options, args);
    } catch (Exception e) {
      logger.error(e.toString(), e);
      printHelp();
      System.exit(1);
    }

    if (cli.hasOption("h")) {
      printHelp();
      return;
    }

    if (cli.hasOption("d")) {
      try {
        new CdmIdScraper();
      } catch (Exception e) {
        logger.error(e.toString(), e);
      }
      return;
    }

    // Load the configuration
    File configFile = new File(getAppDir(), "config/rosetta.yml");

    if (cli.hasOption("y")) {
      configFile = new File(cli.getOptionValue("y"));
    }

    Configuration config = null;
    try {
      config = loadConfig(configFile);
    } catch (Exception e) {
      logger.error(e.toString(), e);
      logger.error("Error loading the config.");
      System.exit(1);
    }

    SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");

    Date from = null;
    Date until = null;

    if (cli.hasOption('f')) {
      try {
        from = dateParser.parse(cli.getOptionValue('f'));
      } catch (Exception e) {
        logger.error(e.toString(), e);
        logger.error("Error parsing the from date.");
        System.exit(1);
      }
    }

    if (cli.hasOption('u')) {
      try {
        until = dateParser.parse(cli.getOptionValue('u'));
      } catch (Exception e) {
        logger.error(e.toString(), e);
        logger.error("Error parsing the until date");
        System.exit(1);
      }
    }

    String source = null;
    String collection = null;
    String reportName = null;
    List<String> items = new LinkedList<>();
    String user = null;
    String password = null;

    Object[] argc = cli.getArgs();
    for (int i = 0; i < argc.length; i++) {
      String arg = argc[i].toString();
      switch (i) {
        case 0:
          source = arg;
          if (arg.equals("ia")) {
            collection = "ia";
          }
          break;
        case 1:
          if (!source.equals("ia")) {
            collection = arg;
            break;
          }
          //fall through
        default:
          if (cli.hasOption('g')) {
            reportName = arg;
          } else {
            items.add(arg);
          }
      }
    }

    if (source == null && cli.hasOption('l')) {
      printSources(config);
      return;
    }

    if (source == null) {
      logger.error("Must specify a source\nSources:");
      printSources(config);
      System.exit(1);
    }

    // force login to rosetta system if auto user not being used
    // and there is a possibility of a submission
    if ((cli.hasOption('a') || cli.hasOption('x') || cli.hasOption('l') || cli.hasOption('g'))) {
      Console console = System.console();
      console.format("username: ");
      user = console.readLine();
      console.format("password: ");
      password = new String(console.readPassword("password: "));
    }

    // Construct Main to perform the desired execution.
    Main main =
        new Main.Builder(config)
            .noDownload(cli.hasOption('w'))
            .noSubmit(cli.hasOption('x'))
            .from(from)
            .until(until)
            .startIndex(Integer.parseInt(cli.getOptionValue('u', "0")))
            .throughIndex(Integer.parseInt(cli.getOptionValue('t', "0")))
            .rights(cli.getOptionValue('r', null))
            .retention(cli.getOptionValue('k', null))
            .materialFlowId(cli.getOptionValue('m', null))
            .cdmOriginField(cli.getOptionValue('o', null))
            .producerId(cli.getOptionValue('p', null))
            .rosettaCollection(cli.getOptionValue('c', null))
            .source(source)
            .collection(collection)
            .reportName(reportName)
            .items(items)
            .user(user)
            .password(password)
            .generateReport(cli.hasOption('g'))
            .list(cli.hasOption('l'))
            .build();
  }

  /**
   * Loads the configuration.
   *
   * @param configFile The file containing the configuration
   * @return Configuration
   * @throws IOException if an IO error occurs
   */
  private static Configuration loadConfig(File configFile) throws IOException {
    Configuration config = Configuration.getInstance();
    config.loadConfigFrom(Paths.get(configFile.toString()));
    return config;
  }

  /**
   * Gets the directory the app is executing from.
   *
   * @return The App Dir.
   */
  private static String getAppDir() {
    try {
      File loc = new File(System.getProperty("APPDIR", "."));
      return loc.getCanonicalPath();
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /** Prints the help. */
  public static void printHelp() {
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("harvest [OPTION]... <SOURCE> [<COLLECTION> [<ITEM>]...]", options);
  }

  /**
   * prints the sources specified in the config.
   *
   * @param config the config
   */
  public static void printSources(Configuration config) {
    JsonField sources = new JsonField(config.getConfig().path("sources"));
    sources
        .fields()
        .forEach(
            source -> {
              logger.info(" * " + source.getKey() + ":");
              source
                  .fields()
                  .forEach(
                      field -> {
                        logger.info("        " + field.getKey() + ": " + field.getValue());
                      });
            });
  }
}
