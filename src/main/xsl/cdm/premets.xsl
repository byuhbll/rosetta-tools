<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />


<!-- The dublin core metadata -->
<xsl:param name="dc" />

<!-- The metadata from the source -->
<xsl:param name="srcmd" />

<!-- The retention policy as set by user or default from configuration -->
<xsl:param name="retentionPolicy" />

<!-- The rights policy as set by user or default from configuration -->
<xsl:param name="rightsPolicy" />

<xsl:template match="/">
	<premets>
		<dc>
			<xsl:copy-of select="$dc" />
		</dc>
		<srcmd type="OTHER" otherType="cdm">
			<xsl:copy-of select="$srcmd" />
		</srcmd>
		<files>
			<xsl:apply-templates select="cpd/node|cpd/page|xml/find" />
		</files>
		<retentionPolicy>
			<xsl:copy-of select="$retentionPolicy" />
		</retentionPolicy>
		<rightsPolicy>
			<xsl:copy-of select="$rightsPolicy" />
		</rightsPolicy>
	</premets>
</xsl:template>


<xsl:template match="node">
	<div>
		<xsl:attribute name="label" select="nodetitle" />
		<xsl:apply-templates select="node|page"/>
	</div>
</xsl:template>


<xsl:template match="page">
	<file>
		<xsl:attribute name="label" select="pagetitle" />
		<xsl:value-of select="pagefile" />
	</file>
	<file>
		<xsl:attribute name="label" select="pagetitle" />
		<!-- several file types can come in including .jpg .jp2 .tif etc. this scrapes the file extension and replaces it with .xml -->
		<xsl:value-of select="replace(pagefile,'\..+','.xml')" />
	</file>
</xsl:template>


<xsl:template match="find">
	<file>
		<xsl:attribute name="label" select="." />
		<xsl:value-of select="." />
	</file>
</xsl:template>


</xsl:stylesheet>
