package edu.byu.hbll.rosettatools.filelist;

import static java.nio.file.FileVisitResult.CONTINUE;

import com.fasterxml.jackson.core.JsonParseException;
import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.harvester.CdmHarvester;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringJoiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Identifies missing cdm harvest files by collection and Id.
 *
 * @author Eric Nord
 */
public class CdmIdScraper extends SimpleFileVisitor<Path> {
  private final Logger logger = LoggerFactory.getLogger(CdmIdScraper.class);
  private static Path pathToMetsDir;
  private CdmHarvester cdm;

  /**
   * Collects IDs located in the /operational_shared/mets/ file. Existence of IDs in this folder
   * indicate a harvest was running but terminated unexpectedly Final harvesting step is to copy the
   * filed from mets to metsDeposited
   */
  private HashMap<String, Set<Integer>> idsInMets;

  private HashMap<String, Set<Integer>> missingIds;

  /**
   * Creates new CdmIdScraper object @ default path.
   *
   * @throws Exception exception
   */
  public CdmIdScraper() throws Exception {
    pathToMetsDir = Paths.get("/operational_shared/mets");
    missingIds = new HashMap<>();
    this.idsInMets = new HashMap<>();
    cdm = new CdmHarvester();
    run();
  }

  /**
   * MetIdScrapper with custom path.
   *
   * @param path to mets folder
   * @throws Exception exception
   */
  public CdmIdScraper(Path path) throws Exception {
    pathToMetsDir = path;
    missingIds = new HashMap<>();
    this.idsInMets = new HashMap<>();
    cdm = new CdmHarvester();
    run();
  }

  /**
   * Executes scraper.
   *
   * @throws Exception exception
   */
  private void run() throws Exception {
    Files.walkFileTree(pathToMetsDir, this);
    diffForMissingIds();
    logger.info(this.toString());
  }

  /**
   * Given a path - checks for cdm collection names in the path and records existing cdm itemIds per
   * collection.
   */
  @Override
  public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
    final int UP_TO_COLLECTION_FOLDER = 4;
    try {
      String collectionRun = file.getName(file.getNameCount() - UP_TO_COLLECTION_FOLDER).toString();

      //Get collection label only - not the subdivision
      String collection = "";
      if (collectionRun.contains("-")) {
        collection = collectionRun.substring(0, collectionRun.indexOf("-"));
      } else {
        collection = collectionRun;
      }

      Integer itemId = Integer.parseInt(file.getFileName().toString().replaceAll("\\..+", ""));

      //Add mets IDs to the list
      idsInMets.putIfAbsent(collection, new HashSet<>());
      idsInMets.get(collection).add(itemId);
    } catch (NumberFormatException e) {
      //throw away itemIds that can't be converted to numbers and warn
      if (!(file.getFileName().toString().contains("mets.xml")
          || file.getFileName().toString().contains("dc.xml"))) {
        logger.warn("could not identify an itemId from filename: " + file);
      }
    }
    return CONTINUE;
  }

  /**
   * Returns the difference between ids.
   *
   * @throws Exception if any error occurs
   */
  public void diffForMissingIds() throws Exception {
    for (Entry<String, Set<Integer>> entry : idsInMets.entrySet()) {
      String collection = entry.getKey();
      Set<Item> allItems = null;

      //removes all the found metsIds from the fullId list for the collection
      try {
        allItems = cdm.list(collection);
      } catch (JsonParseException e) {
        logger.info(
            collection
                + " found in mets file is not a ContentDM collection or could not obtain a list of ids from contentDM");
        continue;
      }

      Set<Integer> allIds = getIdsFromItems(allItems);
      allIds.removeAll(entry.getValue());

      missingIds.put(collection, allIds);
    }
  }

  private Set<Integer> getIdsFromItems(Set<Item> allItems) {
    Set<Integer> ids = new HashSet<>();
    for (Item item : allItems) {
      ids.add(Integer.parseInt(item.getId()));
    }
    return ids;
  }

  /**
   * Prints missing ids by collection.
   *
   * @return String collection: missingIds[]
   */
  @Override
  public String toString() {
    StringJoiner joiner = new StringJoiner(":");
    for (Entry<String, Set<Integer>> e : missingIds.entrySet()) {
      joiner.add(e.getKey().toString());
      joiner.add(e.getValue().toString() + "\n");
    }
    return joiner.toString();
  }
}
