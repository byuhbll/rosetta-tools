package edu.byu.hbll.rosettatools.harvester;

import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.util.RosettaXmlUtils;
import edu.byu.hbll.xml.XmlUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * The Oai Harvester.
 *
 * @author cfd2
 */
public abstract class OaiHarvester extends AbstractHarvester {

  static final Logger logger = LoggerFactory.getLogger(OaiHarvester.class);

  private static final Client client = ClientBuilder.newClient();

  private String oaiUri;

  @Override
  public void harvest(Date from, Date until, int startIndex, int throughIndex) throws Exception {
    processItems(null, from, until);
  }

  @Override
  public void harvest(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {
    processItems(collection, from, until);
  }

  @Override
  public void harvest(String collection, String item) throws Exception {
    processItem(collection, item);
  }

  /**
   * Processes items matching the given criteria.
   *
   * @param collection the collection to process
   * @param from start date
   * @param until end date
   * @throws Exception if an error occurs
   */
  public void processItems(String collection, Date from, Date until) throws Exception {

    String resumptionToken = null;

    do {
      WebTarget target = client.target(oaiUri);
      target = target.queryParam("verb", "ListRecords");

      if (resumptionToken != null) {
        target = target.queryParam("resumptionToken", resumptionToken);
      } else {
        target = target.queryParam("metadataPrefix", "oai_dc");
        if (collection != null) {
          target = target.queryParam("set", collection);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        if (from != null) {
          target = target.queryParam("from", dateFormatter.format(from));
        }
        if (until != null) {
          target = target.queryParam("until", dateFormatter.format(until));
        }
      }

      Response response = target.request().buildGet().invoke();
      Document xml = XmlUtils.parse(response.readEntity(String.class));

      resumptionToken = RosettaXmlUtils.evaluate("/OAI-PMH/ListRecords/resumptionToken", xml);
      String completeListSize =
          RosettaXmlUtils.evaluate("/OAI-PMH/ListRecords/resumptionToken/@completeListSize", xml);
      String cursor = RosettaXmlUtils.evaluate("/OAI-PMH/ListRecords/resumptionToken/@cursor", xml);

      logger.info("Harvested " + cursor + "/" + completeListSize);

      List<Node> items = XmlUtils.xpath("/OAI-PMH/ListRecords/record", xml);

      for (Node item : items) {
        processItem(item, xml);
      }

    } while (resumptionToken != null && !resumptionToken.isEmpty());
  }

  public abstract void processItem(Node itemNode, Document xml) throws Exception;

  /**
   * Processes a single item.
   *
   * @param collection the collection
   * @param item the item
   * @throws Exception if an error occurs
   */
  public void processItem(String collection, String item) throws Exception {

    WebTarget target = client.target(oaiUri);
    target = target.queryParam("verb", "GetRecord");
    target = target.queryParam("identifier", "oai:ojsspc.lib.byu.edu:article/" + item);
    target = target.queryParam("metadataPrefix", "oai_dc");

    Response response = target.request().buildGet().invoke();

    Document xml = XmlUtils.parse(response.readEntity(String.class));

    List<Node> items = XmlUtils.xpath("/OAI-PMH/GetRecord/record", xml);

    for (Node itemNode : items) {
      processItem(itemNode, xml);
    }
  }

  @Override
  public Set<Item> list() throws Exception {
    Set<Item> collections = new HashSet<Item>();

    String resumptionToken = null;

    do {
      WebTarget target = client.target(oaiUri);
      target = target.queryParam("verb", "ListSets");

      if (resumptionToken != null) {
        target = target.queryParam("resumptionToken", resumptionToken);
      }

      Response response = target.request().buildGet().invoke();

      Document xml = XmlUtils.parse(response.readEntity(String.class));

      resumptionToken = RosettaXmlUtils.evaluate("/OAI-PMH/ListRecords/resumptionToken", xml);
      List<Node> sets = XmlUtils.xpath("/OAI-PMH/ListSets/set", xml);

      for (Node set : sets) {
        String id = RosettaXmlUtils.evaluate("setSpec", set);
        String name = RosettaXmlUtils.evaluate("setName", set);
        String description = RosettaXmlUtils.evaluate("setDescription", set);

        Item collection = new Item(id, name, description);
        collections.add(collection);
      }

    } while (resumptionToken != null && !resumptionToken.isEmpty());

    return collections;
  }

  @Override
  public Set<Item> list(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {

    Set<Item> itemList = new HashSet<Item>();

    String resumptionToken = null;

    do {
      WebTarget target = client.target(oaiUri);
      target = target.queryParam("verb", "ListRecords");

      if (resumptionToken != null) {
        target = target.queryParam("resumptionToken", resumptionToken);
      } else {
        target = target.queryParam("metadataPrefix", "oai_dc");
        if (collection != null) {
          target = target.queryParam("set", collection);
        }

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        if (from != null) {
          target = target.queryParam("from", dateFormatter.format(from));
        }
        if (until != null) {
          target = target.queryParam("until", dateFormatter.format(until));
        }
      }

      Response response = target.request().buildGet().invoke();

      Document xml = XmlUtils.parse(response.readEntity(String.class));

      resumptionToken = RosettaXmlUtils.evaluate("/OAI-PMH/ListRecords/resumptionToken", xml);
      String completeListSize =
          RosettaXmlUtils.evaluate("/OAI-PMH/ListRecords/resumptionToken/@completeListSize", xml);
      String cursor = RosettaXmlUtils.evaluate("/OAI-PMH/ListRecords/resumptionToken/@cursor", xml);

      logger.info("Harvested " + cursor + "/" + completeListSize);

      List<Node> items = XmlUtils.xpath("/OAI-PMH/ListRecords/record", xml);

      for (Node item : items) {
        String id = RosettaXmlUtils.evaluate("header/identifier", item).replaceAll("^.*/", "");
        String name = RosettaXmlUtils.evaluate("metadata/dc/title", item);
        //String description = xml.evaluate("metadata/dc/description", item);

        Item itemObj = new Item(id, name);
        itemList.add(itemObj);
      }

    } while (resumptionToken != null && !resumptionToken.isEmpty());

    return itemList;
  }

  // getters
  public String getOaiUri() {
    return oaiUri;
  }

  // setters
  public void setOaiUri(String oaiUri) {
    this.oaiUri = oaiUri;
  }
}
