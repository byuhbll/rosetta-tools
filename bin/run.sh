#!/bin/bash

# Exit on error
set -e

# Get the project directory 
APPDIR=$(readlink -f "$(dirname $(readlink -f "$0"))"/..)

CONFIG="$APPDIR/config/config.yml"
LOG="/var/log/rosetta-tools/rosharvest.log"
LEVEL="INFO"

# check for JAVA_HOME
if [[ -n "$JAVA_HOME" ]]; then
  JAVA_BIN=$JAVA_HOME/bin/java
else
  JAVA_BIN=java
fi

$JAVA_BIN -Xmx256m -Dfile.encoding=UTF-8 -DAPPDIR="$APPDIR" -Dlog="$LOG" -Dlevel="$LEVEL" -jar ${APPDIR}/target/rosetta-tools.jar "$@"