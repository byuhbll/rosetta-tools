package edu.byu.hbll.rosettatools.client.deposit;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Request for SubmitDepositActivity.
 *
 * @author cfd2
 */
@XmlRootElement(name = "submitDepositActivity", namespace = "http://dps.exlibris.com/")
@XmlAccessorType(XmlAccessType.NONE)
public class SubmitDepositActivityRequest {

  @XmlElement(name = "arg0")
  private String pdsHandle;

  @XmlElement(name = "arg1")
  private String materialFlowId;

  @XmlElement(name = "arg2")
  private String subDirectoryName;

  @XmlElement(name = "arg3")
  private String producerId;

  @XmlElement(name = "arg4")
  private String depositSetId;

  // getters
  public String getPdsHandle() {
    return pdsHandle;
  }

  public String getMaterialFlowId() {
    return materialFlowId;
  }

  public String getSubDirectoryName() {
    return subDirectoryName;
  }

  public String getProducerId() {
    return producerId;
  }

  public String getDepositSetId() {
    return depositSetId;
  }

  // setters
  public void setPdsHandle(String pdsHandle) {
    this.pdsHandle = pdsHandle;
  }

  public void setMaterialFlowId(String materialFlowId) {
    this.materialFlowId = materialFlowId;
  }

  public void setSubDirectoryName(String subDirectoryName) {
    this.subDirectoryName = subDirectoryName;
  }

  public void setProducerId(String producerId) {
    this.producerId = producerId;
  }

  public void setDepositSetId(String depositSetId) {
    this.depositSetId = depositSetId;
  }
}
