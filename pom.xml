<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>edu.byu.hbll</groupId>
    <artifactId>java-pom</artifactId>
    <version>1.5.0</version>
  </parent>

  <artifactId>rosetta-tools</artifactId>
  <version>1.0.0</version>
  <packaging>jar</packaging>

  <name>${project.groupId}:${project.artifactId}</name>
  <description>Adapters and other tools for working with Rosetta</description>
  <url>https://bitbucket.org/byuhbll/rosetta-tools</url>

  <scm>
    <connection>scm:git:git://bitbucket.org/byuhbll/rosetta-tools.git</connection>
    <developerConnection>scm:git:ssh://bitbucket.org:byuhbll/rosetta-tools.git</developerConnection>
    <url>https://bitbucket.org/byuhbll/rosetta-tools</url>
  </scm>

  <properties>
    <jersey.version>2.26</jersey.version>
    <jackson.version>2.9.2</jackson.version>
  </properties>

  <dependencies>
    <!-- logging -->
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <version>1.7.25</version>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>jul-to-slf4j</artifactId>
      <version>1.7.25</version>
    </dependency>
    <dependency>
      <groupId>ch.qos.logback</groupId>
      <artifactId>logback-classic</artifactId>
      <version>1.2.3</version>
    </dependency>

    <!-- Core Dependencies -->
    <dependency>
      <groupId>edu.byu.hbll</groupId>
      <artifactId>config</artifactId>
      <version>3.2.0</version>
    </dependency>
    <dependency>
      <groupId>edu.byu.hbll</groupId>
      <artifactId>xml</artifactId>
      <version>1.0.2</version>
    </dependency>
    <dependency>
      <groupId>edu.byu.hbll</groupId>
      <artifactId>xslt</artifactId>
      <version>1.0.2</version>
    </dependency>
    <dependency>
      <groupId>org.glassfish.jersey.core</groupId>
      <artifactId>jersey-client</artifactId>
      <version>${jersey.version}</version>
    </dependency>
    <dependency>
      <groupId>org.glassfish.jersey.inject</groupId>
      <artifactId>jersey-hk2</artifactId>
      <version>${jersey.version}</version>
    </dependency>
    <dependency>
      <groupId>com.drewnoakes</groupId>
      <artifactId>metadata-extractor</artifactId>
      <version>2.11.0</version>
    </dependency>
    <dependency>
      <groupId>net.sf.supercsv</groupId>
      <artifactId>super-csv</artifactId>
      <version>2.4.0</version>
    </dependency>


    <!-- Apache dependencies -->
    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
      <version>2.6</version>
    </dependency>
    <dependency>
      <groupId>commons-cli</groupId>
      <artifactId>commons-cli</artifactId>
      <version>1.4</version>
    </dependency>
    <dependency>
      <groupId>org.apache.poi</groupId>
      <artifactId>poi</artifactId>
      <version>3.17</version>
    </dependency>
    <dependency>
      <groupId>org.apache.poi</groupId>
      <artifactId>poi-ooxml</artifactId>
      <version>3.17</version>
    </dependency>


    <!-- JSON -->
    <dependency>
      <groupId>edu.byu.hbll</groupId>
      <artifactId>json</artifactId>
      <version>1.2.0</version>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-core</artifactId>
      <version>${jackson.version}</version>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-annotations</artifactId>
      <version>${jackson.version}</version>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>${jackson.version}</version>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.dataformat</groupId>
      <artifactId>jackson-dataformat-yaml</artifactId>
      <version>${jackson.version}</version>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.datatype</groupId>
      <artifactId>jackson-datatype-jdk8</artifactId>
      <version>${jackson.version}</version>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.datatype</groupId>
      <artifactId>jackson-datatype-jsr310</artifactId>
      <version>${jackson.version}</version>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.module</groupId>
      <artifactId>jackson-module-jaxb-annotations</artifactId>
      <version>${jackson.version}</version>
    </dependency>

  </dependencies>
  
  <build>
    <plugins>
      <!-- Disable unnecessary plugins from the parent pom -->
      <plugin>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>2.10.4</version>
        <executions>
          <execution>
            <id>attach-javadocs</id>
            <phase>none</phase>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <version>3.0.1</version>
        <executions>
          <execution>
            <id>attach-sources</id>
            <phase>none</phase>
          </execution>
        </executions>
      </plugin>
      <plugin>
      	<artifactId>maven-jar-plugin</artifactId>
      	<executions>
      	  <execution>
      	    <id>default-jar</id>
      	    <phase>none</phase>
      	  </execution>
      	</executions>
      </plugin>

      <!-- Build a single jar file -->
      <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
        <configuration>
          <archive>
            <manifest>
              <mainClass>edu.byu.hbll.rosettatools.Cli</mainClass>
            </manifest>
          </archive>
          <descriptorRefs>
            <descriptorRef>jar-with-dependencies</descriptorRef>
          </descriptorRefs>
          <finalName>${project.artifactId}</finalName>
          <appendAssemblyId>false</appendAssemblyId>
        </configuration>
        <executions>
          <execution>
            <id>make-assembly</id>
            <phase>package</phase>
            <goals>
              <goal>single</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</project>
