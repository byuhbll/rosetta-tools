<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />
	
<xsl:param name="collection" />
<xsl:param name="item" />
<xsl:param name="fieldMap" />
<xsl:param name="cpd" />
<xsl:variable name="dcFields" select="document('dcFields.xml')" />


<!-- root xml element -->
<xsl:template match="xml">
	<cdm>
		<xsl:attribute name="collection" select="$collection" />
		<xsl:attribute name="item" select="$item" />
		<xsl:copy>
			<xsl:apply-templates select="*"/>
		</xsl:copy>
		<xsl:copy-of select="$cpd" />
	</cdm>
</xsl:template>


<!-- enrich each field with a name and dublin core fields -->
<xsl:template match="*">
	<xsl:variable name="nick" select="name()" />
	<xsl:variable name="cdmField" select="$fieldMap/fields/field[nick=$nick]" />
	<xsl:variable name="dcField" select="$dcFields/fields/field[nick=$cdmField/dc]" />
	
	<xsl:copy>
		<xsl:if test="$cdmField">
			<xsl:attribute name="name" select="$cdmField/name" />
		</xsl:if>
		
		<xsl:if test="$dcField">
			<xsl:attribute name="dc" select="$dcField/dc" />
			<xsl:attribute name="dcterms" select="$dcField/dcterms" />
		</xsl:if>
		
		<xsl:copy-of select="@*|node()" />
	</xsl:copy>
</xsl:template>


</xsl:stylesheet>
