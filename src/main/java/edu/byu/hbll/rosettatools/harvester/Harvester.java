package edu.byu.hbll.rosettatools.harvester;

import edu.byu.hbll.rosettatools.data.Item;
import java.util.Date;
import java.util.Set;

/**
 * Harvester interface.
 *
 * @author cfd2
 */
public interface Harvester {

  /**
   * Returns all collections for the given source.
   *
   * @return Set of Items
   * @throws Exception if an error occurs
   */
  public Set<Item> list() throws Exception;

  /**
   * Returns all items for a given collection.
   *
   * @param collection Collection to list
   * @return Set of Items
   * @throws Exception if an error occurs
   */
  public Set<Item> list(String collection) throws Exception;

  /**
   * Returns all items for a given collection.
   *
   * @param collection Collection to list
   * @param from Start date
   * @param until End date
   * @param startIndex Start index
   * @param throughIndex End index
   * @return Set of Items
   * @throws Exception if an error occurs
   */
  public Set<Item> list(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception;

  /**
   * Runs the harvest.
   *
   * @throws Exception if an error occurs
   */
  public void harvest() throws Exception;

  /**
   * Runs the harvest.
   *
   * @param from Start date
   * @param until End date
   * @param startIndex Start index
   * @param throughIndex End index
   * @throws Exception if an error occurs
   */
  public void harvest(Date from, Date until, int startIndex, int throughIndex) throws Exception;

  /**
   * Runs the harvest.
   *
   * @param collection The collection to harvest
   * @throws Exception if an error occurs
   */
  public void harvest(String collection) throws Exception;

  /**
   * Harvests from the specified collection.
   *
   * @param collection Collection to harvest
   * @param from Start date
   * @param until End date
   * @param startIndex Start index
   * @param throughIndex End index
   * @throws Exception if an error occurs
   */
  public void harvest(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception;

  /**
   * Harvests an item from a collection.
   *
   * @param collection The collection to harvest
   * @param item The item to harvest
   * @throws Exception if an error occurs
   */
  public void harvest(String collection, String item) throws Exception;
}
