package edu.byu.hbll.rosettatools.data;

import java.math.BigDecimal;
import java.util.Comparator;

/**
 * Item model.
 *
 * @author cfd2
 * @author Ben Welker
 */
public class Item {

  private String id;
  private String name;
  private String description;

  public Item() {
    this(null, null, null);
  }

  public Item(String id) {
    this(id, null, null);
  }

  public Item(String id, String name) {
    this(id, name, null);
  }

  /**
   * Constructs a new instance.
   *
   * @param id the id
   * @param name the name
   * @param description the description
   */
  public Item(String id, String name, String description) {
    this.id = id;
    this.name = name;
    this.description = description;
  }

  // getters
  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  // setters
  public void setId(String id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int hashCode() {
    return this.id.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o instanceof Item) {
      Item otherCollec = (Item) o;
      return this.id.equals(otherCollec.getId());
    }

    return false;
  }

  public class ItemComp implements Comparator<Item> {

    @Override
    public int compare(Item o1, Item o2) {

      String id1 = o1.getId();
      String id2 = o2.getId();

      if (id1 == null && id2 == null) {
        return 0;
      } else if (id1 == null) {
        return 1;
      } else if (id2 == null) {
        return -1;
      }

      // try comparing numerically first
      try {
        return new BigDecimal(id1).compareTo(new BigDecimal(id2));
      } catch (NumberFormatException e) {
        //Do Nothing
      }

      return id1.compareToIgnoreCase(id2);
    }
  }

  @Override
  public String toString() {
    return "Item [id=" + id + ", name=" + name + ", description=" + description + "]";
  }
}
