package edu.byu.hbll.rosettatools.model;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Records {
  private String collection;
  private int pointer;
  private String filetype;
  private int parentobject;
  private LocalDate dmmodified;
  private String find;

  public String getCollection() {
    return collection;
  }

  public void setCollection(String collection) {
    this.collection = collection;
  }

  public int getPointer() {
    return pointer;
  }

  public void setPointer(int pointer) {
    this.pointer = pointer;
  }

  public String getFiletype() {
    return filetype;
  }

  public void setFiletype(String filetype) {
    this.filetype = filetype;
  }

  public int getParentobject() {
    return parentobject;
  }

  public void setParentobject(int parentobject) {
    this.parentobject = parentobject;
  }

  public LocalDate getDmmodified() {
    return dmmodified;
  }

  public void setDmmodified(LocalDate dmmodified) {
    this.dmmodified = dmmodified;
  }

  public String getFind() {
    return find;
  }

  public void setFind(String find) {
    this.find = find;
  }
}
