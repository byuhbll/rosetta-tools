<?xml version="1.0" encoding="UTF-8"?>

<!-- Make sure dcterms is there or the submission will fail -->
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:dcterms="http://purl.org/dc/terms/"
	xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
	xpath-default-namespace="http://www.openarchives.org/OAI/2.0/"
	exclude-result-prefixes="oai_dc">

<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />



<xsl:template match="record">
	<dc:record><xsl:apply-templates select="metadata/oai_dc:dc/dc:*" /></dc:record>
</xsl:template>


<xsl:template match="dc:*">
	<xsl:element name="{name()}"><xsl:value-of select="." /></xsl:element>
</xsl:template>


</xsl:stylesheet>
