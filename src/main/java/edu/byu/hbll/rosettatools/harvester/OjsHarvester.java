package edu.byu.hbll.rosettatools.harvester;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.sip.Sip;
import edu.byu.hbll.xml.XmlUtils;
import edu.byu.hbll.xslt.XslTransformer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * OjsHarvester class
 *
 * <p>OjsHarvester connects to OJS's Json Export Plugin to harvest pdfs and meta data to be ingested
 * into Rosetta
 *
 * <p>get collections https://ojs.lib.byu.edu/spc/plugins/importexport/json/list.php
 *
 * <p>get items in collection
 * https://ojs.lib.byu.edu/spc/plugins/importexport/json/list.php?journal=CBPR
 *
 * <p>get item info https://ojs.lib.byu.edu/spc/plugins/importexport/json/export.php?issues=1604
 *
 * @author joelarson
 */
public class OjsHarvester extends AbstractHarvester {

  /** The constant logger. */
  static final Logger logger = LoggerFactory.getLogger(OjsHarvester.class);

  private static final Client client = ClientBuilder.newClient();

  private String dcXsl = "/srv/utils/rosettaharvest/xsl/ojsnew/dc.xsl";
  private String ojsEnrichXsl = "/srv/utils/rosettaharvest/xsl/ojsnew/ojsEnrich.xsl";
  private String premetsXsl = "/srv/utils/rosettaharvest/xsl/ojsnew/premets.xsl";
  private String dcFields = "/srv/utils/rosettaharvest/xsl/ojsnew/dcFields.xml";
  private String host = "https://ojs.lib.byu.edu/spc/";

  private XslTransformer dcXslUtil;
  private XslTransformer premetsXslUtil;
  private XslTransformer ojsEnrichXslUtil;

  private HashSet<Item> collections;

  /**
   * Instantiates a new Ojs harvester.
   *
   * @throws IOException the iO exception
   */
  public OjsHarvester() throws IOException {}

  private HashSet<Item> getCollections() throws IOException {
    WebTarget target = client.target(host + "plugins/importexport/json/list.php");
    Response response = target.request().buildGet().invoke();
    JsonNode node = mapper.readTree(response.readEntity(String.class));

    HashSet<Item> collections = new HashSet<Item>();

    for (JsonNode collection : node) {
      String id = collection.get("alias").asText();
      String name = collection.get("name").asText();

      Item collecn = new Item(id, name);
      collections.add(collecn);
    }
    return collections;
  }

  @Override
  public Set<Item> list() throws IOException {
    this.collections = getCollections();
    return collections;
  }

  @Override
  public Set<Item> list(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {

    List<String> pointers = null;

    Set<Item> items = new HashSet<Item>();

    pointers = getItems(collection, from, until);

    for (String pointer : pointers) {
      Item item = new Item(pointer);
      items.add(item);
    }

    return items;
  }

  @Override
  public void harvest(Date from, Date until, int startIndex, int throughIndex) throws Exception {
    for (Item collection : list()) {
      harvest(collection.getId(), from, until, startIndex, throughIndex);
    }
  }

  @Override
  public void harvest(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {
    Node fieldMap = null;
    try (FileInputStream in = new FileInputStream(dcFields)) {
      fieldMap = XmlUtils.parse(in);
    }

    for (Item item : list(collection, from, until, startIndex, throughIndex)) {
      harvest(collection, item.getId(), fieldMap);
    }
  }

  @Override
  public void harvest(String collection, String item)
      throws IOException, TransformerException, SAXException {
    Document doc = null;
    try (FileInputStream in = new FileInputStream(dcFields)) {
      doc = XmlUtils.parse(in);
    }
    harvest(collection, item, doc);
  }

  /**
   * Harvests an item from OJS.
   *
   * @param collection the collection
   * @param item the item
   * @param fieldMap the fieldMap
   * @throws IOException if an error occurs
   * @throws TransformerException if an error occurs
   */
  private void harvest(String collection, String item, Node fieldMap)
      throws IOException, TransformerException {

    logger.info("Harvesting item: " + collection + "/" + item);

    // 1. Get the issue json from ojs
    JsonNode json = mapper.readTree(getItem(collection, item)).get(0);

    // 2. Create SIP
    Sip sip = sipFactory.newInstance(collection, item);

    // 4. Process issue object
    Node doc = processIssue(sip, collection, json);

    // 3. Create params map
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("collection", rosettaCollection);
    params.put("item", item);
    params.put("retentionPolicy", getRetention());
    params.put("rightsPolicy", getRights());

    // 6. Enrich the issue with name and dc counterparts
    DOMSource xmlSource = new DOMSource(doc);
    DOMResult ojsResult = new DOMResult();
    ojsEnrichXslUtil.transform(xmlSource, ojsResult, params);

    Node ojs = ojsResult.getNode();
    params.put("srcmd", xmlSource);

    // 7. Convert to actual dc
    DOMResult dcResult = new DOMResult();
    dcXslUtil.transform(new DOMSource(ojs), dcResult, params);
    Node dc = dcResult.getNode();
    params.put("dc", dc);

    // 8. Save the metadata for the SIP (not the item)
    sip.saveDc(XmlUtils.toString(dc, true));

    // 9. Create the mets document
    String metsXsml = toMetsXml(premetsXslUtil, xmlSource, params);
    sip.saveMets(metsXsml);

    logger.info("Harvested item: " + collection + "/" + item);

    // 10. Submit the SIP
    if (noSubmit) {
      logger.warn("Skipping submission to Rosetta");
    } else {
      sip.submit();
    }
  }

  /**
   * Process issue.
   *
   * @param sip the sip
   * @param collection the collection
   * @param json the json
   * @return document document
   * @throws IOException the iO exception
   */
  public Document processIssue(Sip sip, String collection, JsonNode json) throws IOException {
    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = null;
    try {
      docBuilder = docFactory.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
    Document doc = docBuilder.newDocument();
    Element rootElement = doc.createElement("xml");
    doc.appendChild(rootElement);
    jsonToXml(json, doc, rootElement);
    if (!noDownload) {
      for (JsonNode article : json.get("articles")) {
        downloadArticleFiles(sip, article, collection);
      }
    } else {
      logger.warn("Skipping downloading of object");
    }
    return doc;
  }

  private void jsonToXml(JsonNode json, Document doc, Element parent) {
    if (!json.isArray()) {
      Iterator<String> fieldNames = json.fieldNames();
      while (fieldNames.hasNext()) {
        String field = fieldNames.next();
        if (!json.get(field).isTextual()) {
          Element element = doc.createElement(field);
          parent.appendChild(element);
          jsonToXml(json.get(field), doc, element);
        } else {
          Element element = doc.createElement(field);
          element.appendChild(doc.createTextNode(json.get(field).textValue()));
          parent.appendChild(element);
        }
      }
    } else {
      for (JsonNode node : json) {
        String parentName = parent.getTagName();
        Element element = doc.createElement(parentName.substring(0, parentName.length() - 1));
        parent.appendChild(element);
        jsonToXml(node, doc, element);
      }
    }
  }

  /**
   * Download article files.
   *
   * @param sip the sip
   * @param article the article
   * @param collection the collection
   * @throws IOException the iO exception
   */
  public void downloadArticleFiles(Sip sip, JsonNode article, String collection)
      throws IOException {
    for (JsonNode fileInfo : article.get("files")) {
      sip.downloadObject(fileInfo.get("url").asText(), null);
    }
    for (JsonNode fileInfo : article.get("supp_files")) {
      sip.downloadObject(fileInfo.get("url").asText(), null);
    }
  }

  /**
   * Gets item.
   *
   * @param collection the collection
   * @param item the item
   * @return the item
   */
  public String getItem(String collection, String item) {

    logger.debug("Getting item info: " + collection + "/" + item);

    WebTarget target = client.target(host + "plugins/importexport/json/export.php?issues=" + item);

    Response response = target.request().buildGet().invoke();
    String itemInfo = response.readEntity(String.class);

    logger.debug(
        "Received item info: " + collection + "/" + item + ": " + itemInfo.length() + " bytes");

    return itemInfo;
  }

  /**
   * Gets items.
   *
   * @param collection the collection
   * @param from the from
   * @param until the until
   * @return 7688 : Volume 1 Number 26
   * @throws IOException the iO exception
   */
  public List<String> getItems(String collection, Date from, Date until) throws IOException {

    WebTarget target =
        client.target(host + "plugins/importexport/json/list.php?journal=" + collection);
    Response response = target.request().buildGet().invoke();

    JsonNode issues = mapper.readTree(response.readEntity(String.class));

    List<String> pointers = new ArrayList<String>();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    for (JsonNode issue : issues) {
      String pointer = issue.get("id").asText();

      if (from != null || until != null) {
        try {
          String rawModified = issue.get("date_published").asText();
          Date modified = dateFormat.parse(rawModified);

          boolean fromCheck = from == null || modified.compareTo(from) >= 0;
          boolean untilCheck = until == null || modified.compareTo(until) <= 0;

          if (!(fromCheck && untilCheck)) {
            continue;
          }
        } catch (Exception e) {
          logger.error(
              "Unable to determine published date for " + collection + "/" + pointer + ": " + e);
          continue;
        }
      }
      pointers.add(pointer);
    }

    return pointers;
  }

  /**
   * Gets host.
   *
   * @return the host
   */
  // getters
  public String getHost() {
    return host;
  }

  /**
   * Gets dc xsl.
   *
   * @return the dc xsl
   */
  public String getDcXsl() {
    return dcXsl;
  }

  /**
   * Gets xsl.
   *
   * @return the xsl
   */
  public String getpremetsXsl() {
    return premetsXsl;
  }

  /**
   * Gets ojs enrich xsl.
   *
   * @return the ojs enrich xsl
   */
  public String getOjsEnrichXsl() {
    return ojsEnrichXsl;
  }

  /**
   * Gets dc fields.
   *
   * @return the dc fields
   */
  public String getDcFields() {
    return dcFields;
  }

  /**
   * Sets host.
   *
   * @param host the host
   */
  // setters
  public void setHost(String host) {
    this.host = host;
  }

  /**
   * Sets dc fields.
   *
   * @param dcFields the dc fields
   */
  public void setDcFields(String dcFields) {
    this.dcFields = dcFields;
  }

  /**
   * Sets dc xsl.
   *
   * @param dcXsl the dc xsl
   * @throws TransformerConfigurationException the transformer configuration exception
   */
  public void setDcXsl(String dcXsl) throws TransformerConfigurationException {
    this.dcXsl = dcXsl;
    this.dcXslUtil = new XslTransformer(new StreamSource(new File(dcXsl)));
  }

  /**
   * Sets premets xsl.
   *
   * @param premetsXsl the premets xsl
   * @throws TransformerConfigurationException the transformer configuration exception
   */
  public void setPremetsXsl(String premetsXsl) throws TransformerConfigurationException {
    this.premetsXsl = premetsXsl;
    this.premetsXslUtil = new XslTransformer(new StreamSource(new File(premetsXsl)));
  }

  /**
   * Sets ojs enrich xsl.
   *
   * @param ojsEnrichXsl the ojs enrich xsl
   * @throws TransformerConfigurationException the transformer configuration exception
   */
  public void setOjsEnrichXsl(String ojsEnrichXsl) throws TransformerConfigurationException {
    this.ojsEnrichXsl = ojsEnrichXsl;
    this.ojsEnrichXslUtil = new XslTransformer(new StreamSource(new File(ojsEnrichXsl)));
  }

  /**
   * Gets key part.
   *
   * @param file the file
   * @return key part
   */
  public static String getKeyPart(File file) {
    return FilenameUtils.removeExtension(file.getName()).toLowerCase();
  }
}
