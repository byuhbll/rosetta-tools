package edu.byu.hbll.rosettatools;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.config.YamlLoader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Path;
import java.util.Map;

public class Configuration {

  private static final Configuration instance = new Configuration();

  public static Configuration getInstance() {
    return instance;
  }

  private JsonNode config;

  private Configuration() {}

  public void loadConfigFrom(Path... configFiles) throws IOException {
    config = new YamlLoader().load(configFiles);
  }

  public JsonNode getConfig() {
    return config;
  }

  /**
   * Uses reflection to set properties specified on the subject and in the config on the subject as
   * they are specified in the config.
   *
   * @param subject The object to set properties on.
   * @param path The Json Pointer expression to get properties from in the config.
   * @throws IllegalAccessException If a reflection error occurs.
   * @throws IllegalArgumentException If a reflection error occurs.
   * @throws InvocationTargetException If a reflection error occurs.
   */
  public void setProperties(Object subject, String path)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

    JsonNode node = config.at(path);

    for (Method method : subject.getClass().getMethods()) {
      String methodName = method.getName();

      if (isMethodSetter(method)) {

        String key = methodName.substring(3, 4).toLowerCase() + methodName.substring(4);
        Class<?> destClass = method.getParameterTypes()[0];
        JsonNode property = node.get(key);
        if (property != null) {
          Object value = convert(property, destClass);
          method.invoke(subject, value);
        }
      }
    }
  }

  /**
   * Uses reflection to set the properties provided on the subject.
   *
   * @param subject The object to set properties on.
   * @param properties The properties to set.
   * @throws IllegalAccessException If a reflection error occurs.
   * @throws IllegalArgumentException If a reflection error occurs.
   * @throws InvocationTargetException If a reflection error occurs.
   */
  public static void setProperties(Object subject, Map<String, Object> properties)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    for (Method method : subject.getClass().getMethods()) {
      String methodName = method.getName();

      if (isMethodSetter(method)) {

        String key = methodName.substring(3, 4).toLowerCase() + methodName.substring(4);
        Object property = properties.get(key);

        Class<?> destClass = method.getParameterTypes()[0];

        if (property != null) {
          Object propertyToSet = convert(property, destClass);
          method.invoke(subject, propertyToSet);
        }
      }
    }
  }

  /**
   * Converts the provided JsonNode to the specified type, if possible.
   *
   * @param node The JsonNode to convert.
   * @param destClass The class to convert to.
   * @return The converted value.
   */
  @SuppressWarnings("unchecked")
  private static <T> T convert(JsonNode node, Class<T> destClass) {
    if (destClass.equals(String.class)) {
      return (T) node.asText();
    } else if (destClass.equals(int.class)) {
      return (T) Integer.valueOf(node.asInt());
    } else if (destClass.equals(boolean.class)) {
      return (T) Boolean.valueOf(node.asBoolean());
    } else if (destClass.equals(long.class)) {
      return (T) Long.valueOf(node.asLong());
    } else if (destClass.equals(double.class)) {
      return (T) Double.valueOf(node.asDouble());
    } else if (destClass.equals(float.class)) {
      return (T) Float.valueOf(node.floatValue());
    }

    throw new IllegalArgumentException("Unable to get type " + destClass + " from JsonNode");
  }

  /**
   * Converts an object to the specified type, if possible.
   *
   * @param property The object to convert.
   * @param destClass The class to convert to.
   * @return The converted object
   */
  @SuppressWarnings("unchecked")
  private static <T> T convert(Object property, Class<T> destClass) {
    Class<?> srcClass = property.getClass();

    if (destClass.isAssignableFrom(srcClass)) {
      return (T) property;
    }

    String text = String.valueOf(property);

    if (destClass.isAssignableFrom(String.class)) {
      return (T) text;
    } else if (destClass.isAssignableFrom(Integer.class) || destClass.isAssignableFrom(int.class)) {
      return (T) Integer.valueOf(text);
    } else if (destClass.isAssignableFrom(Boolean.class)
        || destClass.isAssignableFrom(boolean.class)) {
      return (T) Boolean.valueOf(text);
    } else if (destClass.isAssignableFrom(Long.class) || destClass.isAssignableFrom(long.class)) {
      return (T) Long.valueOf(text);
    } else if (destClass.isAssignableFrom(Double.class)
        || destClass.isAssignableFrom(double.class)) {
      return (T) Double.valueOf(text);
    } else if (destClass.isAssignableFrom(Float.class) || destClass.isAssignableFrom(float.class)) {
      return (T) Float.valueOf(text);
    }

    throw new IllegalArgumentException(
        "Unable to convert object of type " + srcClass + " to " + destClass);
  }

  /**
   * Checks if the method provided is a setter.
   *
   * @param method The method to check
   * @return Whether or not the method is a setter.
   */
  private static boolean isMethodSetter(Method method) {
    String methodName = method.getName();

    return methodName.startsWith("set")
        && methodName.length() > 3
        && Character.isUpperCase(methodName.charAt(3))
        && method.getReturnType().equals(Void.TYPE)
        && Modifier.isPublic(method.getModifiers())
        && method.getParameterTypes().length == 1;
  }
}
