package edu.byu.hbll.rosettatools.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Dublin Core model.
 *
 * @author cfd2
 * @author Ben Welker
 */
@XmlRootElement(name = DublinCore.RECORD, namespace = DublinCore.DC_NAMESPACE)
@XmlAccessorType(XmlAccessType.NONE)
public class DublinCore implements Serializable {

  private static final long serialVersionUID = 1L;

  public static final String DC_NAMESPACE = "http://purl.org/dc/elements/1.1/";
  public static final String DCTERMS_NAMESPACE = "http://purl.org/dc/terms/";

  public static final String RECORD = "record";

  public static final String TITLE = "title";
  public static final String CREATOR = "creator";
  public static final String SUBJECT = "subject";
  public static final String DESCRIPTION = "description";
  public static final String PUBLISHER = "publisher";
  public static final String CONTRIBUTOR = "contributor";
  public static final String DATE = "date";
  public static final String TYPE = "type";
  public static final String FORMAT = "format";
  public static final String IDENTIFIER = "identifier";
  public static final String SOURCE = "source";
  public static final String LANGUAGE = "language";
  public static final String RELATION = "relation";
  public static final String COVERAGE = "coverage";
  public static final String RIGHTS = "rights";
  public static final String ALTERNATIVE = "alternative";
  public static final String TABLE_OF_CONTENTS = "tableOfContents";
  public static final String ABSTRACT = "abstract";
  public static final String CREATED = "created";
  public static final String VALID = "valid";
  public static final String AVAILABLE = "available";
  public static final String ISSUED = "issued";
  public static final String MODIFIED = "modified";
  public static final String DATE_ACCEPTED = "dateAccepted";
  public static final String DATE_COPYRIGHTED = "dateCopyrighted";
  public static final String DATE_SUBMITTED = "dateSubmitted";
  public static final String EXTENT = "extent";
  public static final String MEDIUM = "medium";
  public static final String IS_VERSION_OF = "isVersionOf";
  public static final String HAS_VERSION = "hasVersion";
  public static final String IS_REPLACED_BY = "isReplacedBy";
  public static final String REPLACES = "replaces";
  public static final String IS_REQUIRED_BY = "isRequiredBy";
  public static final String REQUIRES = "requires";
  public static final String IS_PART_OF = "isPartOf";
  public static final String HAS_PART = "hasPart";
  public static final String IS_REFERENCED_BY = "isReferencedBy";
  public static final String REFERENCES = "references";
  public static final String IS_FORMAT_OF = "isFormatOf";
  public static final String HAS_FORMAT = "hasFormat";
  public static final String CONFORMS_TO = "conformsTo";
  public static final String SPATIAL = "spatial";
  public static final String TEMPORAL = "temporal";
  public static final String AUDIENCE = "audience";
  public static final String ACCRUAL_METHOD = "accrualMethod";
  public static final String ACCRUAL_PERIODICITY = "accrualPeriodicity";
  public static final String ACCRUAL_POLICY = "accrualPolicy";
  public static final String INSTRUCTIONAL_METHOD = "instructionalMethod";
  public static final String PROVENANCE = "provenance";
  public static final String RIGHTS_HOLDER = "rightsHolder";
  public static final String MEDIATOR = "mediator";
  public static final String EDUCATION_LEVEL = "educationLevel";
  public static final String ACCESS_RIGHTS = "accessRights";
  public static final String LICENSE = "license";
  public static final String BIBLIOGRAPHIC_CITATION = "bibliographicCitation";

  @XmlElement(name = TITLE, namespace = DC_NAMESPACE)
  private List<String> title = new ArrayList<String>();

  @XmlElement(name = CREATOR, namespace = DC_NAMESPACE)
  private List<String> creator = new ArrayList<String>();

  @XmlElement(name = SUBJECT, namespace = DC_NAMESPACE)
  private List<String> subject = new ArrayList<String>();

  @XmlElement(name = DESCRIPTION, namespace = DC_NAMESPACE)
  private List<String> description = new ArrayList<String>();

  @XmlElement(name = PUBLISHER, namespace = DC_NAMESPACE)
  private List<String> publisher = new ArrayList<String>();

  @XmlElement(name = CONTRIBUTOR, namespace = DC_NAMESPACE)
  private List<String> contributor = new ArrayList<String>();

  @XmlElement(name = DATE, namespace = DC_NAMESPACE)
  private List<String> date = new ArrayList<String>();

  @XmlElement(name = TYPE, namespace = DC_NAMESPACE)
  private List<String> type = new ArrayList<String>();

  @XmlElement(name = FORMAT, namespace = DC_NAMESPACE)
  private List<String> format = new ArrayList<String>();

  @XmlElement(name = IDENTIFIER, namespace = DC_NAMESPACE)
  private List<String> identifier = new ArrayList<String>();

  @XmlElement(name = SOURCE, namespace = DC_NAMESPACE)
  private List<String> source = new ArrayList<String>();

  @XmlElement(name = LANGUAGE, namespace = DC_NAMESPACE)
  private List<String> language = new ArrayList<String>();

  @XmlElement(name = RELATION, namespace = DC_NAMESPACE)
  private List<String> relation = new ArrayList<String>();

  @XmlElement(name = COVERAGE, namespace = DC_NAMESPACE)
  private List<String> coverage = new ArrayList<String>();

  @XmlElement(name = RIGHTS, namespace = DC_NAMESPACE)
  private List<String> rights = new ArrayList<String>();

  @XmlElement(name = ALTERNATIVE, namespace = DCTERMS_NAMESPACE)
  private List<String> alternative = new ArrayList<String>();

  @XmlElement(name = TABLE_OF_CONTENTS, namespace = DCTERMS_NAMESPACE)
  private List<String> tableOfContents = new ArrayList<String>();

  @XmlElement(name = ABSTRACT, namespace = DCTERMS_NAMESPACE)
  private List<String> abstracts = new ArrayList<String>();

  @XmlElement(name = CREATED, namespace = DCTERMS_NAMESPACE)
  private List<String> created = new ArrayList<String>();

  @XmlElement(name = VALID, namespace = DCTERMS_NAMESPACE)
  private List<String> valid = new ArrayList<String>();

  @XmlElement(name = AVAILABLE, namespace = DCTERMS_NAMESPACE)
  private List<String> available = new ArrayList<String>();

  @XmlElement(name = ISSUED, namespace = DCTERMS_NAMESPACE)
  private List<String> issued = new ArrayList<String>();

  @XmlElement(name = MODIFIED, namespace = DCTERMS_NAMESPACE)
  private List<String> modified = new ArrayList<String>();

  @XmlElement(name = DATE_ACCEPTED, namespace = DCTERMS_NAMESPACE)
  private List<String> dateAccepted = new ArrayList<String>();

  @XmlElement(name = DATE_COPYRIGHTED, namespace = DCTERMS_NAMESPACE)
  private List<String> dateCopyrighted = new ArrayList<String>();

  @XmlElement(name = DATE_SUBMITTED, namespace = DCTERMS_NAMESPACE)
  private List<String> dateSubmitted = new ArrayList<String>();

  @XmlElement(name = EXTENT, namespace = DCTERMS_NAMESPACE)
  private List<String> extent = new ArrayList<String>();

  @XmlElement(name = MEDIUM, namespace = DCTERMS_NAMESPACE)
  private List<String> medium = new ArrayList<String>();

  @XmlElement(name = IS_VERSION_OF, namespace = DCTERMS_NAMESPACE)
  private List<String> isVersionOf = new ArrayList<String>();

  @XmlElement(name = HAS_VERSION, namespace = DCTERMS_NAMESPACE)
  private List<String> hasVersion = new ArrayList<String>();

  @XmlElement(name = IS_REPLACED_BY, namespace = DCTERMS_NAMESPACE)
  private List<String> isReplacedBy = new ArrayList<String>();

  @XmlElement(name = REPLACES, namespace = DCTERMS_NAMESPACE)
  private List<String> replaces = new ArrayList<String>();

  @XmlElement(name = IS_REQUIRED_BY, namespace = DCTERMS_NAMESPACE)
  private List<String> isRequiredBy = new ArrayList<String>();

  @XmlElement(name = REQUIRES, namespace = DCTERMS_NAMESPACE)
  private List<String> requires = new ArrayList<String>();

  @XmlElement(name = IS_PART_OF, namespace = DCTERMS_NAMESPACE)
  private List<String> isPartOf = new ArrayList<String>();

  @XmlElement(name = HAS_PART, namespace = DCTERMS_NAMESPACE)
  private List<String> hasPart = new ArrayList<String>();

  @XmlElement(name = IS_REFERENCED_BY, namespace = DCTERMS_NAMESPACE)
  private List<String> isReferencedBy = new ArrayList<String>();

  @XmlElement(name = REFERENCES, namespace = DCTERMS_NAMESPACE)
  private List<String> references = new ArrayList<String>();

  @XmlElement(name = IS_FORMAT_OF, namespace = DCTERMS_NAMESPACE)
  private List<String> isFormatOf = new ArrayList<String>();

  @XmlElement(name = HAS_FORMAT, namespace = DCTERMS_NAMESPACE)
  private List<String> hasFormat = new ArrayList<String>();

  @XmlElement(name = CONFORMS_TO, namespace = DCTERMS_NAMESPACE)
  private List<String> conformsTo = new ArrayList<String>();

  @XmlElement(name = SPATIAL, namespace = DCTERMS_NAMESPACE)
  private List<String> spatial = new ArrayList<String>();

  @XmlElement(name = TEMPORAL, namespace = DCTERMS_NAMESPACE)
  private List<String> temporal = new ArrayList<String>();

  @XmlElement(name = AUDIENCE, namespace = DCTERMS_NAMESPACE)
  private List<String> audience = new ArrayList<String>();

  @XmlElement(name = ACCRUAL_METHOD, namespace = DCTERMS_NAMESPACE)
  private List<String> accrualMethod = new ArrayList<String>();

  @XmlElement(name = ACCRUAL_PERIODICITY, namespace = DCTERMS_NAMESPACE)
  private List<String> accrualPeriodicity = new ArrayList<String>();

  @XmlElement(name = ACCRUAL_POLICY, namespace = DCTERMS_NAMESPACE)
  private List<String> accrualPolicy = new ArrayList<String>();

  @XmlElement(name = INSTRUCTIONAL_METHOD, namespace = DCTERMS_NAMESPACE)
  private List<String> instructionalMethod = new ArrayList<String>();

  @XmlElement(name = PROVENANCE, namespace = DCTERMS_NAMESPACE)
  private List<String> provenance = new ArrayList<String>();

  @XmlElement(name = RIGHTS_HOLDER, namespace = DCTERMS_NAMESPACE)
  private List<String> rightsHolder = new ArrayList<String>();

  @XmlElement(name = MEDIATOR, namespace = DCTERMS_NAMESPACE)
  private List<String> mediator = new ArrayList<String>();

  @XmlElement(name = EDUCATION_LEVEL, namespace = DCTERMS_NAMESPACE)
  private List<String> educationLevel = new ArrayList<String>();

  @XmlElement(name = ACCESS_RIGHTS, namespace = DCTERMS_NAMESPACE)
  private List<String> accessRights = new ArrayList<String>();

  @XmlElement(name = LICENSE, namespace = DCTERMS_NAMESPACE)
  private List<String> license = new ArrayList<String>();

  @XmlElement(name = BIBLIOGRAPHIC_CITATION, namespace = DCTERMS_NAMESPACE)
  private List<String> bibliographicCitation = new ArrayList<String>();

  /**
   * Add.
   *
   * @param field the field
   * @param value the value
   */
  public void add(String field, String value) {
    switch (field) {
      case TITLE:
        title.add(value);
        break;
      case CREATOR:
        creator.add(value);
        break;
      case SUBJECT:
        subject.add(value);
        break;
      case DESCRIPTION:
        description.add(value);
        break;
      case PUBLISHER:
        publisher.add(value);
        break;
      case CONTRIBUTOR:
        contributor.add(value);
        break;
      case DATE:
        date.add(value);
        break;
      case TYPE:
        type.add(value);
        break;
      case FORMAT:
        format.add(value);
        break;
      case IDENTIFIER:
        identifier.add(value);
        break;
      case SOURCE:
        source.add(value);
        break;
      case LANGUAGE:
        language.add(value);
        break;
      case RELATION:
        relation.add(value);
        break;
      case COVERAGE:
        coverage.add(value);
        break;
      case RIGHTS:
        rights.add(value);
        break;
      case ALTERNATIVE:
        alternative.add(value);
        break;
      case TABLE_OF_CONTENTS:
        tableOfContents.add(value);
        break;
      case ABSTRACT:
        abstracts.add(value);
        break;
      case CREATED:
        created.add(value);
        break;
      case VALID:
        valid.add(value);
        break;
      case AVAILABLE:
        available.add(value);
        break;
      case ISSUED:
        issued.add(value);
        break;
      case MODIFIED:
        modified.add(value);
        break;
      case DATE_ACCEPTED:
        dateAccepted.add(value);
        break;
      case DATE_COPYRIGHTED:
        dateCopyrighted.add(value);
        break;
      case DATE_SUBMITTED:
        dateSubmitted.add(value);
        break;
      case EXTENT:
        extent.add(value);
        break;
      case MEDIUM:
        medium.add(value);
        break;
      case IS_VERSION_OF:
        isVersionOf.add(value);
        break;
      case HAS_VERSION:
        hasVersion.add(value);
        break;
      case IS_REPLACED_BY:
        isReplacedBy.add(value);
        break;
      case REPLACES:
        replaces.add(value);
        break;
      case IS_REQUIRED_BY:
        isRequiredBy.add(value);
        break;
      case REQUIRES:
        requires.add(value);
        break;
      case IS_PART_OF:
        isPartOf.add(value);
        break;
      case HAS_PART:
        hasPart.add(value);
        break;
      case IS_REFERENCED_BY:
        isReferencedBy.add(value);
        break;
      case REFERENCES:
        references.add(value);
        break;
      case IS_FORMAT_OF:
        isFormatOf.add(value);
        break;
      case HAS_FORMAT:
        hasFormat.add(value);
        break;
      case CONFORMS_TO:
        conformsTo.add(value);
        break;
      case SPATIAL:
        spatial.add(value);
        break;
      case TEMPORAL:
        temporal.add(value);
        break;
      case AUDIENCE:
        audience.add(value);
        break;
      case ACCRUAL_METHOD:
        accrualMethod.add(value);
        break;
      case ACCRUAL_PERIODICITY:
        accrualPeriodicity.add(value);
        break;
      case ACCRUAL_POLICY:
        accrualPolicy.add(value);
        break;
      case INSTRUCTIONAL_METHOD:
        instructionalMethod.add(value);
        break;
      case PROVENANCE:
        provenance.add(value);
        break;
      case RIGHTS_HOLDER:
        rightsHolder.add(value);
        break;
      case MEDIATOR:
        mediator.add(value);
        break;
      case EDUCATION_LEVEL:
        educationLevel.add(value);
        break;
      case ACCESS_RIGHTS:
        accessRights.add(value);
        break;
      case LICENSE:
        license.add(value);
        break;
      case BIBLIOGRAPHIC_CITATION:
        bibliographicCitation.add(value);
        break;
    }
  }

  // getters
  public List<String> getTitle() {
    return title;
  }

  public List<String> getCreator() {
    return creator;
  }

  public List<String> getSubject() {
    return subject;
  }

  public List<String> getDescription() {
    return description;
  }

  public List<String> getPublisher() {
    return publisher;
  }

  public List<String> getContributor() {
    return contributor;
  }

  public List<String> getDate() {
    return date;
  }

  public List<String> getType() {
    return type;
  }

  public List<String> getFormat() {
    return format;
  }

  public List<String> getIdentifier() {
    return identifier;
  }

  public List<String> getSource() {
    return source;
  }

  public List<String> getLanguage() {
    return language;
  }

  public List<String> getRelation() {
    return relation;
  }

  public List<String> getCoverage() {
    return coverage;
  }

  public List<String> getRights() {
    return rights;
  }

  public List<String> getAlternative() {
    return alternative;
  }

  public List<String> getTableOfContents() {
    return tableOfContents;
  }

  public List<String> getAbstract() {
    return abstracts;
  }

  public List<String> getCreated() {
    return created;
  }

  public List<String> getValid() {
    return valid;
  }

  public List<String> getAvailable() {
    return available;
  }

  public List<String> getIssued() {
    return issued;
  }

  public List<String> getModified() {
    return modified;
  }

  public List<String> getDateAccepted() {
    return dateAccepted;
  }

  public List<String> getDateCopyrighted() {
    return dateCopyrighted;
  }

  public List<String> getDateSubmitted() {
    return dateSubmitted;
  }

  public List<String> getExtent() {
    return extent;
  }

  public List<String> getMedium() {
    return medium;
  }

  public List<String> getIsVersionOf() {
    return isVersionOf;
  }

  public List<String> getHasVersion() {
    return hasVersion;
  }

  public List<String> getIsReplacedBy() {
    return isReplacedBy;
  }

  public List<String> getReplaces() {
    return replaces;
  }

  public List<String> getIsRequiredBy() {
    return isRequiredBy;
  }

  public List<String> getRequires() {
    return requires;
  }

  public List<String> getIsPartOf() {
    return isPartOf;
  }

  public List<String> getHasPart() {
    return hasPart;
  }

  public List<String> getIsReferencedBy() {
    return isReferencedBy;
  }

  public List<String> getReferences() {
    return references;
  }

  public List<String> getIsFormatOf() {
    return isFormatOf;
  }

  public List<String> getHasFormat() {
    return hasFormat;
  }

  public List<String> getConformsTo() {
    return conformsTo;
  }

  public List<String> getSpatial() {
    return spatial;
  }

  public List<String> getTemporal() {
    return temporal;
  }

  public List<String> getAudience() {
    return audience;
  }

  public List<String> getAccrualMethod() {
    return accrualMethod;
  }

  public List<String> getAccrualPeriodicity() {
    return accrualPeriodicity;
  }

  public List<String> getAccrualPolicy() {
    return accrualPolicy;
  }

  public List<String> getInstructionalMethod() {
    return instructionalMethod;
  }

  public List<String> getProvenance() {
    return provenance;
  }

  public List<String> getRightsHolder() {
    return rightsHolder;
  }

  public List<String> getMediator() {
    return mediator;
  }

  public List<String> getEducationLevel() {
    return educationLevel;
  }

  public List<String> getAccessRights() {
    return accessRights;
  }

  public List<String> getLicense() {
    return license;
  }

  public List<String> getBibliographicCitation() {
    return bibliographicCitation;
  }

  // setters
  public void setTitle(List<String> title) {
    this.title = title;
  }

  public void setCreator(List<String> creator) {
    this.creator = creator;
  }

  public void setSubject(List<String> subject) {
    this.subject = subject;
  }

  public void setDescription(List<String> description) {
    this.description = description;
  }

  public void setPublisher(List<String> publisher) {
    this.publisher = publisher;
  }

  public void setContributor(List<String> contributor) {
    this.contributor = contributor;
  }

  public void setDate(List<String> date) {
    this.date = date;
  }

  public void setType(List<String> type) {
    this.type = type;
  }

  public void setFormat(List<String> format) {
    this.format = format;
  }

  public void setIdentifier(List<String> identifier) {
    this.identifier = identifier;
  }

  public void setSource(List<String> source) {
    this.source = source;
  }

  public void setLanguage(List<String> language) {
    this.language = language;
  }

  public void setRelation(List<String> relation) {
    this.relation = relation;
  }

  public void setCoverage(List<String> coverage) {
    this.coverage = coverage;
  }

  public void setRights(List<String> rights) {
    this.rights = rights;
  }

  public void setAlternative(List<String> alternative) {
    this.alternative = alternative;
  }

  public void setTableOfContents(List<String> tableOfContents) {
    this.tableOfContents = tableOfContents;
  }

  public void setAbstract(List<String> abstracts) {
    this.abstracts = abstracts;
  }

  public void setCreated(List<String> created) {
    this.created = created;
  }

  public void setValid(List<String> valid) {
    this.valid = valid;
  }

  public void setAvailable(List<String> available) {
    this.available = available;
  }

  public void setIssued(List<String> issued) {
    this.issued = issued;
  }

  public void setModified(List<String> modified) {
    this.modified = modified;
  }

  public void setDateAccepted(List<String> dateAccepted) {
    this.dateAccepted = dateAccepted;
  }

  public void setDateCopyrighted(List<String> dateCopyrighted) {
    this.dateCopyrighted = dateCopyrighted;
  }

  public void setDateSubmitted(List<String> dateSubmitted) {
    this.dateSubmitted = dateSubmitted;
  }

  public void setExtent(List<String> extent) {
    this.extent = extent;
  }

  public void setMedium(List<String> medium) {
    this.medium = medium;
  }

  public void setIsVersionOf(List<String> isVersionOf) {
    this.isVersionOf = isVersionOf;
  }

  public void setHasVersion(List<String> hasVersion) {
    this.hasVersion = hasVersion;
  }

  public void setIsReplacedBy(List<String> isReplacedBy) {
    this.isReplacedBy = isReplacedBy;
  }

  public void setReplaces(List<String> replaces) {
    this.replaces = replaces;
  }

  public void setIsRequiredBy(List<String> isRequiredBy) {
    this.isRequiredBy = isRequiredBy;
  }

  public void setRequires(List<String> requires) {
    this.requires = requires;
  }

  public void setIsPartOf(List<String> isPartOf) {
    this.isPartOf = isPartOf;
  }

  public void setHasPart(List<String> hasPart) {
    this.hasPart = hasPart;
  }

  public void setIsReferencedBy(List<String> isReferencedBy) {
    this.isReferencedBy = isReferencedBy;
  }

  public void setReferences(List<String> references) {
    this.references = references;
  }

  public void setIsFormatOf(List<String> isFormatOf) {
    this.isFormatOf = isFormatOf;
  }

  public void setHasFormat(List<String> hasFormat) {
    this.hasFormat = hasFormat;
  }

  public void setConformsTo(List<String> conformsTo) {
    this.conformsTo = conformsTo;
  }

  public void setSpatial(List<String> spatial) {
    this.spatial = spatial;
  }

  public void setTemporal(List<String> temporal) {
    this.temporal = temporal;
  }

  public void setAudience(List<String> audience) {
    this.audience = audience;
  }

  public void setAccrualMethod(List<String> accrualMethod) {
    this.accrualMethod = accrualMethod;
  }

  public void setAccrualPeriodicity(List<String> accrualPeriodicity) {
    this.accrualPeriodicity = accrualPeriodicity;
  }

  public void setAccrualPolicy(List<String> accrualPolicy) {
    this.accrualPolicy = accrualPolicy;
  }

  public void setInstructionalMethod(List<String> instructionalMethod) {
    this.instructionalMethod = instructionalMethod;
  }

  public void setProvenance(List<String> provenance) {
    this.provenance = provenance;
  }

  public void setRightsHolder(List<String> rightsHolder) {
    this.rightsHolder = rightsHolder;
  }

  public void setMediator(List<String> mediator) {
    this.mediator = mediator;
  }

  public void setEducationLevel(List<String> educationLevel) {
    this.educationLevel = educationLevel;
  }

  public void setAccessRights(List<String> accessRights) {
    this.accessRights = accessRights;
  }

  public void setLicense(List<String> license) {
    this.license = license;
  }

  public void setBibliographicCitation(List<String> bibliographicCitation) {
    this.bibliographicCitation = bibliographicCitation;
  }
}
