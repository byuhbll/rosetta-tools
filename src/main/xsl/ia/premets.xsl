<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />


<!-- The dublin core metadata -->
<xsl:param name="dc" />

<!-- The metadata from the source -->
<xsl:param name="srcmd" />

<!-- The retention policy as set by user or default from configuration -->
<xsl:param name="retentionPolicy" />

<!-- The rights policy as set by user or default from configuration -->
<xsl:param name="rightsPolicy" />

<!-- The archive.org item id -->
<xsl:param name="item" />

<xsl:template match="xml">
	<premets>
		<dc>
			<xsl:copy-of select="$dc" />
		</dc>
		<srcmd type="OTHER" otherType="ia">
			<xsl:copy-of select="$srcmd" />
		</srcmd>
		<files>
			/*Make the PDF the first in the list for Rosetta's Viewer*/
			<xsl:apply-templates
				select="files/file[name/text()=concat($item,'.pdf')]"
			/>
			<xsl:apply-templates
					select="files/file[
					name/text()=concat($item,'_dc.xml') or
					name/text()=concat($item,'_files.xml') or
					name/text()=concat($item,'_marc.xml') or
					name/text()=concat($item,'_meta.xml') or
					name/text()=concat($item,'_scandata.xml') or
					name/text()=concat($item,'.gif') or
					name/text()=concat($item,'_jp2.zip') or
					name/text()=concat($item,'_orig_jp2.tar')
				]"
			/>
		</files>
		<retentionPolicy>
			<xsl:copy-of select="$retentionPolicy" />
		</retentionPolicy>
		<rightsPolicy>
			<xsl:copy-of select="$rightsPolicy" />
		</rightsPolicy>
	</premets>
</xsl:template>

<xsl:template match="file">
	<file>
		<xsl:attribute name="label" select="name/text()" />
		<xsl:value-of select="name" />
	</file>
</xsl:template>

</xsl:stylesheet>
