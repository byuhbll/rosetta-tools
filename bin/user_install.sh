#!/bin/bash

# Exit on error
set -e

DIR=$(readlink -f "$(dirname $(readlink -f "$0"))"/..)

if [[ $USER == "dps" ]]; then
  echo "This cannot be run as the dps user. Please run as a different user."
  exit 1
fi

cd ~
mkdir -p bin
cd bin

ln -s /opt/maven/bin/mvn mvn
ln -s /opt/java/bin/java java
ln -s "$DIR"/bin/run.sh harvest
