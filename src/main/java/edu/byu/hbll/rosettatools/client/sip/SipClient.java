
package edu.byu.hbll.rosettatools.client.sip;

import edu.byu.hbll.rosettatools.Configuration;
import edu.byu.hbll.rosettatools.client.SoapClient;
import edu.byu.hbll.rosettatools.sip.SipMoveJob;
import edu.byu.hbll.rosettatools.util.RosettaXmlUtils;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SipClient
 *
 * <p>Enables access to the "SIP Processing Related Web Services". See docs at:
 * http://www.exlibrisgroup.org/display/RosettaOI/SIP+Processing+Related+Web+Services
 *
 * @author joelarson
 * @author Ben Welker
 */
public class SipClient {

  static final Logger logger = LoggerFactory.getLogger(SipMoveJob.class);

  public static final String URI = Configuration.getInstance().getConfig().path("sipUri").asText();

  /**
   * Gets the status of the sip.
   *
   * @param sipId the sip ID to check
   * @return The status of the sip
   * @throws SOAPException the SOAP exception
   * @throws JAXBException the JAXB exception
   * @throws InterruptedException the Interrupted exception
   */
  public static Map<String, String> getStatus(String sipId)
      throws SOAPException, JAXBException, InterruptedException {

    SOAPPart soapPart = getSipStatusInfo(sipId);

    Map<String, String> status = new HashMap<>();
    status.put("module", RosettaXmlUtils.evaluate("//module/text()", soapPart));
    status.put("status", RosettaXmlUtils.evaluate("//status/text()", soapPart));
    status.put("stage", RosettaXmlUtils.evaluate("//stage/text()", soapPart));

    return status;
  }

  /**
   * Gets the sip status info.
   *
   * @param sipId the sip ID to check
   * @return SOAPPart
   * @throws SOAPException the SOAP exception
   * @throws JAXBException the JAXB exception
   */
  public static SOAPPart getSipStatusInfo(String sipId) throws SOAPException, JAXBException {

    SipStatusInfo data = new SipStatusInfo();

    data.setSipId(sipId);
    SOAPPart soapPart = SoapClient.send(URI, "getSIPStatusInfo", data);

    return soapPart;
  }
}
