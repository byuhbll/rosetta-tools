package edu.byu.hbll.rosettatools.sip;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Sip Move Job.
 *
 * @author cfd2
 */
public class SipMoveJob implements Runnable {

  static final Logger logger = LoggerFactory.getLogger(SipMoveJob.class);

  private Sip sip;
  private Map<String, String> status;

  /**
   * Constructor.
   *
   * @param sip the sip to move
   */
  public SipMoveJob(Sip sip) {
    this.sip = sip;
  }

  @Override
  public void run() {
    try {
      logger.info(sip.getCollection() + "-" + sip.getItem() + " processing inside Rosetta");

      // wait until Rosetta is done processing SIP before moving directory
      for (int i = 1; !isDone(); i++) {
        if (i > 5) {
          logger.info(
              sip.getCollection() + "-" + sip.getItem() + " still processing inside Rosetta");
          Thread.sleep(10000);
        } else {
          Thread.sleep(i * 1000);
        }
      }

      logger.info(
          sip.getCollection()
              + "-"
              + sip.getItem()
              + " done processing inside Rosetta (Module="
              + status.get("module")
              + ", Status="
              + status.get("status")
              + ", Stage="
              + status.get("stage")
              + ")");

      while (true) {
        try {
          FileUtils.moveDirectory(sip.getDir(), sip.getFinalDir());
          break;
        } catch (FileExistsException e) {
          while (true) {
            try {
              // if restingDir already exists, timestamp the existing restingDir
              SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
              File oldFinalDir = new File(sip.getFinalDir() + "." + sdf.format(new Date()));
              FileUtils.moveDirectory(sip.getFinalDir(), oldFinalDir);
              break;
            } catch (FileExistsException e1) {
              Thread.sleep(1);
            }
          }
        }
      }
    } catch (Exception e) {
      logger.warn("Unable to move \"" + sip.getDir() + "\" to \"" + sip.getFinalDir() + "\": " + e);
    }
  }

  /**
   * Indicates whether or not this job is done.
   *
   * @return boolean
   */
  public boolean isDone() {
    status = sip.getStatus();
    boolean isDone =
        (status.get("module").equals("REP") || status.get("module").equals("PER"))
            && (status.get("status").equals("FINISHED")
                || status.get("status").equals("IN_TA")
                || status.get("status").equals("IN_HUMAN_STAGE"));
    return isDone;
  }
}
