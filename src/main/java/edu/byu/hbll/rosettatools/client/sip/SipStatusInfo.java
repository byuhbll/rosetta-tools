package edu.byu.hbll.rosettatools.client.sip;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The SIP Status Info model.
 *
 * @author Joe Larson
 * @author Ben Welker
 */
@XmlRootElement(name = "getSIPStatusInfo", namespace = "http://dps.exlibris.com/")
@XmlAccessorType(XmlAccessType.NONE)
public class SipStatusInfo {

  @XmlElement(name = "arg0")
  private String sipId;

  // getters
  public String getSipId() {
    return sipId;
  }

  // setters
  public void setSipId(String sipId) {
    this.sipId = sipId;
  }
}
