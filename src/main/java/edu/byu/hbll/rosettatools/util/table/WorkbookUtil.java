package edu.byu.hbll.rosettatools.util.table;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

public class WorkbookUtil {

  private Workbook workbook;
  private FormulaEvaluator evaluator;
  private DataFormatter formatter = new DataFormatter();

  /**
   * Constructor.
   *
   * @param path the csv workbook
   * @throws FileNotFoundException if an error occurs
   * @throws IOException if an error occurs
   * @throws ParseException if an error occurs
   */
  public WorkbookUtil(File path) throws FileNotFoundException, IOException, ParseException {
    if (path.getName().matches(".*\\.(csv)$")) {
      this.workbook = csvToWorkbook(path);
    } else {
      try {
        this.workbook = WorkbookFactory.create(new FileInputStream(path));
      } catch (InvalidFormatException e) {
        throw new IllegalArgumentException(e);
      }
    }

    if (workbook instanceof HSSFWorkbook) {
      evaluator = new HSSFFormulaEvaluator((HSSFWorkbook) workbook);
    } else if (workbook instanceof XSSFWorkbook) {
      evaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);
    }
  }

  private Workbook csvToWorkbook(File file) throws IOException, ParseException {
    HSSFWorkbook result = new HSSFWorkbook();
    CreationHelper creationHelper = result.getCreationHelper();
    CellStyle cellStyle = result.createCellStyle();
    cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("m/d/yy"));
    HSSFSheet sheet = result.createSheet("sheet1");

    ICsvListReader listReader = null;
    try {
      listReader = new CsvListReader(new FileReader(file), CsvPreference.STANDARD_PREFERENCE);
      List<String> rowData;
      int counter = 0;
      while ((rowData = listReader.read()) != null) {
        HSSFRow row = sheet.createRow(counter);
        for (int i = 0; i < rowData.size(); i++) {
          if (rowData.get(i) == null) {
            row.createCell(i);
            continue;
          }

          String cellDataType = getCellDataType(rowData.get(i));

          if (cellDataType.equals("Date")) {
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");
            Date date = df.parse(rowData.get(i));
            HSSFCell cell = row.createCell(i);
            cell.setCellValue(date);
            cell.setCellStyle(cellStyle);
          } else if (cellDataType.equals("Float")) {
            row.createCell(i).setCellValue(Float.parseFloat(rowData.get(i)));
          } else if (cellDataType.equals("Integer")) {
            row.createCell(i).setCellValue(Integer.parseInt(rowData.get(i)));
          } else {
            row.createCell(i).setCellValue(rowData.get(i));
          }
        }
        counter++;
      }
    } finally {
      if (listReader != null) {
        listReader.close();
      }
    }

    return result;
  }

  private String getCellDataType(String string) {
    try {
      SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");
      df.parse(string);
      return "Date";
    } catch (ParseException e) {
      // Do Nothing
    }

    try {
      Integer.parseInt(string);
      return "Integer";
    } catch (NumberFormatException e) {

      //Do Nothing
    }

    try {
      Float.parseFloat(string);
      return "Float";
    } catch (NumberFormatException e) {
      //Do Nothing
    }

    return "String";
  }

  public Sheet getFirstSheet() {
    Sheet sheet = workbook.getSheetAt(0);
    return sheet;
  }

  /**
   * Converts this workbook to a table.
   *
   * @return the table
   */
  public Table toTable() {
    return toTable(0);
  }

  /**
   * Converts the indicated sheet to a table.
   *
   * @param sheetIndex the sheet index
   * @return the table
   */
  public Table toTable(int sheetIndex) {
    return toTable(0, false, false, 0);
  }

  /**
   * Converts the indicated sheet to a table.
   *
   * @param sheetIndex the sheet index
   * @param hasHeader whether or not the sheet has headers
   * @param useCellAsValue if the cell should be used as a value
   * @param skip the number of rows to skip
   * @return the table
   */
  public Table toTable(int sheetIndex, boolean hasHeader, boolean useCellAsValue, int skip) {
    Table table = new TableImpl();

    int rowIndex = -1;

    for (Row row : workbook.getSheetAt(sheetIndex)) {
      rowIndex++;

      if (rowIndex < skip) {
        continue;
      }

      if (hasHeader && rowIndex == skip) {
        for (Cell cell : row) {
          // Ignore empty cells
          if (!cell.toString().trim().isEmpty()) {
            table.addColumnName(format(cell).trim());
          }
        }
        continue;
      }

      table.add();

      // Ignore cells out side for header range
      for (int i = 0; i < table.getColumnNames().size(); i++) {
        Cell cell = row.getCell(i);
        Object value = null;

        if (useCellAsValue) {
          value = cell;
        } else {
          value = format(cell);
        }

        table.add(value);
      }
    }

    return table;
  }

  /**
   * Converts every sheet in the workbook to tables.
   *
   * @return the tables
   */
  public List<Table> toTables() {
    return toTables(false, false);
  }

  /**
   * Converts every sheet in the workbook to tables.
   *
   * @param hasHeader if the sheets have headers
   * @param useCellAsValue if the cells are the values
   * @return the sheets
   */
  public List<Table> toTables(boolean hasHeader, boolean useCellAsValue) {
    List<Table> tables = new ArrayList<Table>();

    for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
      Table table = toTable(i, hasHeader, useCellAsValue, 0);
      tables.add(table);
    }

    return tables;
  }

  /**
   * permanently formats cell.
   *
   * @param cell cell to format
   * @return the formatted content
   */
  public String format(Cell cell) {
    return formatter.formatCellValue(cell, evaluator);
  }
}
