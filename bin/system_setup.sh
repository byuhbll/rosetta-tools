#!/bin/bash

# Exit on error
set -e

APPDIR=$(readlink -f "$(dirname $(readlink -f "$0"))"/..)

JDK_URL="https://download.java.net/java/jdk8u172/archive/b03/binaries/jdk-8u172-ea-bin-b03-linux-x64-18_jan_2018.tar.gz"
MAVEN_URL="http://mirrors.ocf.berkeley.edu/apache/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.tar.gz"

if [[ "$EUID" != "0" ]]; then
    echo "Please run as root."
    exit 1
fi

cd /opt

if [[ ! -e /opt/java ]]; then
  curl "$JDK_URL" -o jdk.tar.gz
  tar -xzvf jdk.tar.gz
  ln -s /opt/jdk1.8.0_172/ java
  rm -f jdk.tar.gz
fi

if [[ ! -e /opt/maven ]]; then
  curl "$MAVEN_URL" -o maven.tar.gz
  tar -xzvf maven.tar.gz
  ln -s /opt/apache-maven-3.5.2/ maven
  rm -f maven.tar.gz
fi

if [[ ! -e /usr/bin/rosetta-tools-user-setup ]]; then
  cd /usr/bin
  ln -s "$APPDIR"/bin/user_install.sh rosetta-tools-user-setup
fi

cd $APPDIR
export JAVA_HOME=/opt/java
/opt/maven/bin/mvn clean package

echo "Setup Complete!"