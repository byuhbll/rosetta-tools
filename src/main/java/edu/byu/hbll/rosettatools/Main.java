package edu.byu.hbll.rosettatools;

import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.harvester.AbstractHarvester;
import edu.byu.hbll.rosettatools.harvester.CdmHarvester;
import edu.byu.hbll.rosettatools.harvester.Harvester;
import edu.byu.hbll.rosettatools.report.AbstractReport;
import edu.byu.hbll.rosettatools.sip.Sip;
import edu.byu.hbll.rosettatools.sip.SipFactory;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entry point for the Rosetta Tools. Parses the command line, loads configuration, and executes the
 * commands.
 *
 * @author Ben Welker
 */
public class Main {
  Logger logger = LoggerFactory.getLogger(Main.class);

  /** cdm, ojs, csv, etc... */
  private String source = null;

  /** collection name from source meta data. */
  private String collection = null;

  private String reportName = null;

  /** if different than source collection name. */
  private String rosettaCollection = null;

  private List<String> items = new ArrayList<>();

  /**
   * Good for troubleshooting and testing - runs application but skips downloading the source files.
   */
  private boolean noDownload = false;

  /** Skips submission to Rosetta. */
  private boolean noSubmit = false;

  private boolean list = false;
  private Date from = null;
  private Date until = null;
  private int startIndex = 0;
  private int throughIndex = 0;
  private String user = null;
  private String password = null;
  private String institution = null;
  private String loginUri = null;
  private String retention = null;

  /** Rosetta rights policy code. */
  private String rights = null;

  /** Rosetta material flow code. */
  private String materialFlowId = null;

  /** Rosetta producer code. */
  private String producerId = null;

  private String cdmOriginField = null;
  private boolean generateReport = false;
  private Configuration config = null;

  private Main(Builder builder) {
    this.source = builder.source;
    this.collection = builder.collection;
    this.reportName = builder.reportName;
    this.rosettaCollection = builder.rosettaCollection;
    this.items = builder.items;
    this.noDownload = builder.noDownload;
    this.noSubmit = builder.noSubmit;
    this.list = builder.list;
    this.from = builder.from;
    this.until = builder.until;
    this.startIndex = builder.startIndex;
    this.throughIndex = builder.throughIndex;
    this.user = builder.user;
    this.password = builder.password;
    this.institution = builder.institution;
    this.loginUri = builder.loginUri;
    this.retention = builder.retention;
    this.rights = builder.rights;
    this.materialFlowId = builder.materialFlowId;
    this.producerId = builder.producerId;
    this.cdmOriginField = builder.cdmOriginField;
    this.generateReport = builder.generateReport;
    this.config = builder.config;
  }

  /**
   * Executes the command that was configured in this Main object.
   *
   * @throws Exception if any error occurs.
   */
  public void execute() throws Exception {
    if (generateReport) {
      String className = config.getConfig().path("reports").path(source).path("class").asText();
      AbstractReport report = (AbstractReport) Class.forName(className).newInstance();

      config.setProperties(report, "/");
      config.setProperties(report, "/reports/" + source);

      report.setCollection(collection);
      report.generateReport(reportName);
      return;
    }

    Harvester harvester = createHarvester();
    setMasterFileOrigin(harvester);
    prepareLogin();
    setupSipFactory(harvester);

    if (list) {
      harvestList(harvester);
    } else {
      harvest(harvester);
    }
  }

  /** Creates a new harvester. */
  private Harvester createHarvester()
      throws InstantiationException, IllegalAccessException, ClassNotFoundException,
          IllegalArgumentException, InvocationTargetException {
    String className = config.getConfig().path("sources").path(source).path("class").asText();
    AbstractHarvester harvester = (AbstractHarvester) Class.forName(className).newInstance();

    config.setProperties(harvester, "/");
    config.setProperties(harvester, "/sources/" + source);

    harvester.setRosettaCollection(rosettaCollection == null ? collection : rosettaCollection);

    return harvester;
  }

  /** Sets the cdmOriginField of the harvester is a CdmHarvester. */
  private void setMasterFileOrigin(Harvester harvester) {
    String className = config.getConfig().path("sources").path(source).path("class").asText();

    if (className.endsWith("CdmHarvester") && cdmOriginField != null) {
      ((CdmHarvester) harvester).setCdmOriginField(cdmOriginField);
    }
  }

  /** Gets the appropriate login information. */
  private void prepareLogin() {
    this.user = this.user == null ? config.getConfig().path("user").asText() : this.user;
    this.password =
        this.password == null ? config.getConfig().path("password").asText() : this.password;
    this.institution =
        this.institution == null
            ? config.getConfig().path("institution").asText()
            : this.institution;
    this.loginUri =
        this.loginUri == null ? config.getConfig().path("loginUri").asText() : this.loginUri;
  }

  /** Creates a new SipFactory. */
  private void setupSipFactory(Harvester harvester) throws Exception {
    SipFactory sipFactory;

    // Login not required.
    if (noSubmit || list) {
      sipFactory = new SipFactory();
    } else {
      sipFactory = new SipFactory(loginUri, user, password, institution);
    }

    config.setProperties(sipFactory, "/");
    config.setProperties(sipFactory, "/sources/" + source);

    sipFactory.setMaterialFlowId(
        this.materialFlowId == null
            ? config.getConfig().path("materialFlowId").asText()
            : this.materialFlowId);

    sipFactory.setProducerId(
        this.producerId == null ? config.getConfig().path("producerId").asText() : this.producerId);

    Map<String, Object> properties = new TreeMap<>();
    properties.put("sipFactory", sipFactory);
    properties.put("noDownload", noDownload);
    properties.put("noSubmit", noSubmit);
    properties.put("list", list);
    properties.put(
        "retention",
        this.retention == null ? config.getConfig().path("retention").asText() : this.retention);
    properties.put(
        "rights", this.rights == null ? config.getConfig().path("rights").asText() : this.rights);
    Configuration.setProperties(harvester, properties);
  }

  /** Performs a list harvest. */
  private void harvestList(Harvester harvester) throws Exception {
    Set<Item> itemSet;
    if (collection == null) {
      itemSet = harvester.list();
    } else {
      itemSet = harvester.list(collection, from, until, startIndex, throughIndex);
    }
    printItems(itemSet);
  }

  /** Performs a harvest. */
  private void harvest(Harvester harvester) throws Exception {
    try {
      Sip.startMoveExecutor();

      if (collection == null) {
        harvester.harvest(from, until, startIndex, throughIndex);
      } else {
        if (items.isEmpty()) {
          harvester.harvest(collection, from, until, startIndex, throughIndex);
        } else {
          for (String item : items) {
            harvester.harvest(collection, item);
          }
        }
      }
    } finally {
      Sip.shutdownMoveExecutor();
    }
  }

  /** Outputs a collection of items to standard out. */
  private static void printItems(Collection<? extends Item> items) {
    List<Item> itemList = new ArrayList<>(items);
    Collections.sort(itemList, new Item().new ItemComp());

    int maxIdLength = 0;
    int maxNameLength = 0;
    int maxDescriptionLength = 0;

    // Find the longest vale for each field.
    for (Item item : itemList) {
      maxIdLength = Math.max(maxIdLength, item.getId() != null ? item.getId().length() : 0);
      maxNameLength = Math.max(maxNameLength, item.getName() != null ? item.getName().length() : 0);
      maxDescriptionLength =
          Math.max(
              maxDescriptionLength,
              item.getDescription() != null ? item.getDescription().length() : 0);
    }

    // Print the items.
    for (Item item : itemList) {
      System.out.printf("%-" + maxIdLength + "s", item.getId());

      if (maxNameLength > 0) {
        System.out.printf("\t%-" + maxNameLength + "s", item.getName());
      }

      if (maxDescriptionLength > 0) {
        System.out.printf("\t%.50s", item.getDescription());
      }
      System.out.println();
    }
  }

  /**
   * Builder for the Main class.
   *
   * @author bwelker
   */
  public static class Builder {

    private String source = null;
    private String collection = null;
    private String reportName = null;
    private String rosettaCollection = null;
    private List<String> items = new LinkedList<>();
    private boolean noDownload = false;
    private boolean noSubmit = false;
    private boolean list = false;
    private Date from = null;
    private Date until = null;
    private int startIndex = 0;
    private int throughIndex = 0;
    private String user = null;
    private String password = null;
    private String institution = null;
    private String loginUri = null;
    private String retention = null;
    private String rights = null;
    private String materialFlowId = null;
    private String producerId = null;
    private String cdmOriginField = null;
    private boolean generateReport = false;
    private Configuration config;

    public Builder(Configuration config) {
      this.config = config;
    }

    public Builder source(String source) {
      this.source = source;
      return this;
    }

    public Builder collection(String collection) {
      this.collection = collection;
      return this;
    }

    public Builder reportName(String reportName) {
      this.reportName = reportName;
      return this;
    }

    public Builder rosettaCollection(String rosettaCollection) {
      this.rosettaCollection = rosettaCollection;
      return this;
    }

    public Builder items(List<String> items) {
      this.items = items;
      return this;
    }

    public Builder noDownload(boolean noDownload) {
      this.noDownload = noDownload;
      return this;
    }

    public Builder noSubmit(boolean noSubmit) {
      this.noSubmit = noSubmit;
      return this;
    }

    public Builder list(boolean list) {
      this.list = list;
      return this;
    }

    public Builder from(Date from) {
      this.from = from;
      return this;
    }

    public Builder until(Date until) {
      this.until = until;
      return this;
    }

    public Builder startIndex(int startIndex) {
      this.startIndex = startIndex;
      return this;
    }

    public Builder throughIndex(int throughIndex) {
      this.throughIndex = throughIndex;
      return this;
    }

    public Builder user(String user) {
      this.user = user;
      return this;
    }

    public Builder password(String password) {
      this.password = password;
      return this;
    }

    public Builder institution(String institution) {
      this.institution = institution;
      return this;
    }

    public Builder loginUri(String loginUri) {
      this.loginUri = loginUri;
      return this;
    }

    public Builder retention(String retention) {
      this.retention = retention;
      return this;
    }

    public Builder rights(String rights) {
      this.rights = rights;
      return this;
    }

    public Builder materialFlowId(String materialFlowId) {
      this.materialFlowId = materialFlowId;
      return this;
    }

    public Builder producerId(String producerId) {
      this.producerId = producerId;
      return this;
    }

    public Builder cdmOriginField(String cdmOriginField) {
      this.cdmOriginField = cdmOriginField;
      return this;
    }

    public Builder generateReport(boolean generateReport) {
      this.generateReport = generateReport;
      return this;
    }

    /**
     * Uses the configured options to create a new Main object.
     *
     * @return a new Main object.
     */
    public Main build() {
      if (generateReport && reportName == null) {
        throw new IllegalArgumentException("Report name must not be null if generating a report.");
      }
      return new Main(this);
    }
  }
}
