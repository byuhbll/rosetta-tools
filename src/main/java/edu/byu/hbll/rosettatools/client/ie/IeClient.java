
package edu.byu.hbll.rosettatools.client.ie;

import edu.byu.hbll.rosettatools.Configuration;
import edu.byu.hbll.rosettatools.client.SoapClient;
import edu.byu.hbll.rosettatools.sip.SipMoveJob;
import edu.byu.hbll.xml.XmlUtils;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.TransformerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IeClient.
 *
 * <p>Enables access to the "IE Update Web Services". See docs at:
 * http://www.exlibrisgroup.org/display/RosettaOI/IE+Update+Web+Services
 *
 * @author joelarson
 * @author Ben Welker
 */
public class IeClient {

  static final Logger logger = LoggerFactory.getLogger(SipMoveJob.class);

  public static final String URI = Configuration.getInstance().getConfig().path("ieUri").asText();

  /**
   * getIEMD.
   *
   * @param pdsHandle PDS Handle to use
   * @param ieId IE ID
   * @return IEMD
   * @throws SOAPException the SOAP exception
   * @throws JAXBException the JAXB exception
   * @throws TransformerException the Transformer exception
   */
  public static String getIEMD(String pdsHandle, String ieId)
      throws SOAPException, JAXBException, TransformerException {

    GetIEMD data = new GetIEMD();
    data.setPdsHandle(pdsHandle);
    data.setIeId(ieId);
    SOAPPart soapPart = SoapClient.send(URI, "getIEMD", data);

    String xml = XmlUtils.toString(soapPart);
    // fix Rosetta API response (returns escaped "<" and ">" chars)
    xml = xml.replace("&lt;", "<").replace("&gt;", ">");

    return xml;
  }
}
