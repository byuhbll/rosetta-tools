package edu.byu.hbll.rosettatools.util.table;

import java.util.Collection;
import java.util.List;

public interface Table extends Iterable<Row> {

  public Row add();

  public Cell add(Object value);

  public Row add(Row row);

  public void addColumnName(String columnName);

  public void addColumnNames(Collection<String> columnNames);

  public Row get(int index);

  public Cell get(int rowIndex, int columnIndex);

  public Cell get(int columnIndex, Object value);

  public String getColumnName(int index);

  public List<String> getColumnNames();

  public int getIndex(String columnName);

  public Cell set(int rowIndex, int columnIndex, Cell cell);

  public Cell set(int rowIndex, int columnIndex, Object value);

  public Row set(int index, Row row);

  public Cell set(int rowIndex, String columnName, Cell cell);

  public Cell set(int rowIndex, String columnName, Object value);

  public String setColumnName(int index, String columnName);

  public int size();
}
