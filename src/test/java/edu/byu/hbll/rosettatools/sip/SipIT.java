package edu.byu.hbll.rosettatools.sip;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SipIT {
  Path testPath;
  File testFile;
  Sip sip;
  Path tempFolder;

  @Before
  public void setup() throws IOException {
    testPath = Paths.get("src/test/resources/books.jpg");
    testFile = testPath.toFile();
    tempFolder = Files.createTempDirectory("sipTest");
    sip = new Sip(tempFolder.toFile(), "cdm", "120398");
  }

  @Test
  public void testSip() throws IOException {
    assertTrue(sip != null);
  }

  @Test
  public void testGetFullFile() {
    File file = Sip.getFullFile(testFile);
    assertTrue(file.exists() && testFile.isFile());
  }

  @Test
  public void testGetChecksumTIFF() throws IOException {
    Path path = Paths.get("src/test/resources/book.tif");
    String md5 = sip.getChecksum(path.toFile());

    assertTrue(md5.equals("fa23fb33bc39c4dd9c333a3983671386"));
  }

  @Test
  public void testGetChecksumJPG() throws IOException {
    Path path = Paths.get("src/test/resources/books.jpg");
    String md5 = sip.getChecksum(path.toFile());

    assertTrue(md5.equals("e966fb6a9a4e55db75be4c0c94fbcea6"));
  }

  @After
  public void after() throws IOException {
    Files.deleteIfExists(tempFolder);
  }
}
