<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:dcterms="http://purl.org/dc/terms/">
	
<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />
	

<xsl:param name="collection" />
<xsl:param name="item" />

<xsl:template match="cdm">
	<xsl:apply-templates select="xml" />
</xsl:template>

<!-- root xml element -->
<xsl:template match="xml">
	<dc:record>
		<xsl:apply-templates select="*"/>
		<dcterms:isPartOf><xsl:value-of select="$collection" /></dcterms:isPartOf>
	</dc:record>
</xsl:template>


<!-- convert each field to the actual dc -->
<xsl:template match="*">
	<xsl:choose>
		<xsl:when test="not(text())" />
		<xsl:when test="@dc and @dc=@dcterms">
			<xsl:element name="dc:{@dc}">
				<xsl:value-of select="." />
			</xsl:element>
		</xsl:when>
		<xsl:when test="@dcterms='isPartOf'">
			<dc:relation><xsl:value-of select="." /></dc:relation>
		</xsl:when>
		<xsl:when test="@dcterms">
			<xsl:element name="dcterms:{@dcterms}">
				<xsl:value-of select="." />
			</xsl:element>
		</xsl:when>
	</xsl:choose>
</xsl:template>


</xsl:stylesheet>
