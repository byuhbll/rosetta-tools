<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />


<xsl:param name="dc" />
<xsl:param name="srcmd" />
<xsl:param name="object-file" />
<xsl:param name="rightsPolicy" />
<xsl:param name="retentionPolicy" />


<xsl:template match="/">
	<premets>
		<xsl:apply-templates select="//record[1]" />
	</premets>
</xsl:template>


<xsl:template match="record">
	<dc>
		<xsl:copy-of select="$dc" />
	</dc>
	<srcmd type="OTHER" otherType="csv"> 
		<xsl:copy-of select="$srcmd" />
	</srcmd>
	<files>
		<file>
			<xsl:attribute name="label" select="$object-file" />
			<xsl:value-of select="$object-file" />
		</file>
	</files>
	<retentionPolicy>
		<xsl:copy-of select="$retentionPolicy" />
	</retentionPolicy>
	<rightsPolicy>
		<xsl:copy-of select="$rightsPolicy" />
	</rightsPolicy>
</xsl:template>


</xsl:stylesheet>
