<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />
	

<xsl:param name="collection" />
<xsl:param name="item" />
<xsl:variable name="dcFields" select="document('dcFields.xml')" />


<!-- root xml element -->
<xsl:template match="xml">
	<ia>
		<xsl:attribute name="collection" select="$collection" />
		<xsl:attribute name="item" select="$item" />
		<xsl:copy>
			<xsl:apply-templates select="metadata/*"/>
		</xsl:copy>
	</ia>
</xsl:template>


<!-- enrich each field with a name and dublin core fields -->
<xsl:template match="metadata/*">
	<xsl:variable name="name" select="name()" />
	<xsl:variable name="dcField" select="$dcFields/fields/field[name=$name]" />
	<xsl:variable name="text" select="text()" />
	<xsl:copy>
		<xsl:if test="$dcField">
			<xsl:attribute name="name" select="$dcField/name"/>
			<xsl:attribute name="dc" select="$dcField/dc"/>
			<xsl:attribute name="dcterms" select="$dcField/dcterms"/>
			<xsl:value-of select="$text"/>
		</xsl:if>
	</xsl:copy>
</xsl:template>

</xsl:stylesheet>
