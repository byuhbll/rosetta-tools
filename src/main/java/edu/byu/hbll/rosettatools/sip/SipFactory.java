package edu.byu.hbll.rosettatools.sip;

import edu.byu.hbll.rosettatools.util.PdsUtil;
import java.io.File;
import java.io.IOException;

/**
 * Sip Factory.
 *
 * @author cfd2
 */
public class SipFactory {

  private String materialFlowId = "77922";
  private String pdsHandle = "";
  private String producerId = "90187";
  private String depositUri = "http://rosetta.lib.byu.edu:1801/dpsws/deposit/DepositWebServices";
  private String depositSetId = "1";
  private String depositDir = "/operational_shared/mets";
  private String dcFile = "dc.xml";
  private String metsFile = "content/mets.xml";
  private String objectDir = "content/streams/";
  private String sipMove;

  /**
   * Constructor.
   *
   * @param loginUri Login URI for PDS
   * @param user User for PDS
   * @param password Password for PDS
   * @param institution Institution for PDS
   * @throws Exception if an error occurs
   */
  public SipFactory(String loginUri, String user, String password, String institution)
      throws Exception {
    this.pdsHandle = PdsUtil.getPdsHandle(loginUri, user, password, institution);
  }

  public SipFactory() throws Exception {}

  /**
   * Builds a new Sip for the indicated collection and item.
   *
   * @param collection the collection
   * @param item the item
   * @return the new Sip
   */
  public Sip newInstance(String collection, String item) {
    collection = collection.replaceAll("\\W", "_");
    item = item.replaceAll("\\W", "_");

    File dir = new File(depositDir + "/" + collection + "-" + item);

    try {
      dir = dir.getCanonicalFile();
    } catch (IOException e) {
      dir = dir.getAbsoluteFile();
    }

    Sip sip = new Sip(dir, collection, item);
    sip.setDcFile(new File(sip.getDir(), dcFile));
    sip.setMetsFile(new File(sip.getDir(), metsFile));
    sip.setObjectDir(new File(sip.getDir(), objectDir));
    sip.setMaterialFlowId(materialFlowId);
    sip.setProducerId(producerId);
    sip.setPdsHandle(pdsHandle);
    sip.setDepositSetId(depositSetId);

    if (sipMove != null) {
      String actualSipMove = sipMove.replaceAll("\\{\\{SIP\\}\\}", dir.getName());
      sip.setSipMove(actualSipMove);
    }

    return sip;
  }

  // getters
  public String getPdsHandle() {
    return pdsHandle;
  }

  public String getMaterialFlowId() {
    return materialFlowId;
  }

  public String getProducerId() {
    return producerId;
  }

  public String getDepositUri() {
    return depositUri;
  }

  public String getDepositSetId() {
    return depositSetId;
  }

  public String getDepositDir() {
    return depositDir;
  }

  public String getDcFile() {
    return dcFile;
  }

  public String getMetsFile() {
    return metsFile;
  }

  public String getObjectDir() {
    return objectDir;
  }

  public String getSipMove() {
    return sipMove;
  }

  // setters
  public void setPdsHandle(String pdsHandle) {
    this.pdsHandle = pdsHandle;
  }

  public void setMaterialFlowId(String materialFlowId) {
    this.materialFlowId = materialFlowId;
  }

  public void setProducerId(String producerId) {
    this.producerId = producerId;
  }

  public void setDepositUri(String depositUri) {
    this.depositUri = depositUri;
  }

  public void setDepositSetId(String depositSetId) {
    this.depositSetId = depositSetId;
  }

  public void setDepositDir(String depositDir) {
    this.depositDir = depositDir;
  }

  public void setDcFile(String dcFile) {
    this.dcFile = dcFile;
  }

  public void setMetsFile(String metsFile) {
    this.metsFile = metsFile;
  }

  public void setObjectDir(String objectDir) {
    this.objectDir = objectDir;
  }

  public void setSipMove(String sipMove) {
    this.sipMove = sipMove;
  }
}
