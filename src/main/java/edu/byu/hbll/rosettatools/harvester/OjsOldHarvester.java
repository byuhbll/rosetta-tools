package edu.byu.hbll.rosettatools.harvester;

import edu.byu.hbll.rosettatools.sip.Sip;
import edu.byu.hbll.rosettatools.util.RosettaXmlUtils;
import edu.byu.hbll.xml.XmlUtils;
import edu.byu.hbll.xslt.XslTransformer;
import java.io.File;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * The Old Ojs Harvester.
 *
 * @author cfd2
 */
public class OjsOldHarvester extends OaiHarvester {

  static final Logger logger = LoggerFactory.getLogger(OjsOldHarvester.class);

  private String dcXsl = "xsl/ojs/dc.xsl";
  private String premetsXsl = "xsl/ojs/premets.xsl";

  private XslTransformer dcXslUtil;
  private XslTransformer premetsXslUtil;

  @Override
  public void processItem(Node itemNode, Document xml) throws Exception {

    String collection = RosettaXmlUtils.evaluate("header/setSpec", itemNode).replaceAll(":.*", "");
    String item = RosettaXmlUtils.evaluate("header/identifier", itemNode).replaceAll(".*/", "");

    Sip sip = sipFactory.newInstance(collection, item);

    logger.info("Processing item: " + collection + "/" + item);

    File objectFile = null;

    if (noDownload) {

      logger.warn("Skipping the object download");

    } else {
      String link = RosettaXmlUtils.evaluate("metadata/dc/relation", itemNode);
      String downloadLink = link.replaceAll("/view/", "/download/");
      objectFile = sip.downloadObject(downloadLink, null);
    }

    DOMResult dcResult = new DOMResult();
    dcXslUtil.transform(new DOMSource(itemNode), dcResult);
    Node dc = dcResult.getNode();

    // save the metadata for the sip
    sip.saveDc(XmlUtils.toString(dc, true));

    Map<String, Object> params = new HashMap<String, Object>();
    params.put("dc", dc);
    params.put("srcmd", itemNode);
    params.put("rightsPolicy", getRights());

    if (objectFile != null) {
      params.put("object-file", objectFile.getName());
    }

    String metsXml =
        toMetsXml(premetsXslUtil, new StreamSource(new StringReader("<record />")), params);

    sip.saveMets(metsXml);

    logger.debug("Harvested item: " + collection + "/" + item);

    // send it to Rosetta
    if (noSubmit) {
      logger.warn("Skipping submission to Rosetta");
    } else {
      sip.submit();
    }
  }

  // getters
  public String getDcXsl() {
    return dcXsl;
  }

  public String getpremetsXsl() {
    return premetsXsl;
  }

  /**
   * Sets the DC XSL.
   *
   * @param dcXsl the dc xsl
   * @throws TransformerConfigurationException if an error occurs
   */
  public void setDcXsl(String dcXsl) throws TransformerConfigurationException {
    this.dcXsl = dcXsl;
    this.dcXslUtil = new XslTransformer(new StreamSource(new File(dcXsl)));
  }

  /**
   * Sets the premets XSL.
   *
   * @param premetsXsl the premets XSL
   * @throws TransformerConfigurationException if an error occurs
   */
  public void setPremetsXsl(String premetsXsl) throws TransformerConfigurationException {
    this.premetsXsl = premetsXsl;
    this.premetsXslUtil = new XslTransformer(new StreamSource(new File(premetsXsl)));
  }
}
