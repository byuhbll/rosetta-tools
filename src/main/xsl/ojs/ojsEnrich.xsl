<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
<xsl:output 
	indent="yes" 
	method="xml"
	encoding="UTF-8" />
	

<xsl:param name="collection" />
<xsl:param name="item" />
<xsl:variable name="dcFields" select="document('dcFields.xml')" />


<!-- root xml element -->
<xsl:template match="xml">
	<ojs>
		<xsl:attribute name="collection" select="$collection" />
		<xsl:attribute name="item" select="$item" />
		<xsl:copy>
			<xsl:apply-templates select="*"/>
		</xsl:copy>
	</ojs>
</xsl:template>


<!-- enrich each field with a name and dublin core fields -->
<xsl:template match="*">
	<xsl:variable name="name" select="name()" />
	<xsl:choose>
		<xsl:when test="$name='journal'">
			<xsl:variable name="dcField" select="$dcFields/fields/field[name='journal_title']" />
			<xsl:variable name="text" select="name/text()" />
			<xsl:copy>
				<xsl:if test="$dcField">
					<xsl:attribute name="name" select="$dcField/name"/>
					<xsl:attribute name="dc" select="$dcField/dc"/>
					<xsl:attribute name="dcterms" select="$dcField/dcterms"/>
					<xsl:value-of select="$text"/>
				</xsl:if>
			</xsl:copy>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="dcField" select="$dcFields/fields/field[name=$name]" />
			<xsl:variable name="text" select="text()" />
			<xsl:copy>
				<xsl:if test="$dcField">
					<xsl:attribute name="name" select="$dcField/name"/>
					<xsl:attribute name="dc" select="$dcField/dc"/>
					<xsl:attribute name="dcterms" select="$dcField/dcterms"/>
					<xsl:value-of select="$text"/>
				</xsl:if>
			</xsl:copy>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>
