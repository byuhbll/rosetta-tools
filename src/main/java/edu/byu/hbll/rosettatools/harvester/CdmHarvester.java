package edu.byu.hbll.rosettatools.harvester;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.sip.Sip;
import edu.byu.hbll.rosettatools.util.RosettaXmlUtils;
import edu.byu.hbll.xml.XmlUtils;
import edu.byu.hbll.xslt.XslTransformer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPathExpressionException;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * CdmHarvester connects to CONTENTdm's web services to harvest the digital images and pdfs there
 *
 * <p>get collections https://cdmhost/dmwebservices/index.php?q=dmGetCollectionList/json
 *
 * <p>collection field mapping
 * https://cdmhost/dmwebservices/index.php?q=dmGetCollectionFieldInfo/HerbR/json
 *
 * <p>get item pointers in collection
 * https://cdmhost/dmwebservices/index.php?q=dmQuery/HerbR/0/dmmodified/dmmodified:reverse/100/1/0/0/0/0/1/0/json
 *
 * <p>get item info https://cdmhost/dmwebservices/index.php?q=dmGetItemInfo/HerbR/75/xml
 *
 * <p>get compound item list
 * https://cdmhost/dmwebservices/index.php?q=dmGetCompoundObjectInfo/Diaries/7758/json Can then be
 * used to get other info about the item just like a normal object
 *
 * <p>oai item
 * https://cdmhost/cgi-bin/oai.exe?verb=GetRecord&metadataPrefix=oai_dc&identifier=oai:cdmhost:HerbR/75
 *
 * <p>dc field info https://cdmhost/dmwebservices/index.php?q=dmGetDublinCoreFieldInfo/xml
 *
 * <p>download object https://cdmhost/cgi-bin/showfile.exe?CISOROOT=/HerbR&CISOPTR=75
 *
 * @author cfd2
 * @author joelarson
 * @author Eric Nord
 */
public class CdmHarvester extends AbstractHarvester {

  static final Logger logger = LoggerFactory.getLogger(CdmHarvester.class);

  private static final Client client = ClientBuilder.newClient();

  private String host;

  private WebTarget target = client.target(host + "dmwebservices/index.php");
  private String dcXsl = "xsl/contentdm/dc.xsl";
  private String cdmEnrichXsl = "xsl/contentdm/cdmEnrich.xsl";
  private String premetsXsl = "xsl/contentdm/premets.xsl";
  private String masterFileDir = "/operational_shared/cdm/objects";

  private String cdmOriginField = "fullrs";

  private XslTransformer dcXslUtil;
  private XslTransformer premetsXslUtil;
  private XslTransformer cdmEnrichXslUtil;
  private Map<String, File> masterFileMap;

  public CdmHarvester() throws IOException {}

  /**
   * Gets collection list from ContentDM and returns a id, name pair for each collection ie. alias:
   * "19CMNI" name: "19th Century Mormon Article Newspaper Index"
   *
   * @see https://cdmhost/dmwebservices/index.php?q=dmGetCollectionList/json
   * @return Set&lt;Item&gt; alias, name pair
   */
  @Override
  public Set<Item> list() throws IOException {
    WebTarget collectionsTarget = target.queryParam("q", "dmGetCollectionList/json");
    Response response = collectionsTarget.request().buildGet().invoke();
    String s = response.readEntity(String.class);
    JsonNode node = mapper.readTree(s);

    Set<Item> collections = new HashSet<Item>();

    for (JsonNode collection : node) {
      String id = collection.get("alias").asText().replaceAll("^/", "");
      String name = collection.get("name").asText();

      Item item = new Item(id, name);
      collections.add(item);
    }
    return collections;
  }

  /**
   * Get list of items constrained by from - until date and start - through index.
   *
   * @param collection collection name
   * @param from start date
   * @param until end date
   * @param startIndex start index - defaults to 1
   * @param throughIndex end index - defaults to total collection size
   * @throws Exception if an error occurs
   * @return Set&lt;Item&gt; list of items sorted
   */
  @Override
  public Set<Item> list(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {
    startIndex = startIndex == 0 ? 1 : startIndex;
    int pageSize = 100;
    int index = 1;

    WebTarget listTarget =
        target.queryParam(
            "q", "dmQuery/" + collection + "/0/dmmodified/dmmodified:reverse/1/1/0/0/0/0/1/0/json");
    Response response = listTarget.request().buildGet().invoke();

    int total =
        mapper.readTree(response.readEntity(String.class)).get("pager").get("total").asInt();

    throughIndex = throughIndex > 0 ? throughIndex : total;
    Set<Item> allItems = new HashSet<>();

    do {
      List<String> pagePointers = getItems(collection, from, until, index, pageSize, total, total);
      for (String pointer : pagePointers) {
        Item item = new Item(pointer);
        allItems.add(item);
      }
      index += pageSize;

    } while (index <= total && index <= 10001);

    List<Item> allItemsSorted = new ArrayList<>(allItems);
    Collections.sort(allItemsSorted, new Item().new ItemComp());

    Set<Item> items = new HashSet<>();

    for (Item item : allItemsSorted) {
      items.add(item);
    }

    return items;
  }

  /**
   * Gets list of collections and then harvest() to.
   *
   * @param from start date
   * @param until end date
   * @param startIndex start index
   * @param throughIndex end index
   * @throws Exception in an error occurs
   */
  @Override
  public void harvest(Date from, Date until, int startIndex, int throughIndex) throws Exception {
    for (Item collection : list()) {
      harvest(collection.getId(), from, until, startIndex, throughIndex);
    }
  }

  /**
   * Harvest setup - prepares the item list and FieldMap - starts harvesting collections by Id
   * constrained by from - until date and start - through index.
   *
   * @param collection - collection.id
   * @param from start date
   * @param until end date
   * @param startIndex start index
   * @param throughIndex end index
   * @throws Exception in an error occurs
   */
  @Override
  public void harvest(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {

    Node fieldMap = XmlUtils.parse(getFieldMap(collection));

    List<Item> itemList = new ArrayList<>(list(collection, from, until, startIndex, throughIndex));
    Collections.sort(itemList, new Item().new ItemComp());
    for (Item item : itemList) {
      harvest(collection, item.getId(), fieldMap);
    }
  }

  /**
   * Harvest setup - adds fieldMap for harvest
   *
   * @param collection - collection.id
   * @param item - item id
   * @throws SAXException if a SAX error occurs
   * @throws XPathExpressionException if an XPath error occurs
   * @throws Exception if an error occurs
   */
  @Override
  public void harvest(String collection, String item)
      throws IOException, TransformerException, SAXException, XPathExpressionException {
    harvest(collection, item, XmlUtils.parse(getFieldMap(collection)));
  }

  /**
   * This is where harvesting is processed by collection, item and Fieldmap
   *
   * @param String collection - collection.id
   * @param String item
   * @param Node fieldMap
   * @throws IOException if an IO error occurs
   * @throws TransformerException if a transformer error occurs
   * @throws SAXException if a SAX error occurs
   * @throws XPathExpressionException if an XPath error occurs
   */
  private void harvest(String collection, String item, Node fieldMap)
      throws IOException, TransformerException, XPathExpressionException, SAXException {
    logger.info("Harvesting item: " + collection + "/" + item);

    /*
     * Get the item's metadata from cdm
     */
    String itemInfo = stripNonValidXMLCharacters(getItem(collection, item));
    Document itemXml = XmlUtils.parse(itemInfo);

    //Create SIP (Submission Information Packet) - this contains item and metadata for submission
    Sip sip = sipFactory.newInstance(collection, item);

    //Download pull master from master folder or lower res file from cdm
    File objectFile;
    try {
      objectFile = downloadFile(sip, itemXml, collection, item);
    } catch (Exception e) {
      logger.error("Harvest failed: Item object not found" + e);
      return;
    }

    //Create params map for all XSLs
    Map<String, Object> params = new HashMap<>();

    //Declare the source for the final toMets call
    Source xmlSource;

    //Process a compound object or do nothing if simple
    if (isCompoundObject(RosettaXmlUtils.evaluate("/xml/find", itemXml))) { // if compound item
      logger.info(collection + "/" + item + " is a compound object");
      objectFile.deleteOnExit();

      //here object file is the .cpd file
      Node doc = processCompoundItem(sip, collection, objectFile);

      params.put("cpd", doc);
      xmlSource = new DOMSource(doc);
    } else { // if simple object
      logger.info(collection + "/" + item + " is a simple object");
      xmlSource = new DOMSource(itemXml);
    }

    params.put("collection", rosettaCollection);
    params.put("item", item);
    params.put("fieldMap", fieldMap);
    params.put("retentionPolicy", getRetention());
    params.put("rightsPolicy", getRights());

    // Enrich the cdm item with name and dc counterparts
    DOMResult cdmResult = new DOMResult();
    cdmEnrichXslUtil.transform(new DOMSource(itemXml), cdmResult, params);
    Node cdm = cdmResult.getNode();
    params.put("srcmd", cdm);

    // Convert to actual dc
    DOMResult dcResult = new DOMResult();
    dcXslUtil.transform(new DOMSource(cdm), dcResult, params);

    Node dc = dcResult.getNode();
    params.put("dc", dc);

    // Save the metadata for the SIP (not the item)
    sip.saveDc(XmlUtils.toString(dc, true));

    // Create the mets document
    String metsXsml = toMetsXml(premetsXslUtil, xmlSource, params);
    sip.saveMets(metsXsml);

    logger.info("Harvested item: " + collection + "/" + item);

    // 13. Submit the SIP
    if (noSubmit) {
      logger.warn("Skipping submission to Rosetta");
    } else {
      sip.submit();
    }
  }

  /**
   * Some objects in ContentDM represent a collection of objects These compoundObjects are processed
   * differently as they don't contain images but rather .cpd files that contain references to their
   * child objects
   *
   * @param xmlFindElement element to find
   * @return boolean true if compound false if not
   */
  private boolean isCompoundObject(String xmlFindElement) {
    if (xmlFindElement.endsWith(".cpd")) {
      return true;
    }
    return false;
  }

  /**
   * Process compound item.
   *
   * @param sip the sip
   * @param collection the collection
   * @param cpd the cpd
   * @return document
   * @throws IOException the iO exception
   * @throws TransformerException the transformer exception
   * @throws SAXException if a SAX exception occurs
   * @throws XPathExpressionException if an XPATH error occurs
   */
  private Document processCompoundItem(Sip sip, String collection, File cpd)
      throws IOException, TransformerException, XPathExpressionException, SAXException {

    Document cpdUtil = null;
    try (FileInputStream in = new FileInputStream(cpd)) {
      cpdUtil = XmlUtils.parse(in);
    }

    List<Node> pages = XmlUtils.xpath("//page", cpdUtil);

    for (Node page : pages) {
      //Gets id for the page level item
      String childItem = RosettaXmlUtils.evaluate("pageptr", page);

      //XML for the page level item
      String itemInfo = getItem(collection, childItem);

      //Passes itemInfo into a java object
      Document itemXml = XmlUtils.parse(itemInfo);

      //Downloads file
      File masterFile = downloadFile(sip, itemXml, collection, childItem);

      //Gets an ###.xml file name to coorelate with the ###.jpg listed in the metadata and builds the file path
      //Writes xml metadata file to accompany the compound object file
      String xmlFilename =
          RosettaXmlUtils.evaluate("/xml/find", itemXml).replaceAll("\\..+", ".xml");
      Path xmlPath = Paths.get(sip.getObjectDir().getPath() + "/" + xmlFilename);

      Files.write(xmlPath, itemInfo.getBytes(), StandardOpenOption.CREATE);
      logger.info("Saved metadata to: " + xmlPath);

      if (masterFile != null) {
        // modify the filename in the cpd document (the one in memory, not the file itself) from "7916.jpg" to "Bird, Edwin R. (Edwin Ruthven), 1829-1910 p71"
        XmlUtils.xpath("pagefile", page).get(0).setTextContent(masterFile.getName());
      }
    }
    return cpdUtil;
  }

  /**
   * Download file from contentDM - if a masterfile is provided - looks up the masterfilename from
   * the itemxml and searches for it in the cdm/objects/ dir and loads the masterFile in place of
   * the contentDM download.
   *
   * @param sip (Submission Information Pacakge) - SIP contains image metadata etc for submission to
   *     Rosetta
   * @param itemXml ContentDMs xml file for this image
   * @param collection the collection
   * @param objectId the page level objectId "4576"
   * @return file file to be preserved
   * @throws IOException the IO Exception
   * @throws XPathExpressionException the XPath exception
   */
  private File downloadFile(Sip sip, Document itemXml, String collection, String objectId)
      throws IOException, XPathExpressionException {
    File objectFile = null;
    String objectFilename = RosettaXmlUtils.evaluate("/xml/find", itemXml);
    String fullrs = RosettaXmlUtils.evaluate("/xml/fullrs", itemXml);
    //Is it a compound object
    boolean isCompoundObject = objectFilename.endsWith(".cpd");

    /*
     *Skip download with arg -w unless it's a parent compound object (then we need the .cpd file to target child objects
     */
    if (noDownload && !isCompoundObject) {
      logger.warn("Skipping object download due to arg -w");
    } else {

      /*
       * Check if masterFile has been provided (masterFile is the original uncompressed version - usually of an image)
       */
      final File masterFile = findMasterFile(itemXml);

      if (masterFile != null) {
        logger.info("Master file found: " + masterFile.getName());

        /*
         * Makes a copy of the masterFile
         */
        objectFile = sip.saveObjectCopy(masterFile);

        /*
         * path to new location for file post harvest
         */
        String harvestedFilePath = masterFile.getParent() + "/harvested/" + masterFile.getName();

        try {
          //creates a new file
          File harvestedFile = new File(harvestedFilePath);
          //moves the masterfile to the harvested dir //TODO this seems like a lot of work for a file copy.
          FileUtils.moveFile(masterFile, harvestedFile);
        } catch (FileExistsException e) {
          // if harvestedFile already exists, timestamp the existing restingDir
          SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
          File harvestedFile = new File(harvestedFilePath + "." + sdf.format(new Date()));
          FileUtils.moveFile(masterFile, harvestedFile);
        }

        Node objectNode = XmlUtils.xpath("/xml/find", itemXml).get(0);

        if (objectNode != null) {
          objectNode.setTextContent(masterFile.getName());
        }
      } else {
        if (!isCompoundObject) {
          logger.info("Master file not found");
        }
        try {
          String metaFileName = RosettaXmlUtils.evaluate("/xml/find", itemXml);
          if (metaFileName.equals("")) {
            metaFileName = fullrs;
          }

          objectFile =
              sip.downloadObject(
                  host + "cgi-bin/showfile.exe?CISOROOT=/" + collection + "&CISOPTR=" + objectId,
                  metaFileName);
        } catch (NullPointerException e) {
          logger.error("Unable to download item object! " + e);
        }
      }
    }
    return objectFile;
  }

  /**
   * masterFile ContentDM stores a compressed image in jpg or jp2 format. When the original tif is
   * desired it can be provided manually in cdm/objects by the collection curator. ContentDM stores
   * the masterFileName in various locations in the itemXml metadata depending on the collection
   * masterFile is found by looking up the masterFileName in cdm itemXml identi, identa, fullrs, or
   * file elements and then checking against Map<&lt;masterFileName, path to file&gt;.
   *
   * @param itemXml cdm metadata
   */
  private File findMasterFile(Document itemXml) {
    //existence of a .cpd filename in itemXml/find indicates compound object parent files.
    //the .cpd file provides a list of children objects
    String objectFilename = RosettaXmlUtils.evaluate("/xml/find", itemXml);
    boolean isCompoundObject = objectFilename.endsWith(".cpd");

    File masterFile = null;

    if (cdmOriginField.equals("fullrs")) {
      String fullrs = RosettaXmlUtils.evaluate("/xml/fullrs", itemXml);
      // if fullrs has format "###\###\..." take only the last chunk (after final "\")
      // Example: GeorgeBeard 1 fullrs = Volume3\MSS_P_3_0002.jpg -> MSS_P_3_0002.jpg
      String[] splitOnSlash = fullrs.split("\\\\");
      if (splitOnSlash.length > 1) {
        fullrs = splitOnSlash[splitOnSlash.length - 1];
      }

      if (!isCompoundObject) {
        String identi = RosettaXmlUtils.evaluate("/xml/identi", itemXml);
        String[] splitOnSemi = identi.split(";");
        if (splitOnSemi.length > 1) {
          identi = splitOnSemi[0].trim();
        }
        masterFile = getMasterFile(fullrs, identi);
      }
    } else {
      //If not the parent compound Object use the user supplied filename location
      if (!isCompoundObject) {
        String originFieldText = (RosettaXmlUtils.evaluate("/xml/" + cdmOriginField, itemXml));
        if (originFieldText.isEmpty() || originFieldText == null) {
          logger.warn("-o value" + cdmOriginField + " not found");
        } else {
          //
          masterFile =
              getMasterFile(" ", originFieldText.substring(0, originFieldText.length() - 4));
        }
      }
    }

    return masterFile;
  }

  /**
   * Gets item.
   *
   * @param collection the collection
   * @param item the item
   * @return the itemInfo
   */
  public String getItem(String collection, String item) {

    logger.debug("Getting item info: " + collection + "/" + item);

    WebTarget getItemTarget =
        target.queryParam("q", "dmGetItemInfo/" + collection + "/" + item + "/xml");
    Response response = getItemTarget.request().buildGet().invoke();
    String itemInfo = response.readEntity(String.class);

    logger.debug(
        "Received item info: " + collection + "/" + item + ": " + itemInfo.length() + " bytes");

    return itemInfo;
  }

  /**
   * Gets items in the collection.
   *
   * @param collection the collection
   * @param from the from
   * @param until the until
   * @param startIndex the index
   * @param pageSize the page size
   * @param endIndex the through index
   * @param total the total
   * @return items
   * @throws IOException the iO exception
   */
  public List<String> getItems(
      String collection,
      Date from,
      Date until,
      int startIndex,
      int pageSize,
      int endIndex,
      int total)
      throws IOException {
    //If index is bigger than 10001 then 10001 otherwises start at startIndex
    int start = startIndex > 10001 ? 10001 : startIndex;

    //If startIndex is less than 10001 use pageSize otherwise use end - start +1
    pageSize = startIndex <= 10000 ? pageSize : endIndex - start + 1;

    boolean isLastPage = endIndex - startIndex + 1 <= pageSize;
    pageSize = isLastPage ? endIndex - start + 1 : pageSize;
    logger.info(
        "Retrieving pointers from "
            + collection
            + ": "
            + startIndex
            + "-"
            + (startIndex >= 10001 ? endIndex : startIndex + pageSize - 1));

    WebTarget getItemsTarget =
        target.queryParam(
            "q",
            "dmQuery/"
                + collection
                + "/0/dmmodified/dmmodified:reverse/"
                + pageSize
                + "/"
                + start
                + "/0/0/0/0/1/0/json");
    Response res = getItemsTarget.request().buildGet().invoke();
    String response = res.readEntity(String.class);
    JsonNode root = mapper.readTree(response);

    ArrayNode records = (ArrayNode) root.get("records");

    List<String> pointers = new ArrayList<>();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    for (int i = 0; i <= records.size() - 1; i++) {
      if (i < startIndex - start) {
        continue;
      }
      JsonNode record = records.get(i);
      String pointer = record.get("pointer").asText();

      if (from != null || until != null) {
        try {

          //Checks the dmmodified time reported by cdm against the from and until date params
          String dmModified = record.get("dmmodified").asText();

          //Some records do not contain a modified date we add the pointer and continue
          if (dmModified.isEmpty()) {
            pointers.add(pointer);
            continue;
          }

          Date modified = dateFormat.parse(dmModified);

          //true when modified date is after or on from date
          boolean fromCheck = from == null || modified.compareTo(from) >= 0;

          //true when modified date is before or on from date
          boolean untilCheck = until == null || modified.compareTo(until) <= 0;

          //If not bound between from and until continue
          if (!(fromCheck && untilCheck)) {
            continue;
          }
        } catch (Exception e) {
          //Some records have a dmmodified date of "" and will only be recognized by this exception
          logger.error(
              "Unable to determine modified date for " + collection + "/" + pointer + ": " + e);
          continue;
        }
      }
      pointers.add(pointer);
      if (isLastPage && i == endIndex - start) {
        break;
      }
    }

    logger.info(
        "Received pointers from "
            + collection
            + ": "
            + startIndex
            + "-"
            + (startIndex + pageSize - 1)
            + "/"
            + total);

    return pointers;
  }

  /**
   * Gets field map.
   *
   * @param collection the collection
   * @return field map
   */
  public String getFieldMap(String collection) {
    WebTarget getFieldMapTarget =
        target.queryParam("q", "dmGetCollectionFieldInfo/" + collection + "/xml");
    Response response = getFieldMapTarget.request().buildGet().invoke();
    return response.readEntity(String.class);
  }

  /**
   * Gets dublin core map.
   *
   * @return dublin core map
   */
  public String getDublinCoreMap() {
    WebTarget getDublinCoreMapTarget = target.queryParam("q", "dmGetDublinCoreFieldInfo/xml");
    Response response = getDublinCoreMapTarget.request().buildGet().invoke();
    return response.readEntity(String.class);
  }

  // getters
  public String getHost() {
    return host;
  }

  public String getDcXsl() {
    return dcXsl;
  }

  public String getpremetsXsl() {
    return premetsXsl;
  }

  public String getCdmEnrichXsl() {
    return cdmEnrichXsl;
  }

  public String getMasterFileDir() {
    return masterFileDir;
  }

  public String getCdmOriginField() {
    return cdmOriginField;
  }

  // setters
  public void setCdmOriginField(String cdmOriginField) {
    this.cdmOriginField = cdmOriginField;
  }

  public void setHost(String host) {
    this.host = host;
    target = client.target(host + "dmwebservices/index.php");
  }

  /**
   * Sets the master file directory.
   *
   * @param masterFileDir the master file directory.
   */
  public void setMasterFileDir(String masterFileDir) {
    this.masterFileDir = masterFileDir;
    this.masterFileMap = new HashMap<String, File>();

    if (new File(masterFileDir).exists()) {
      for (File file : new File(masterFileDir).listFiles()) {
        masterFileMap.put(getKeyPart(file), file);
      }
    }
  }

  public void setDcXsl(String dcXsl) throws TransformerConfigurationException {
    this.dcXsl = dcXsl;
    this.dcXslUtil = new XslTransformer(new StreamSource(new File(dcXsl)));
  }

  public void setPremetsXsl(String premetsXsl) throws TransformerConfigurationException {
    this.premetsXsl = premetsXsl;
    this.premetsXslUtil = new XslTransformer(new StreamSource(new File(premetsXsl)));
  }

  public void setCdmEnrichXsl(String cdmEnrichXsl) throws TransformerConfigurationException {
    this.cdmEnrichXsl = cdmEnrichXsl;
    this.cdmEnrichXslUtil = new XslTransformer(new StreamSource(new File(cdmEnrichXsl)));
  }

  /**
   * Gets the key part of a file name.
   *
   * @param file the file
   * @return the key part
   */
  public static String getKeyPart(File file) {
    return FilenameUtils.removeExtension(file.getName()).toLowerCase();
  }

  /**
   * @param fullrs the fullrs
   * @param masterFileName contains the name of the master file ie. MSS_3201_SP1_p003.
   * @return the master file
   */
  private File getMasterFile(String fullrs, String masterFileName) {

    // check identi string exists inside fullrs
    // substring from the match all the way to the end
    String fullrsPart =
        fullrs.contains(masterFileName) ? fullrs.substring(fullrs.indexOf(masterFileName)) : "";
    String[] originFields =
        cdmOriginField.equals("fullrs")
            ? new String[] {fullrs, masterFileName, fullrsPart}
            : new String[] {masterFileName, fullrsPart, fullrs};
    logger.info(
        "fullrs: " + fullrs + ", Master File: " + masterFileName + ", fullrsPart: " + fullrsPart);

    //for each filename lookup the filename in the masterFileMap.
    for (String filename : originFields) {
      String keyPart = getKeyPart(new File(filename));
      File masterFile = masterFileMap.get(keyPart);
      if (masterFile != null) {
        return masterFile;
      }
    }

    return null;
  }

  /**
   * Strip non valid Xml characters.
   *
   * @param in the in
   * @return the string
   */
  public String stripNonValidXMLCharacters(String in) {
    StringBuffer out = new StringBuffer(); // Used to hold the output.
    char current; // Used to reference the current character.

    if (in == null || ("".equals(in))) return ""; // vacancy test.
    for (int i = 0; i < in.length(); i++) {
      current =
          in.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
      if ((current == 0x9)
          || (current == 0xA)
          || (current == 0xD)
          || ((current >= 0x20) && (current <= 0xD7FF))
          || ((current >= 0xE000) && (current <= 0xFFFD))
          || ((current >= 0x10000) && (current <= 0x10FFFF))) {
        out.append(current);
      }
    }
    return out.toString();
  }
}
