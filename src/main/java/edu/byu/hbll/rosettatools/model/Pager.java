package edu.byu.hbll.rosettatools.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pager {
  private int start;
  private int maxrecs;
  private int total;

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public int getMaxrecs() {
    return maxrecs;
  }

  public void setMaxrecs(int maxrecs) {
    this.maxrecs = maxrecs;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(int total) {
    this.total = total;
  }
}
