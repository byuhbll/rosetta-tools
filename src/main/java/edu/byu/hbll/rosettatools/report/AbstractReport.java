package edu.byu.hbll.rosettatools.report;

import java.sql.Connection;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** @author Joe Larson */
public abstract class AbstractReport implements Report {

  static final Logger logger = LoggerFactory.getLogger(AbstractReport.class);

  protected String collection;

  protected String loginUri;
  protected String user;
  protected String password;
  protected String institution;

  protected String reportDBUri;
  protected String reportDBUser;
  protected String reportDBPassword;
  protected Connection connection = null;

  protected HSSFWorkbook wb = null;
  protected HSSFCellStyle headerCellStyle = null;
  protected HSSFCellStyle dateCellStyle = null;
  protected HSSFCellStyle warnCellStyle = null;

  public AbstractReport() {
    wb = new HSSFWorkbook();

    headerCellStyle = wb.createCellStyle();
    headerCellStyle.setBorderBottom(BorderStyle.MEDIUM);
    HSSFFont headerFont = wb.createFont();
    headerFont.setBold(true);
    headerCellStyle.setFont(headerFont);

    dateCellStyle = wb.createCellStyle();
    short df = wb.createDataFormat().getFormat("mmm dd, yyyy hh:mm:ss AM/PM");
    dateCellStyle.setDataFormat(df);

    HSSFPalette palette = wb.getCustomPalette();
    palette.setColorAtIndex(HSSFColorPredefined.RED.getIndex(), (byte) 245, (byte) 205, (byte) 198);

    warnCellStyle = wb.createCellStyle();
    warnCellStyle.setFillForegroundColor(HSSFColorPredefined.RED.getIndex());
    warnCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
  }

  /* getters and setters */

  public String getCollection() {
    return collection;
  }

  public void setCollection(String collection) {
    this.collection = collection;
  }

  public String getLoginUri() {
    return loginUri;
  }

  public void setLoginUri(String loginUri) {
    this.loginUri = loginUri;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getInstitution() {
    return institution;
  }

  public void setInstitution(String institution) {
    this.institution = institution;
  }

  public String getReportDBUri() {
    return reportDBUri;
  }

  public void setReportDBUri(String reportDBUri) {
    this.reportDBUri = reportDBUri;
  }

  public String getReportDBUser() {
    return reportDBUser;
  }

  public void setReportDBUser(String reportDBUser) {
    this.reportDBUser = reportDBUser;
  }

  public String getReportDBPassword() {
    return reportDBPassword;
  }

  public void setReportDBPassword(String reportDBPassword) {
    this.reportDBPassword = reportDBPassword;
  }
}
