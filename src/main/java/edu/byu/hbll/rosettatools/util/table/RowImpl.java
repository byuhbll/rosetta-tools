package edu.byu.hbll.rosettatools.util.table;

import java.util.ArrayList;
import java.util.Iterator;

public class RowImpl implements Row {

  private Table table;
  private int index;
  private ArrayList<Cell> cells = new ArrayList<Cell>();

  public RowImpl(Table table, int index) {
    this.table = table;
    this.index = index;
  }

  @Override
  public Cell add(Object value) {
    Cell cell = new CellImpl(this, cells.size(), value);
    cells.add(cell);
    return cell;
  }

  @Override
  public Cell get(int index) {
    return cells.get(index);
  }

  @Override
  public Cell get(String columnName) {
    return get(table.getIndex(columnName));
  }

  @Override
  public int getIndex() {
    return index;
  }

  @Override
  public Table getTable() {
    return table;
  }

  @Override
  public Iterator<Cell> iterator() {
    return cells.iterator();
  }

  @Override
  public Cell set(int index, Cell cell) {
    return set(index, cell.getValue());
  }

  @Override
  public Cell set(int index, Object value) {
    cells.ensureCapacity(index + 1);
    Cell cell = new CellImpl(this, index, value);
    cells.set(index, cell);
    return cell;
  }

  @Override
  public Cell set(String columnName, Cell cell) {
    return set(table.getIndex(columnName), cell);
  }

  @Override
  public Cell set(String columnName, Object value) {
    return set(table.getIndex(columnName), value);
  }

  @Override
  public int size() {
    return cells.size();
  }

  @Override
  public String toString() {
    return cells.toString();
  }
}
