package edu.byu.hbll.rosettatools.util;

import edu.byu.hbll.xml.XmlUtils;
import java.util.List;
import javax.xml.xpath.XPathExpressionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

public class RosettaXmlUtils {

  private static Logger logger = LoggerFactory.getLogger(RosettaXmlUtils.class);

  public static String evaluate(String xpath, Node node) {
    List<Node> list;
    try {
      list = XmlUtils.xpath(xpath, node);
    } catch (XPathExpressionException e) { // TODO Auto-generated catch block
      logger.error(e.toString(), e);
      return "";
    }

    if (list.isEmpty()) {
      return "";
    }

    Node result = list.get(0);

    String response = result == null ? "" : result.getTextContent();

    return response == null ? "" : response;
  }
}
