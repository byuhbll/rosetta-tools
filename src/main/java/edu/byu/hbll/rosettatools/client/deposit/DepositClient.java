package edu.byu.hbll.rosettatools.client.deposit;

import edu.byu.hbll.rosettatools.Configuration;
import edu.byu.hbll.rosettatools.client.SoapClient;
import edu.byu.hbll.rosettatools.sip.SipMoveJob;
import edu.byu.hbll.rosettatools.util.RosettaXmlUtils;
import edu.byu.hbll.xml.XmlUtils;
import java.util.Date;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPPart;
import javax.xml.xpath.XPathExpressionException;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * Deposit Client.
 *
 * @author cfd2
 */
public class DepositClient {

  public static final String URI =
      Configuration.getInstance().getConfig().path("depositUri").asText();

  public static final String IN_PROCESS = "Inprocess";

  static final Logger logger = LoggerFactory.getLogger(SipMoveJob.class);

  /**
   * Is in process.
   *
   * @param pdsHandle the pds handle
   * @param sipId the sip id
   * @return boolean
   * @throws SOAPException the sOAP exception
   * @throws JAXBException the jAXB exception
   * @throws InterruptedException the interrupted exception
   * @throws SAXException the SAX exception
   * @throws XPathExpressionException the XPathExpression exception
   */
  public static boolean isInProcess(String pdsHandle, String sipId)
      throws SOAPException, JAXBException, InterruptedException, SAXException,
          XPathExpressionException {

    // Include yesterday in case a harvest is run overnight
    Date today = new Date();
    Date yesterday = DateUtils.addDays(today, -1);

    SOAPPart soapPart =
        getDepositActivityBySubmitDate(pdsHandle, IN_PROCESS, null, null, yesterday, today, 0, 0);

    String response = RosettaXmlUtils.evaluate("//SubmitDateResult", soapPart);

    Document xml = XmlUtils.parse(response);
    xml = XmlUtils.removeNamespaces(xml);
    Node sip = XmlUtils.xpath("//record[sip_id='" + sipId + "']", xml).get(0);

    if (sip == null) {
      return false;
    }

    String reason = XmlUtils.xpath("//dps:sip_reason/text()", xml).get(0).toString();
    if (reason.length() < 1 || reason == null) {
      return true;
    }

    return false;
  }

  /**
   * Gets deposit activity by submit date.
   *
   * @param pdsHandle the pds handle
   * @param depositActivityStatus the deposit activity status
   * @param producerID the producer iD
   * @param producerAgentID the producer agent iD
   * @param submitDateFrom the submit date from
   * @param submitDateTo the submit date to
   * @param startRecord the start record
   * @param endRecord the end record
   * @return deposit activity by submit date
   * @throws SOAPException the sOAP exception
   * @throws JAXBException the jAXB exception
   */
  public static SOAPPart getDepositActivityBySubmitDate(
      String pdsHandle,
      String depositActivityStatus,
      String producerID,
      String producerAgentID,
      Date submitDateFrom,
      Date submitDateTo,
      int startRecord,
      int endRecord)
      throws SOAPException, JAXBException {

    DepositActivityBySubmitDateRequest data = new DepositActivityBySubmitDateRequest();

    data.setPdsHandle(pdsHandle);
    data.setDepositActivityStatus(depositActivityStatus);
    data.setProducerID(producerID);
    data.setProducerAgentID(producerAgentID);
    data.setSubmitDateFrom(submitDateFrom);
    data.setSubmitDateTo(submitDateTo);
    if (startRecord > 0 && endRecord > startRecord) {
      data.setStartRecord(startRecord);
      data.setEndRecord(endRecord);
    }

    SOAPPart soapPart = SoapClient.send(URI, "getDepositActivityBySubmitDate", data);

    return soapPart;
  }

  /**
   * Submit deposit activity.
   *
   * @param pdsHandle the pds handle
   * @param materialFlowId the material flow id
   * @param subDirectoryName the sub directory name
   * @param producerId the producer id
   * @param depositSetId the deposit set id
   * @return sOAP part
   * @throws SOAPException the sOAP exception
   * @throws JAXBException the jAXB exception
   */
  public static SOAPPart submitDepositActivity(
      String pdsHandle,
      String materialFlowId,
      String subDirectoryName,
      String producerId,
      String depositSetId)
      throws SOAPException, JAXBException {

    SubmitDepositActivityRequest data = new SubmitDepositActivityRequest();

    data.setPdsHandle(pdsHandle);
    data.setMaterialFlowId(materialFlowId);
    data.setSubDirectoryName(subDirectoryName);
    data.setProducerId(producerId);
    data.setDepositSetId(depositSetId);

    SOAPPart soapPart = SoapClient.send(URI, "submitDepositActivity", data);

    return soapPart;
  }
}
