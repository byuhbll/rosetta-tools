package edu.byu.hbll.rosettatools.model;

import java.util.ArrayList;
import java.util.List;

public class CdmCollections {
  private List<CdmCollection> collections = new ArrayList<CdmCollection>();

  public List<CdmCollection> getCollections() {
    return collections;
  }

  public void setCollections(List<CdmCollection> collections) {
    this.collections = collections;
  }
}
