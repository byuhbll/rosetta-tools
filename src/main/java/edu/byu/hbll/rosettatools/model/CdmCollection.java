package edu.byu.hbll.rosettatools.model;

public class CdmCollection {
  private String alias;
  private String name;
  private String path;
  private String secondary_alias;

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getSecondary_alias() {
    return secondary_alias;
  }

  public void setSecondary_alias(String secondary_alias) {
    this.secondary_alias = secondary_alias;
  }
}
