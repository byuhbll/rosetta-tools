package edu.byu.hbll.rosettatools.harvester;

import static org.junit.Assert.assertTrue;

import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.sip.Sip;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import org.junit.Ignore;
import org.junit.Test;

public class CdmHarvesterIT {
  CdmHarvester harvester;
  Sip sip;
  String collection;
  String item;
  Date from;
  Date until;
  int startIndex;
  int endIndex;

  public CdmHarvesterIT() throws Exception {
    harvester = new CdmHarvester();
    sip = new Sip(Files.createTempDirectory("sipTest").toFile(), collection, item);
    from = new SimpleDateFormat("yyyy-MM-dd").parse("2010-01-01");
    until = new SimpleDateFormat("yyyy-MM-dd").parse("2015-01-01");

    collection = "p15999coll24";
    item = "354";
  }

  @Test
  public void testCdmHarvester() {
    assertTrue(harvester != null);
  }

  @Ignore
  public void testListWithPopulatedParamsPass() throws Exception {
    Set<Item> items = harvester.list(collection, from, until, 1, 82);
    System.out.println(items.toString());
    assertTrue(items != null && !items.isEmpty());
  }

  @Ignore
  public void testListWithCollectionParamsOnly() throws Exception {
    Set<Item> items = harvester.list(collection);
    System.out.println(items.toString());
    assertTrue(items != null && !items.isEmpty());
  }

  @Ignore
  public void testGetItem() throws InterruptedException {
    String item = harvester.getItem(collection, this.item);
    assertTrue(item.contains("MSS2407_S1_B1_F4_p063.jpg"));
  }
}
