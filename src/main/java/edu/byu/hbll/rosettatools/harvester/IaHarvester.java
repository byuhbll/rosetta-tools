package edu.byu.hbll.rosettatools.harvester;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.sip.Sip;
import edu.byu.hbll.rosettatools.util.RosettaXmlUtils;
import edu.byu.hbll.xml.XmlUtils;
import edu.byu.hbll.xslt.XslTransformer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPathExpressionException;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * IaHarvester uses Internet Archives' web api's to harvest items
 * --------------------------------------------------------------- This harvester is different from
 * the others in that it is setup to harvest items from a specified scan-date range instead of
 * harvesting by collection. This is because items either (1) don't belong to a collection at all,
 * (2) exist on archive.org, but aren't yet prepared to be harvested, or (3) belong to a collection
 * too large to harvest all at once. Because of this difference the command-line interface has
 * changed somewhat as well:
 *
 * <p>harvest -f 2012-09-01 -u 2012-10-01 ia
 *
 * <p>In the above example, "-f" and "-u" refer not to "modified date", as with the other
 * harvesters, but instead point to the desired scan-date range to harvest. It is also important to
 * note that no collection is specified (nor can it be).
 *
 * <p>get item info http://archive.org/details/breviariumromanu03cath
 *
 * @author Joe Larson
 */
public class IaHarvester extends AbstractHarvester {

  static final Logger logger = LoggerFactory.getLogger(IaHarvester.class);

  private static final Client client = ClientBuilder.newClient();

  private String dcXsl = "/srv/utils/rosettaharvest/xsl/ia/dc.xsl";
  private String iaEnrichXsl = "/srv/utils/rosettaharvest/xsl/ia/iaEnrich.xsl";
  private String premetsXsl = "/srv/utils/rosettaharvest/xsl/ia/premets.xsl";
  private String dcFields = "/srv/utils/rosettaharvest/xsl/ia/dcFields.xml";
  private String host = "http://archive.org/";

  private XslTransformer dcXslUtil;
  private XslTransformer premetsXslUtil;
  private XslTransformer iaEnrichXslUtil;

  private boolean autoOrganizeCollections = false;

  @Override
  public Set<Item> list() throws Exception {
    return list("ia", null, null, 0, 0);
  }

  @Override
  public Set<Item> list(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {

    List<String> pointers;

    Set<Item> items = new HashSet<>();

    pointers = getItems(from, until);

    for (String pointer : pointers) {
      Item item = new Item(pointer);
      items.add(item);
    }

    return items;
  }

  @Override
  public void harvest(Date from, Date until, int startIndex, int throughIndex) throws Exception {
    harvest("ia", from, until, startIndex, throughIndex);
  }

  @Override
  public void harvest(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {

    Node fieldMap = null;

    try (FileInputStream in = new FileInputStream(dcFields)) {
      fieldMap = XmlUtils.parse(in);
    }

    autoOrganizeCollections = rosettaCollection.equals("ia");

    for (Item item : list(collection, from, until, startIndex, throughIndex)) {
      harvest(collection, item.getId(), fieldMap);
    }
  }

  @Override
  public void harvest(String collection, String item)
      throws IOException, TransformerException, SAXException, XPathExpressionException,
          TransformerFactoryConfigurationError {
    Document doc = null;
    try (FileInputStream in = new FileInputStream(dcFields)) {
      doc = XmlUtils.parse(dcFields);
    }
    harvest(collection, item, doc);
  }

  /**
   * Harvests an item from IA.
   *
   * @param collection the collection
   * @param item the item
   * @param fieldMap the fieldMap
   * @throws IOException if an error occurs
   * @throws TransformerException if an error occurs
   * @throws XPathExpressionException if an error occurs
   * @throws TransformerFactoryConfigurationError if an error occurs
   */
  private void harvest(String collection, String item, Node fieldMap)
      throws IOException, TransformerException, XPathExpressionException,
          TransformerFactoryConfigurationError {
    rosettaCollection = rosettaCollection.equals("ia") ? "InternetArchive" : rosettaCollection;

    logger.info("Harvesting item: " + collection + "/" + item);

    // 1. Get the json from archive.org
    JsonNode json;
    try {
      json = mapper.valueToTree(getItem(collection, item));
    } catch (Exception e) {
      logger.error("Unable to harvest item:" + collection + "/" + item + " " + e);
      return;
    }
    JsonNode iaCollections = json.get("metadata").get("collection");

    // BYU's items in Internet Archive are tagged with the "americana" and
    // "brigham_young_university" collections by default. Most items only
    // have these two collections assigned to them, but in the case that
    // there is a third, the item should be assigned to that specific sub-
    // collection. So items without a third collection will reside in the
    // "ia" collection, where as items with an extra collection "foo" will
    // reside in "ia/foo".
    if (autoOrganizeCollections) {
      for (JsonNode iaCollection : iaCollections) {
        String iaCollectionName = iaCollection.asText();
        if (!iaCollectionName.equals("americana")
            && !iaCollectionName.equals("brigham_young_university")) {
          rosettaCollection = "InternetArchive/" + iaCollectionName;
          break;
        }
      }
    }

    // 2. Create SIP
    Sip sip = sipFactory.newInstance(collection, item);

    // 4. Process issue object
    Node doc = processIssue(sip, json, item);

    // 3. Create params map
    Map<String, Object> params = new HashMap<>();
    params.put("collection", rosettaCollection);
    params.put("item", item);
    params.put("retentionPolicy", getRetention());
    params.put("rightsPolicy", getRights());

    // 6. Enrich the issue with name and dc counterparts
    DOMSource xmlSource = new DOMSource(doc);
    DOMResult iaResult = new DOMResult();
    iaEnrichXslUtil.transform(xmlSource, iaResult, params);

    Node ia = iaResult.getNode();
    params.put("srcmd", xmlSource);

    // 7. Convert to actual dc
    DOMResult dcResult = new DOMResult();
    dcXslUtil.transform(new DOMSource(ia), dcResult, params);
    Node dc = dcResult.getNode();
    params.put("dc", dc);

    // 8. Save the metadata for the SIP (not the item)
    sip.saveDc(XmlUtils.toString(dc, true));

    // 9. Create the mets document
    String metsXsml = toMetsXml(premetsXslUtil, xmlSource, params);
    sip.saveMets(metsXsml);

    logger.info("Harvested item: " + collection + "/" + item);

    // 10. Submit the SIP
    if (noSubmit) {
      logger.warn("Skipping submission to Rosetta");
    } else {
      sip.submit();
    }
  }

  /**
   * Processes an issue.
   *
   * @param sip the sip
   * @param json the json
   * @param item the item
   * @return Document
   * @throws IOException if an error occurs
   * @throws TransformerFactoryConfigurationError if an error occurs
   * @throws TransformerException if an error occurs
   * @throws XPathExpressionException if an error occurs
   */
  public Document processIssue(Sip sip, JsonNode json, String item)
      throws IOException, TransformerFactoryConfigurationError, TransformerException,
          XPathExpressionException {
    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = null;
    try {
      docBuilder = docFactory.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
    Document doc = docBuilder.newDocument();
    Element rootElement = doc.createElement("xml");
    doc.appendChild(rootElement);
    jsonToXml(json, doc, rootElement);

    if (!noDownload) {
      downloadArticleFiles(sip, doc, item);
    } else {
      logger.warn("Skipping downloading of object");
    }
    return doc;
  }

  private void jsonToXml(JsonNode json, Document doc, Element parent) {
    if (!json.isArray()) {
      Iterator<String> fieldNames = json.fieldNames();
      while (fieldNames.hasNext()) {
        String field = fieldNames.next();
        String xmlFieldName = parent.getNodeName().equals("files") ? "file" : field;
        Element element;
        if (json.get(field).isTextual()) {
          element = doc.createElement(xmlFieldName);
          element.appendChild(doc.createTextNode(json.get(field).textValue()));
          parent.appendChild(element);
        } else if (json.get(field).isNumber()) {
          element = doc.createElement(xmlFieldName);
          element.appendChild(doc.createTextNode(json.get(field).numberValue().toString()));
          parent.appendChild(element);
        } else {
          element = doc.createElement(xmlFieldName);
          parent.appendChild(element);
          jsonToXml(json.get(field), doc, element);
        }
        if (parent.getNodeName().equals("files")) {
          Element nameElement = doc.createElement("name");
          nameElement.appendChild(doc.createTextNode(field.replace("/", "")));
          element.appendChild(nameElement);
        }
      }
    } else {
      for (JsonNode node : json) {
        if ((node.isTextual() || node.isNumber()) && json.size() == 1) {
          parent.appendChild(doc.createTextNode(node.textValue()));
        } else {
          String parentName = parent.getTagName();
          Element element = doc.createElement(parentName.substring(0, parentName.length() - 1));
          element.appendChild(doc.createTextNode(node.textValue()));
          parent.appendChild(element);
          jsonToXml(node, doc, element);
        }
      }
    }
  }

  /**
   * Downloads files for an article.
   *
   * @param sip the sip
   * @param doc the Document
   * @param item the item
   * @throws IOException if an error occurs
   * @throws TransformerFactoryConfigurationError if an error occurs
   * @throws TransformerException if an error occurs
   * @throws XPathExpressionException if an error occurs
   */
  public void downloadArticleFiles(Sip sip, Document doc, String item)
      throws IOException, TransformerFactoryConfigurationError, TransformerException,
          XPathExpressionException {
    String server = RosettaXmlUtils.evaluate("/xml/server", doc);
    String dir = RosettaXmlUtils.evaluate("/xml/dir", doc);
    // If you edit the xpath below, you must also update the  xpath in premets.xsl
    List<Node> files =
        XmlUtils.xpath(
            "xml/files/file["
                + "name/text()='"
                + item
                + "_dc.xml' or "
                + "name/text()='"
                + item
                + "_files.xml' or "
                + "name/text()='"
                + item
                + "_marc.xml' or "
                + "name/text()='"
                + item
                + "_meta.xml' or "
                + "name/text()='"
                + item
                + "_scandata.xml' or "
                + "name/text()='"
                + item
                + ".gif' or "
                + "name/text()='"
                + item
                + ".pdf' or "
                + "name/text()='"
                + item
                + "_jp2.zip' or "
                + "name/text()='"
                + item
                + "_orig_jp2.tar' "
                + "]/name",
            doc);
    for (Node file : files) {
      String md5 =
          RosettaXmlUtils.evaluate(
              "/xml/files/file[name/text()='" + file.getTextContent() + "']/md5", doc);
      sip.downloadObject("http://" + server + dir + "/" + file.getTextContent(), null, md5);
    }
  }

  /**
   * Gets an item from IA.
   *
   * @param collection the collection
   * @param item the item
   * @return String item
   * @throws InterruptedException if an error occurs
   */
  public String getItem(String collection, String item) throws InterruptedException {

    logger.debug("Getting item info: " + collection + "/" + item);

    WebTarget target = client.target(host + "details/" + item + "?output=json");
    Response response = target.request().buildGet().invoke();

    String itemInfo = "";
    for (int i = 0; i < 10; i++) {
      try {
        itemInfo = response.readEntity(String.class);
        break;
      } catch (Exception e) {
        Thread.sleep(1000 * i);
        logger.debug("Retrying getting item info: " + collection + "/" + item + " " + e);
      }
    }

    if (!itemInfo.equals("")) {
      logger.debug(
          "Received item info: " + collection + "/" + item + ": " + itemInfo.length() + " bytes");
    }

    return itemInfo;
  }

  /**
   * Gets all items in the date range.
   *
   * @param from start date
   * @param until end date
   * @return the items
   * @throws IOException if an error occurs
   */
  public List<String> getItems(Date from, Date until) throws IOException {
    int pointersPerPage = 100;
    JsonNode firstPage = getPointersPage(from, until, pointersPerPage, 1);
    int totalPointers = firstPage.get("response").get("numFound").asInt();
    int pageCount = (int) Math.ceil((double) totalPointers / (double) pointersPerPage);
    logger.info("Retrieving pointers...");
    List<String> pointers = new ArrayList<String>();
    if (totalPointers == 0) {
      logger.warn("No pointers found for the specified date range");
    }
    for (int i = 1; i <= pageCount; i++) {
      JsonNode page = getPointersPage(from, until, pointersPerPage, i);
      for (JsonNode id : page.get("response").get("docs")) {
        pointers.add(id.get("identifier").asText());
      }
      int pointerFrom = (i - 1) * pointersPerPage + 1;
      int pointerUntil =
          (i * pointersPerPage) > totalPointers
              ? pointerFrom - 1 + (totalPointers % pointersPerPage)
              : i * pointersPerPage;
      logger.info("Received pointers: " + pointerFrom + "-" + pointerUntil + "/" + totalPointers);
    }

    return pointers;
  }

  private JsonNode getPointersPage(Date from, Date until, int recordsPerPage, int page)
      throws IOException {
    WebTarget target;
    if (from != null || until != null) {
      SimpleDateFormat scanDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
      String scanDateFrom = from != null ? scanDateFormat.format(from) : "null";
      String scanDateUntil = until != null ? scanDateFormat.format(until) : "null";
      target =
          client.target(
              host
                  + "advancedsearch.php?q=(sponsor:Brigham)+AND+scandate:["
                  + scanDateFrom
                  + "+TO+"
                  + scanDateUntil
                  + "]&fl[]=identifier&rows="
                  + recordsPerPage
                  + "&page="
                  + page
                  + "&output=json");
    } else {
      target =
          client.target(
              host
                  + "advancedsearch.php?q=(sponsor:Brigham)&fl[]=identifier&rows="
                  + recordsPerPage
                  + "&page="
                  + page
                  + "&output=json");
    }
    Response response = target.request().buildGet().invoke();
    return mapper.readTree(response.readEntity(String.class));
  }

  // getters
  public String getHost() {
    return host;
  }

  public String getDcXsl() {
    return dcXsl;
  }

  public String getpremetsXsl() {
    return premetsXsl;
  }

  public String getIaEnrichXsl() {
    return iaEnrichXsl;
  }

  public String getDcFields() {
    return dcFields;
  }

  // setters
  public void setHost(String host) {
    this.host = host;
  }

  public void setDcFields(String dcFields) {
    this.dcFields = dcFields;
  }

  /**
   * Sets the dc XSL.
   *
   * @param dcXsl the dc xsl
   * @throws TransformerConfigurationException if an error occurs
   */
  public void setDcXsl(String dcXsl) throws TransformerConfigurationException {
    this.dcXsl = dcXsl;
    this.dcXslUtil = new XslTransformer(new StreamSource(new File(dcXsl)));
  }

  /**
   * Sets the premets xsl.
   *
   * @param premetsXsl the premets xsl
   * @throws TransformerConfigurationException if an error occurs
   */
  public void setPremetsXsl(String premetsXsl) throws TransformerConfigurationException {
    this.premetsXsl = premetsXsl;
    this.premetsXslUtil = new XslTransformer(new StreamSource(new File(premetsXsl)));
  }

  /**
   * Sets the ia enrich xsl.
   *
   * @param iaEnrichXsl the ia enrich xsl
   * @throws TransformerConfigurationException if an error occurs
   */
  public void setIaEnrichXsl(String iaEnrichXsl) throws TransformerConfigurationException {
    this.iaEnrichXsl = iaEnrichXsl;
    this.iaEnrichXslUtil = new XslTransformer(new StreamSource(new File(iaEnrichXsl)));
  }

  /**
   * Gets the name of a file without the extension and lowercase.
   *
   * @param file the file
   * @return the name
   */
  public static String getKeyPart(File file) {
    return FilenameUtils.removeExtension(file.getName()).toLowerCase();
  }
}
