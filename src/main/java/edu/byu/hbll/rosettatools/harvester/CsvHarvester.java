/** */
package edu.byu.hbll.rosettatools.harvester;

import edu.byu.hbll.rosettatools.data.DublinCore;
import edu.byu.hbll.rosettatools.data.Field;
import edu.byu.hbll.rosettatools.data.Item;
import edu.byu.hbll.rosettatools.data.Spreadsheet;
import edu.byu.hbll.rosettatools.sip.Sip;
import edu.byu.hbll.rosettatools.util.RosettaXmlUtils;
import edu.byu.hbll.rosettatools.util.table.Cell;
import edu.byu.hbll.rosettatools.util.table.Row;
import edu.byu.hbll.rosettatools.util.table.Table;
import edu.byu.hbll.rosettatools.util.table.WorkbookUtil;
import edu.byu.hbll.xml.XmlUtils;
import edu.byu.hbll.xslt.XslTransformer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPathExpressionException;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * @author cfd2
 * @author joelarson
 */
public class CsvHarvester extends AbstractHarvester {

  static final Logger logger = LoggerFactory.getLogger(CsvHarvester.class);

  private static final String FILE_LABEL = "FILENAME";
  private static final String PATH_LABEL = "FILEPATH";
  private static final String COLLECTION_LABEL = "COLLECTION";
  private static final String ITEM_LABEL = "ITEM_ID";
  private static final String MODIFIED_LABEL = "DATE_MODIFIED";

  private static final String RETENTION_POLICY = "RETENTION_POLICY";
  private static final String RIGHTS_POLICY = "RIGHTS_POLICY";

  private static final String[] REQUIRED_FIELDS = {
    ITEM_LABEL, PATH_LABEL, MODIFIED_LABEL, COLLECTION_LABEL
  };

  private String metadataDir = "/operational_shared/csv";
  private String objectsDir = "/operational_shared/csv/objects";
  private String premetsXsl = "xsl/csv/premets.xsl";
  private String dcFields = "xsl/csv/dcFields.xml";

  private List<File> csvFiles = new ArrayList<File>();

  private XslTransformer premetsXslUtil;
  private Map<String, String> dcMap = new HashMap<String, String>();
  private Map<String, String> dcTermsMap = new HashMap<String, String>();

  @Override
  public Set<Item> list() throws Exception {
    return list(COLLECTION_LABEL, null, null, null, 0, 0);
  }

  @Override
  public Set<Item> list(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {
    return list(ITEM_LABEL, collection, from, until, startIndex, throughIndex);
  }

  private Set<Item> list(
      String type, String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {
    Set<Item> items = new HashSet<Item>();

    for (File workbookFile : csvFiles) {
      WorkbookUtil wb = new WorkbookUtil(workbookFile);
      Table table = wb.toTable(0, true, true, 1);

      for (Row row : table) {
        if (accept(workbookFile.getName(), wb, row, collection, null, from, until, true)) {
          items.add(new Item(formatCell(wb, row.get(type))));
        }
      }
    }
    return items;
  }

  @Override
  public void harvest(Date from, Date until, int startIndex, int throughIndex) throws Exception {
    harvest(null, null, from, until, startIndex, throughIndex);
  }

  @Override
  public void harvest(String collection, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {
    harvest(collection, null, from, until, startIndex, throughIndex);
  }

  @Override
  public void harvest(String collection, String item) throws Exception {
    harvest(collection, item, null, null, 0, 0);
  }

  private void harvest(
      String collection, String item, Date from, Date until, int startIndex, int throughIndex)
      throws Exception {
    logger.info("csv harvester initiated for collection: " + collection);
    initFields();

    for (File workbookFile : csvFiles) {
      logger.info("Harvesting " + workbookFile);

      //Test requested by Chris Erickson - catches workbooks that are empty or invalid
      //before processing and skips file if error encountered
      try {
        Workbook workbook = WorkbookFactory.create(new FileInputStream(workbookFile));
      } catch (IllegalArgumentException e) {
        logger.error("csv file " + workbookFile + " is empty or not valid");
        continue;
      }

      WorkbookUtil wb = new WorkbookUtil(workbookFile);
      Table userFieldTable = wb.toTable(0, true, true, 0);
      List<String> userFields = userFieldTable.getColumnNames();
      userFieldTable = null;
      Table table = wb.toTable(0, true, true, 1);

      if (missingReqColumns(table, workbookFile)) {
        logger.warn("csv file " + workbookFile + " is missing requried columns");
        continue;
      }

      for (Row row : table) {
        if (accept(workbookFile.getName(), wb, row, collection, item, from, until, false)) {
          harvest(wb, row, userFields);
        }
      }
    }
  }

  private boolean missingReqColumns(Table table, File file) {
    List<String> missingColumns = new ArrayList<String>();
    for (String field : REQUIRED_FIELDS) {
      if (!table.getColumnNames().contains(field)) {
        missingColumns.add(field);
      }
    }
    if (missingColumns.size() > 0) {
      String plurality = missingColumns.size() > 1 ? "s" : "";
      logger.error(
          "Skipping " + file.getName() + ": Missing column" + plurality + " -> " + missingColumns);
      return true;
    } else {
      return false;
    }
  }

  /**
   * Accept boolean.
   *
   * @param filename the filename
   * @param wb the wb
   * @param row the row
   * @param collection the collection
   * @param item the item
   * @param from the from
   * @param until the until
   * @param list the list
   * @return boolean
   */
  public boolean accept(
      String filename,
      WorkbookUtil wb,
      Row row,
      String collection,
      String item,
      Date from,
      Date until,
      boolean list) {
    if (isEmptyRow(wb, row)) {
      return false;
    }

    List<String> missingReqFields = getMissingReqFields(row, collection);

    // check if in correct collection
    if (collection != null) {
      if (missingReqFields.contains(COLLECTION_LABEL)) {
        logger.error("Missing COLLECTION column");
        return false;
      } else if (!collection.equals(formatCell(wb, row.get(COLLECTION_LABEL)).trim())) {
        logger.warn(
            "Collection expected: "
                + collection
                + " Collection found: "
                + formatCell(wb, row.get(COLLECTION_LABEL)));
        return false;
      }
    }

    if (missingReqFields.size() > 0) {
      // Skip incomplete rows without showing error when in list mode
      if (list) {
        return false;
      }

      int sheetRow = 0;
      // Find first non-null cell to pull originating spreadsheet row for error reporting purposes
      for (int i = 0; i < row.size(); i++) {
        org.apache.poi.ss.usermodel.Cell cell =
            (org.apache.poi.ss.usermodel.Cell) row.get(i).getValue();
        if (cell != null) {
          sheetRow = cell.getRowIndex() + 1;
          break;
        }
      }
      logger.warn(
          "Skipping "
              + filename
              + " row "
              + sheetRow
              + ": Missing -> "
              + missingReqFields.toString());
      return false;
    }

    // check if correct item
    if (item != null && !item.equals(formatCell(wb, row.get(ITEM_LABEL)))) {
      return false;
    }

    // check if modified included in range
    if (!AbstractHarvester.include(getDate(wb, row.get(MODIFIED_LABEL)), from, until)) {
      return false;
    }

    return true;
  }

  private boolean isEmptyRow(WorkbookUtil wb, Row row) {
    boolean result = true;
    for (Cell cell : row) {
      String value = formatCell(wb, cell);
      if (value != null && !value.isEmpty()) {
        result = false;
        break;
      }
    }
    return result;
  }

  private List<String> getMissingReqFields(Row row, String collection) {
    List<String> missingFields = new ArrayList<String>();
    for (String field : REQUIRED_FIELDS) {
      if (row.get(field).getValue() == null) {
        missingFields.add(field);
      } else if (row.get(field).getValue().toString().isEmpty()) {
        missingFields.add(field);
      }
    }
    return missingFields;
  }

  private void harvest(WorkbookUtil wb, Row row, List<String> userFields) throws Exception {
    String collection = formatCell(wb, row.get(COLLECTION_LABEL));
    String item = formatCell(wb, row.get(ITEM_LABEL));

    logger.info("Harvesting item: " + collection + "/" + item);

    Sip sip = sipFactory.newInstance(collection, item);

    File object =
        new File(
            new File(formatCell(wb, row.get(PATH_LABEL))), formatCell(wb, row.get(FILE_LABEL)));

    //		logger.info("PATH: " + formatCell(wb, row.get(PATH_LABEL)));
    //		logger.info("FILE: " + formatCell(wb, row.get(FILE_LABEL)));
    //		logger.info("OBJECT: " + object);

    if (noDownload) {
      logger.warn("Skipping copying the object");
    } else {
      logger.info("Copying object: " + collection + "/" + item);
      sip.saveObjectCopy(object);

      String harvestedFilePath = object.getParent() + "/harvested/" + object.getName();

      try {
        File harvestedFile = new File(harvestedFilePath);
        FileUtils.moveFile(object, harvestedFile);
      } catch (FileExistsException e) {
        // if harvestedFile already exists, timestamp the existing restingDir
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        File harvestedFile = new File(harvestedFilePath + "." + sdf.format(new Date()));
        FileUtils.moveFile(object, harvestedFile);
      }
    }

    DublinCore dcObj = rowToDc(wb, row);
    Node dc = XmlUtils.marshal(dcObj);

    Spreadsheet srcObj = rowToSrc(wb, row, userFields);
    Node src = XmlUtils.marshal(srcObj);
    ((Element) (src.getFirstChild())).removeAttribute("xmlns:dc");
    ((Element) (src.getFirstChild())).removeAttribute("xmlns:dcterms");

    // save the metadata for the sip
    sip.saveDc(XmlUtils.toString(dc, true));

    Map<String, Object> params = new HashMap<String, Object>();

    params.put("dc", dc);
    params.put("srcmd", src);
    params.put("object-file", object.getName());

    // Set the retention policy
    try {
      // Check if RIGHTS_POLICY column exists (if not, throws to catcher)
      Cell rowRetention = row.get(RETENTION_POLICY);
      // Check value RETENTION_POLICY cell in current row
      if (rowRetention.getValue() != null
          && !rowRetention.getValue().toString().trim().equals("")) {
        // Assign cell value
        params.put("retentionPolicy", formatCell(wb, rowRetention));
      } else {
        // Assign default value (set in shell command, or if not, uses default from configuration file)
        params.put("retentionPolicy", getRetention());
      }
    } catch (Exception e) {
      // When no RETENTION_POLICY column exists, assign default value
      params.put("retentionPolicy", getRetention());
    }

    // Set the rights policy
    try {
      // Check if POLICY_RIGHTS column exists (if not, throws to catcher)
      Cell rowRights = row.get(RIGHTS_POLICY);
      // Check value POLICY_RIGHTS cell in current row
      if (rowRights.getValue() != null && !rowRights.getValue().toString().trim().equals("")) {
        // Assign cell value
        params.put("rightsPolicy", formatCell(wb, rowRights));
      } else {
        // Assign default value (set in shell command, or if not, uses default from configuration file)
        params.put("rightsPolicy", getRights());
      }
    } catch (Exception e) {
      // When no POLICY_RIGHTS column exists, assign default value
      params.put("rightsPolicy", getRights());
    }

    String metsXsml =
        toMetsXml(premetsXslUtil, new StreamSource(new StringReader("<record />")), params);
    sip.saveMets(metsXsml);

    logger.info("Harvested item: " + collection + "/" + item);

    if (noSubmit) {
      logger.warn("Skipping submission to Rosetta");
    } else {
      sip.submit();
    }
  }

  private Spreadsheet rowToSrc(WorkbookUtil wb, Row row, List<String> userFields) {
    Spreadsheet src = new Spreadsheet();
    int counter = 0;
    for (Cell cell : row) {
      String value = formatCell(wb, cell);

      if (value != null && !value.isEmpty()) {
        src.add(new Field(userFields.get(counter), value));
      }
      counter++;
    }

    return src;
  }

  /**
   * Row to dc.
   *
   * @param wb the wb
   * @param row the row
   * @return dublin core
   */
  public DublinCore rowToDc(WorkbookUtil wb, Row row) {
    DublinCore dc = new DublinCore();

    for (Cell cell : row) {
      String columnName = cell.getColumnName();
      String field = dcMap.get(columnName);
      String dctField = dcTermsMap.get(columnName);
      if (dctField != null) {
        field = dctField;
      }
      String value =
          columnName.equals(COLLECTION_LABEL) && rosettaCollection != null
              ? rosettaCollection
              : formatCell(wb, cell);

      if (field != null && value != null && !value.isEmpty()) {
        dc.add(field, value);
      }
    }

    return dc;
  }

  /**
   * Sets premets xsl.
   *
   * @param premetsXsl the premets xsl
   * @throws TransformerConfigurationException the transformer configuration exception
   */
  public void setPremetsXsl(String premetsXsl) throws TransformerConfigurationException {
    this.premetsXsl = premetsXsl;
    this.premetsXslUtil = new XslTransformer(new StreamSource(new File(premetsXsl)));
  }

  private String formatCell(WorkbookUtil wb, Cell cell) {
    org.apache.poi.ss.usermodel.Cell innerCell = (org.apache.poi.ss.usermodel.Cell) cell.getValue();
    String value = wb.format(innerCell);
    return value;
  }

  private Date getDate(WorkbookUtil wb, Cell cell) {
    org.apache.poi.ss.usermodel.Cell innerCell = (org.apache.poi.ss.usermodel.Cell) cell.getValue();
    if (DateUtil.isCellDateFormatted(innerCell)) {
      return innerCell.getDateCellValue();
    }

    throw new ClassCastException(
        "row["
            + innerCell.getRowIndex()
            + "]col["
            + innerCell.getColumnIndex()
            + "] \""
            + formatCell(wb, cell)
            + "\" is not a date");
  }

  private void initFields() throws SAXException, IOException, XPathExpressionException {

    Document dcMap = null;
    try (FileInputStream in = new FileInputStream(dcFields)) {
      dcMap = XmlUtils.parse(in);
    }

    List<Node> fields = XmlUtils.xpath("/fields/field", dcMap);

    for (Node field : fields) {
      String name = RosettaXmlUtils.evaluate("name", field);
      String dcField = RosettaXmlUtils.evaluate("dc", field);
      String dcTermsField = RosettaXmlUtils.evaluate("dcterms", field);

      this.dcMap.put(name, dcField);
      this.dcTermsMap.put(name, dcTermsField);
    }
  }

  // getters
  public String getMetadataDir() {
    return metadataDir;
  }

  public String getObjectsDir() {
    return objectsDir;
  }

  public String getpremetsXsl() {
    return premetsXsl;
  }

  public String getDcFields() {
    return dcFields;
  }

  // setters
  public void setObjectsDir(String objectsDir) {
    this.objectsDir = objectsDir;
  }

  public void setDcFields(String dcFields) {
    this.dcFields = dcFields;
  }

  /**
   * Sets metadata dir.
   *
   * @param metadataDir the metadata dir
   * @throws IOException the iO exception
   */
  public void setMetadataDir(String metadataDir) throws IOException {
    this.metadataDir = metadataDir;
    csvFiles.clear();

    for (File csvFile : new File(metadataDir).listFiles()) {
      String filename = csvFile.getName();

      if (csvFile.isFile() && filename.matches(".*\\.(csv|xls|xlsx)$")) {
        csvFiles.add(csvFile);
      }
    }
  }
}
